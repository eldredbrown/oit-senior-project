-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2017-01-14
-- Description:	A procedure to return a conference's season standings
-- Revision history:
-- =============================================
CREATE PROCEDURE usp_GetSeasonStandings
	-- Add the parameters for the stored procedure here
	@seasonID int,
	@groupByDivision bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@groupByDivision = 1)
	BEGIN
		-- Insert statements for procedure here
		SELECT
			*
		FROM
			fn_GetSeasonStandings(@seasonID)
		ORDER BY
			Division ASC,
			WinningPercentage DESC
	END
	ELSE
	BEGIN
		-- Insert statements for procedure here
		SELECT
			*
		FROM
			fn_GetSeasonStandings(@seasonID)
		ORDER BY
			Conference ASC,
			WinningPercentage DESC

	END
END
GO
