-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-25
-- Description:	A procedure to compute and return final offensive ratings for all teams
-- Revision History:
--	2017-01-17	Eldred Brown	Added logic to restrict results to one season
-- =============================================
CREATE PROCEDURE usp_GetOffensiveRankings
	-- Add the parameters for the stored procedure here
	@seasonID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT
		TeamName AS Team,
		Wins,
		Losses,
		Ties,
		OffensiveAverage,
		OffensiveFactor,
		OffensiveIndex
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
	ORDER BY
		OffensiveIndex DESC
END
GO
