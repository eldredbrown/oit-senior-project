-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2017-01-14
-- Description:	A procedure to return a division's standings
-- Revision history:
-- =============================================
CREATE PROCEDURE usp_GetDivisionSeasonStandings 
	-- Add the parameters for the stored procedure here
	@divisionName nvarchar(25),
	@seasonID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Validate argument.
	IF EXISTS (
		SELECT * FROM Divisions WHERE Name = @divisionName
	)
	AND EXISTS (
		SELECT * FROM Seasons WHERE ID = @seasonID
	)
	BEGIN
		-- Insert statements for procedure here
		SELECT *
		FROM
			fn_GetDivisionSeasonStandings(@divisionName, @seasonID)
		ORDER BY
			WinningPercentage DESC

	END
END
GO
