-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-21
-- Description:	A function to return a team's profile
-- Revision History:
--	2017-01-05	Eldred Brown
--	*	Added parameter to restrict results to a single season	
-- =============================================
CREATE FUNCTION fn_GetTeamSeasonScheduleProfile 
(	
	-- Add the parameters for the function here
	@teamName varchar(50),
	@seasonID int
)
RETURNS @tbl TABLE
(
	ID								int PRIMARY KEY,
	Opponent						varchar(50),
	GamePointsFor					float,
	GamePointsAgainst				float,
	OpponentWins					float,
	OpponentLosses					float,
	OpponentTies					float,
	OpponentWinningPercentage		float,
	OpponentWeightedGames			float,
	OpponentWeightedPointsFor		float,
	OpponentWeightedPointsAgainst	float
)
AS
BEGIN
	-- Add the SELECT statement with parameter references here
	INSERT @tbl
	SELECT
		Q1.ID,
		Q1.Opponent,
		Q1.GamePointsFor,
		Q1.GamePointsAgainst,
		Q2.OpponentWins,
		Q2.OpponentLosses,
		Q2.OpponentTies,
		Q2.OpponentWinningPercentage,
		Q2.OpponentWeightedGames,
		Q2.OpponentWeightedPointsFor,
		Q2.OpponentWeightedPointsAgainst
	FROM
		fn_GetTeamSeasonGames(@teamName, @seasonID) AS Q1
		INNER JOIN
		fn_GetTeamSeasonScheduleData(@teamName, @seasonID) AS Q2
		  ON Q2.Opponent = Q1.Opponent

	RETURN
END
