-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-23
-- Description:	A function to compute and return the averages 
--				of a team's schedule data
-- Revision History:
--	2017-01-05	Eldred Brown
--	*	Added parameter to restrict results to a single season
-- =============================================
CREATE FUNCTION fn_GetTeamSeasonScheduleAverages 
(	
	-- Add the parameters for the function here
	@teamName varchar(50),
	@seasonID int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		ROUND(Q.PointsFor / Q.Games, 2) AS PointsFor,
		ROUND(Q.PointsAgainst / Q.Games, 2) AS PointsAgainst,
		ROUND(Q.SchedulePointsFor / Q.ScheduleGames, 2) AS SchedulePointsFor,
		ROUND(Q.SchedulePointsAgainst / Q.ScheduleGames, 2) AS SchedulePointsAgainst
	FROM
		fn_GetTeamSeasonScheduleTotals(@teamName, @seasonID) AS Q
)
GO
