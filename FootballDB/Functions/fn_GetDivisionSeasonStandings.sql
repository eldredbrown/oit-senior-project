-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2017-01-15
-- Description:	A function to compute and return a division's standings
-- Revision History:
-- =============================================
CREATE FUNCTION fn_GetDivisionSeasonStandings
(
	@divisionName nvarchar(25),
	@seasonID int
)
RETURNS @tbl TABLE
(
	Team				nvarchar(50),
	Wins				float,
	Losses				float,
	Ties				float,
	WinningPercentage	float,
	PointsFor			float,
	PointsAgainst		float,
	AvgPointsFor		float,
	AvgPointsAgainst	float
)
AS
BEGIN

	IF ((SELECT SUM(Games) FROM TeamSeasons) = 0)
	-- Prevent division by zero.
	BEGIN
		INSERT @tbl

		SELECT
			TeamName as Team,
			Wins,
			Losses,
			Ties,
			NULL As WinningPercentage,
			PointsFor,
			PointsAgainst,
			NULL AS AvgPointsFor,
			NULL AS AvgPointsAgainst
		FROM
			TeamSeasons
		WHERE
			DivisionName = @divisionName
			AND
			SeasonID = @seasonID
	END
	ELSE
	BEGIN
		INSERT @tbl

		SELECT
			TeamName as Team,
			Wins,
			Losses,
			Ties,
			WinningPercentage,
			PointsFor,
			PointsAgainst,
			PointsFor / Games AS AvgPointsFor,
			PointsAgainst / Games AS AvgPointsAgainst
		FROM
			TeamSeasons
		WHERE
			DivisionName = @divisionName
			AND
			SeasonID = @seasonID
	END

	RETURN
END
GO
