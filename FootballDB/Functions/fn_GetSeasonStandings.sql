-- ================================================
-- Template generated from Template Explorer using:
-- Create Inline Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Eldred Brown
-- Create date: 2017-01-15
-- Description:	A function to compute and return a conference's standings
-- Revision History:
-- =============================================
CREATE FUNCTION fn_GetSeasonStandings
(
	@seasonID int
)
RETURNS @tbl TABLE
(
	Team				nvarchar(50),
	Conference			nchar(3),
	Division			nvarchar(25),
	Wins				float,
	Losses				float,
	Ties				float,
	WinningPercentage	float,
	PointsFor			float,
	PointsAgainst		float,
	AvgPointsFor		float,
	AvgPointsAgainst	float
)
AS
BEGIN
	INSERT @tbl

	SELECT
		TeamName as Team,
		ConferenceName as Conference,
		DivisionName as Division,
		Wins,
		Losses,
		Ties,
		WinningPercentage,
		PointsFor,
		PointsAgainst,
		PointsFor / Games AS AvgPointsFor,
		PointsAgainst / Games AS AvgPointsAgainst
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
		AND
		Games > 0
	UNION
	SELECT
		TeamName as Team,
		ConferenceName as Conference,
		DivisionName as Division,
		Wins,
		Losses,
		Ties,
		NULL as WinningPercentage,
		PointsFor,
		PointsAgainst,
		NULL as AvgPointsFor,
		NULL  as AvgPointsAgainst
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
		AND
		Games = 0

RETURN

END
GO
