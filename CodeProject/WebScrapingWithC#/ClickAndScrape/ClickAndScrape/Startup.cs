﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClickAndScrape.Startup))]
namespace ClickAndScrape
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
