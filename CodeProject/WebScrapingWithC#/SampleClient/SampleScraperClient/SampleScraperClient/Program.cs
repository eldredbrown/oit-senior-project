﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HtmlAgilityPack;

using ScrapySharp.Core;
using ScrapySharp.Extensions;
using ScrapySharp.Html.Forms;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Network;

namespace SampleScraperClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // Setup the browser.
            var browser = new ScrapingBrowser();
            browser.AllowAutoRedirect = true;   // Browser has many settings you can access in setup.
            browser.AllowMetaRedirect = true;

            // Go to the home page.
            WebPage pageResult = browser.NavigateToPage(new Uri("http://localhost:51621/"));

            // Get first piece of data, the page title.
            HtmlNode titleNode = pageResult.Html.CssSelect(".navbar-brand").First();
            string pageTitle = titleNode.InnerText;

            // Get a list of data from a table.
            List<String> names = new List<string>();
            var table = pageResult.Html.CssSelect("#PersonTable").First();
            foreach (var row in table.SelectNodes("tbody/tr"))
            {
                foreach (var cell in row.SelectNodes("td[1]"))
                {
                    names.Add(cell.InnerText);  
                }
            }

            // Find a form and send back data.
            PageWebForm form = pageResult.FindFormById("dataForm");

            // Assign values to the form fields.
            form["UserName"] = "AJSON";
            form["Gender"] = "M";
            form.Method = HttpVerb.Post;
            WebPage resultsPage = form.Submit();
        }
    }
}
