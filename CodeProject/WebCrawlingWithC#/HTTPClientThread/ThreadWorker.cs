﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;


namespace HTTPClientThread
{
    class ThreadWorker
    {
        // constructor for the thread. Takes param ID index of the Listview to keep track of.
        public ThreadWorker(SimpleObj rec)
        {
            _rec = rec;
        }

        public event EventHandler<WorkItemCompletedEventArgs> Completed;    // Event handler to be run when work has completed with a result.

        public void DoWork()
        {
            // Get new async op object calling forms sync context.
            _operation = AsyncOperationManager.CreateOperation(null);

            // Queue work so a thread from the thread pool can pick it up and execute it.
            ThreadPool.QueueUserWorkItem((o) => this.PerformWork(_rec));
        }

        private AsyncOperation _operation;      // async operation representing the work item
        private SimpleObj _rec;                 // variable to store the request and response details
        
        protected virtual void OnCompleted(WorkItemCompletedEventArgs args)
        {
            //raise the Completed event in the context of the form 
            EventHandler<WorkItemCompletedEventArgs> temp = Completed;
            if (temp != null)
            {
                temp.Invoke(this, args);
            }
        }

        private async void PerformWork(SimpleObj rec)
        {
            // Call the main work method and wait for it to return.
            SimpleObj resultObj = await Shared.SendWebRequest(rec);
            _rec.ResultCode = resultObj.ResultCode;
            _rec.XMLData = resultObj.XMLData;

            // Once completed, call the "post completed" method, passing in the result received.
            PostCompleted();
        }

        private void PostCompleted() 
        {
            // call OnCompleted, passing in the SimpleObj result to it, the lambda passed into this method is invoked in the context of the form UI
            _operation.PostOperationCompleted((o) => this.OnCompleted(new WorkItemCompletedEventArgs(_rec)), _rec);
        }
    }


    // Event argument that contains the result of each threads work to be passed back to the main UI thread
    public class WorkItemCompletedEventArgs : EventArgs
    {
        // Constructor takes parameters that will be used to store informaiton we need to update the main UI on completion
        public WorkItemCompletedEventArgs(SimpleObj result)
        {
            this.Result = result;
        }

        // SimpleObj is declared in the "Shared" file and used to pass simple data around
        public SimpleObj Result { get; set; }
    }
}
