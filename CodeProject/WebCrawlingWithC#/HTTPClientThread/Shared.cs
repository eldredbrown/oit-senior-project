﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http; //  <-- ensure you add a reference for this to the project
using System.Xml;
using System.Xml.Serialization;

// File contains shared vars and methods between form and worker thread


namespace HTTPClientThread
{
    public class SimpleObj
    {
        public string WebURL;       // web address to send post to
        public string ResultCode;   // 0 = failure, 1 = success
        public string XMLData;      // Used to store the html received back from our HTTPClient request
        public string Message;      // What we will show back to the user as response 
        public string ItemID;       // Used to store the ListView item ID/index so we can update it when the thread completes
    }


    class Shared 
    {
        public static async Task<SimpleObj> SendWebRequest(SimpleObj request)
        {
            SimpleObj result = request;
            var httpClient = new HttpClient();
            var content = new StringContent(request.ItemID); // we send the server the ItemID

            try
            {
                HttpResponseMessage response = await httpClient.PostAsync(request.WebURL, content);

                if (response.IsSuccessStatusCode)
                {
                    HttpContent stream = response.Content;
                    Task<string> data = stream.ReadAsStringAsync();
                    result.XMLData = data.Result.ToString();

                    var doc = new XmlDocument();
                    doc.LoadXml(result.XMLData);

                    XmlNode resultNode = doc.SelectSingleNode("response");
                    string resultStatus = resultNode.InnerText;

                    if (resultStatus == "1")
                        result.ResultCode = "OK";
                    else if (resultStatus == "0")
                        result.ResultCode = "ERR";

                    result.Message = doc.InnerXml;
                }

            }
            catch (Exception ex)
            {
                result.ResultCode = "ERR";
                result.Message = "Connection error: " + ex.Message;
            }

            return result;
        }
    }
}
