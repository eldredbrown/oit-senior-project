﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HTTPClientThread
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        public void CreateWorkThread(SimpleObj rec)
        {
            var item = new ThreadWorker(rec);

            // Subscribe to be notified when result is ready.
            item.Completed += workThread_Completed;
            item.DoWork();
        }

        // Fill the ListView with the count items.
        public void FillListView()
        {
            lvMain.Items.Clear();
            for (int i = 0; i < _runTimes; i++)
            {
                var item = new ListViewItem();
                item.Text = (i + 1).ToString();
                item.SubItems.Add("Pending");
                item.SubItems.Add("-");
                item.SubItems.Add("-");
                lvMain.Items.Add(item);
            }
        }

        // Set up some default values.
        public void InitProcess()
        {
            btnExit.Enabled = false;
            btnRunProcess.Enabled = false;
            chkRunThreaded.Enabled = false;

            _runTimes = int.Parse(edtTimesToRun.Text);

            FillListView();

            _runningThreadCount = 0;
        }

        public async void RunProcess()
        {
            for (int i = 0; i < _runTimes; i++)
            {
                UpdateStatusLabel("Processing: " + (i + 1).ToString() + "/" + _runTimes.ToString());

                lvMain.Items[i].Selected = true;
                lvMain.Items[i].EnsureVisible();
                lvMain.Items[i].SubItems[1].Text = "Processing...";

                SimpleObj result = await Shared.SendWebRequest(new SimpleObj() { ItemID = i.ToString(), WebURL = edtTestServer.Text });
                lvMain.Items[i].SubItems[1].Text = result.ResultCode;
                if (result.ResultCode == "ERR")
                    lvMain.Items[i].SubItems[2].Text = result.Message;
            }

            CleanUp();
        }

        public void RunProcessThreaded()
        {
            UpdateStatusLabel("Status: threaded mode - watch thread count and list status");
            lblThreadCount.Visible = true;

            for (int i = 0; i < _runTimes; i++)
            {
                UpdateStatusLabel("Processing: " + (i + 1).ToString() + "/" + _runTimes.ToString());
                lvMain.Items[i].Selected = true;
                lvMain.Items[i].SubItems[1].Text = "Processing...";

                var rec = new SimpleObj() { ItemID = i.ToString(), WebURL = edtTestServer.Text };
                CreateWorkThread(rec);

                _runningThreadCount++;

                UpdateThreadCount();
            }
        }

        public int UpdateTime()
        {
            var timeEnd = System.Environment.TickCount;
            var timeTaken = (timeEnd - _timeStart) / 1000;
            return timeTaken;
        }

        private int _runTimes;
        private int _runningThreadCount;
        private int _timeStart;

        private void btnRunProcess_Click(object sender, EventArgs e)
        {
            _timeStart = System.Environment.TickCount;

            InitProcess();

            if (chkRunThreaded.Checked)
            {
                RunProcessThreaded();
            }
            else
            {
                RunProcess();
            }
        }

        // Handler method to run when work has completed
        private void workThread_Completed(object sender, WorkItemCompletedEventArgs e)
        {
            lvMain.Items[int.Parse(e.Result.ItemID)].SubItems[1].Text = e.Result.ResultCode;
            if (e.Result.ResultCode == "ERR")
                lvMain.Items[int.Parse(e.Result.ItemID)].SubItems[2].Text = e.Result.Message;

            _runningThreadCount--;
            UpdateThreadCount();

            if (_runningThreadCount == 0)
            {
                CleanUp();
            }
        }


        private void CleanUp()
        {
            btnExit.Enabled = true;
            btnRunProcess.Enabled = true;
            chkRunThreaded.Enabled = true;
            lblThreadCount.Visible = false;
            UpdateStatusLabel("Status: complete  (Time taken: " + UpdateTime().ToString() + ")");
        }

        private void UpdateStatusLabel(string Status)
        {
            lblStatus.Text = Status;
            Application.DoEvents();
        }

        private void UpdateThreadCount()
        {
            lblThreadCount.Text = "Running threads: " + _runningThreadCount.ToString();
        }
    }
}
