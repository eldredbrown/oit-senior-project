﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the Rankings control.
	/// </summary>
	public class RankingsControlViewModel : ViewModelBase
	{
		#region Fields

		private readonly MutexesViewModel _mutexesViewModel;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a default instance of the RankingsControlViewModel class.
		/// </summary>
		public RankingsControlViewModel()
		{
			try
			{
				var classificationsSorted = (from classification in DataAccess.DbContext.Classifications
													  orderby classification.Name descending
													  select classification).ToArray();

				var mutexes = Enumerable.Range(0, classificationsSorted.Count()).
					 Select(i => new MutexViewModel(this) { Text = classificationsSorted[i].Name });

				_mutexesViewModel = new MutexesViewModel(mutexes);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets this control's mutex collection.
		/// </summary>
		public ObservableCollection<MutexViewModel> Mutexes
		{
			get { return _mutexesViewModel.Mutexes; }
		}

		/// <summary>
		/// Gets or sets this control's defensive rankings collection.
		/// </summary>
		private ObjectResult<GetRankingsDefensive_Result> _DefensiveRankingsResult;
		public ObjectResult<GetRankingsDefensive_Result> DefensiveRankingsResult
		{
			get { return _DefensiveRankingsResult; }
			set
			{
				Debug.Assert(value != null);

				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _DefensiveRankingsResult )
				{
					_DefensiveRankingsResult = value;
					OnPropertyChanged("DefensiveRankingsResult");
				}
			}
		}

		/// <summary>
		/// Gets or sets this control's offensive rankings collection.
		/// </summary>
		private ObjectResult<GetRankingsOffensive_Result> _OffensiveRankingsResult;
		public ObjectResult<GetRankingsOffensive_Result> OffensiveRankingsResult
		{
			get { return _OffensiveRankingsResult; }
			set
			{
				Debug.Assert(value != null);

				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _OffensiveRankingsResult )
				{
					_OffensiveRankingsResult = value;
					OnPropertyChanged("OffensiveRankingsResult");
				}
			}
		}

		/// <summary>
		/// Gets or sets this control's total rankings collection.
		/// </summary>
		private ObjectResult<GetRankingsTotal_Result> _TotalRankingsResult;
		public ObjectResult<GetRankingsTotal_Result> TotalRankingsResult
		{
			get { return _TotalRankingsResult; }
			set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _TotalRankingsResult )
				{
					_TotalRankingsResult = value;
					OnPropertyChanged("TotalRankingsResult");
				}
			}
		}

		#endregion
	}
}
