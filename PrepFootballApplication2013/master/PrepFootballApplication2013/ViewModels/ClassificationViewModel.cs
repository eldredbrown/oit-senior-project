﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the Classification tree view item.
	/// </summary>
	public class ClassificationViewModel : TreeViewItemViewModel
	{
		#region Fields

		private readonly Classification _classification;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a custom instance of the ClassificationViewModel class.
		/// </summary>
		/// <param name="parentControl"></param>
		/// <param name="classification"></param>
		public ClassificationViewModel(StateViewModel parentControl, Classification classification)
			: base(null, true)
		{
			// Validate arguments.
			if ( classification == null )
			{
				throw new ArgumentNullException("classification");
			}

			// Assign argument values to member fields.
			this.ParentControl = parentControl;
			_classification = classification;
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets the name of the Classification instance wrapped inside this ViewModel.
		/// </summary>
		public string ClassificationName
		{
			get { return _classification.Name; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the TreeViewItem associated with this object is expanded.
		/// </summary>
		public override bool IsExpanded
		{
			get
			{
				return base.IsExpanded;
			}
			set
			{
				base.IsExpanded = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
		/// </summary>
		public override bool IsSelected
		{
			get
			{
				return base.IsSelected;
			}
			set
			{
				base.IsSelected = value;
			}
		}

		/// <summary>
		/// Gets this object's parent control.
		/// </summary>
		private StateViewModel _ParentControl;
		public StateViewModel ParentControl
		{
			get { return _ParentControl; }
			private set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _ParentControl )
				{
					_ParentControl = value;
				}
			}
		}

		#endregion

		#region Instance Methods

		/// <summary>
		/// Loads this object's children into the TreeView.
		/// </summary>
		protected override void LoadChildren()
		{
			try
			{
				// Load collection of District objects for the Classification object wrapped inside this ViewModel.
				var districts = from district in DataAccess.DbContext.Districts
									 where district.ClassificationName == _classification.Name
									 && district.ID != Globals.Constants.OutOfStateDistrictString
 									 && district.ID != Globals.Constants.IndependentString
									 select district;

				foreach ( var district in districts )
				{
					Children.Add(new DistrictViewModel(this, district));
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion
	}
}
