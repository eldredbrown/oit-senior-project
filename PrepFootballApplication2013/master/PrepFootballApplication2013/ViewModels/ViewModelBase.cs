﻿using System;
using System.ComponentModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// Base class for all the ViewModel classes used in this project.
	/// </summary>
	public abstract class ViewModelBase : INotifyPropertyChanged
	{
		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the ViewModelBase class.
		/// </summary>
		protected ViewModelBase()
		{
		}

		#endregion

		#region Properties

		private bool _RequestUpdate;
		public bool RequestUpdate
		{
			get { return _RequestUpdate; }
			set
			{
				_RequestUpdate = value;
				OnPropertyChanged("RequestUpdate");
				_RequestUpdate = false;
			}
		}

		#endregion

		#region Commands

		public DelegateCommand UpdateCommand
		{
			get
			{
				return new DelegateCommand(param => { RequestUpdate = true; });
			}
		}

		#endregion

		#region Event Raisers

		/// <summary>
		/// Raise the PropertyChanged event.
		/// </summary>
		/// <param name="name"></param>
		protected void OnPropertyChanged(string name)
		{
			// Validate name argument.
			if ( String.IsNullOrEmpty(name) )
			{
				throw new ArgumentException("name");
			}

			// Raise event to all subscribed event handlers.
			var handler = PropertyChanged;
			if ( handler != null )
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

		#endregion

		#region INotifyPropertyChanged Members

		/// <summary>
		/// Event triggered when a property is changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion
	}
}
