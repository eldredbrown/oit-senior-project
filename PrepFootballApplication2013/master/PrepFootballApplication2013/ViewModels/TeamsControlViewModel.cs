﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the Teams control.
	/// </summary>
	public class TeamsControlViewModel : ViewModelBase
	{
		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the TeamsControlViewModel class.
		/// </summary>
		public TeamsControlViewModel()
		{
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets or sets this control's selected team.
		/// </summary>
		private Team _SelectedTeam;
		public Team SelectedTeam
		{
			get { return _SelectedTeam; }
			private set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _SelectedTeam )
				{
					_SelectedTeam = value;
					OnPropertyChanged("SelectedTeam");
				}
			}
		}

		/// <summary>
		/// Gets this control's teams collection.
		/// </summary>
		public ObservableCollection<Team> Teams
		{
			get { return DataAccess.DbContext.Teams.Local; }
		}

		/// <summary>
		/// Gets this control's collection of team schedule averages.
		/// </summary>
		private ObjectResult<GetTeamScheduleAverages_Result> _TeamScheduleAveragesResult;
		public ObjectResult<GetTeamScheduleAverages_Result> TeamScheduleAveragesResult
		{
			get { return _TeamScheduleAveragesResult; }
			private set
			{
				if ( value != _TeamScheduleAveragesResult )
				{
					_TeamScheduleAveragesResult = value;
					OnPropertyChanged("TeamScheduleAveragesResult");
				}
			}
		}

		/// <summary>
		/// Gets this control's team schedule profile collection.
		/// </summary>
		private ObjectResult<GetTeamScheduleProfile_Result> _TeamScheduleProfileResult;
		public ObjectResult<GetTeamScheduleProfile_Result> TeamScheduleProfileResult
		{
			get { return _TeamScheduleProfileResult; }
			private set
			{
				if ( value != _TeamScheduleProfileResult )
				{
					_TeamScheduleProfileResult = value;
					OnPropertyChanged("TeamScheduleProfileResult");
				}
			}
		}

		/// <summary>
		/// Gets this controls's collection of team schedule totals.
		/// </summary>
		private ObjectResult<GetTeamScheduleTotals_Result> _TeamScheduleTotalsResult;
		public ObjectResult<GetTeamScheduleTotals_Result> TeamScheduleTotalsResult
		{
			get { return _TeamScheduleTotalsResult; }
			private set
			{
				if ( value != _TeamScheduleTotalsResult )
				{
					_TeamScheduleTotalsResult = value;
					OnPropertyChanged("TeamScheduleTotalsResult");
				}
			}
		}

		#endregion

		#region Commands

		/// <summary>
		/// Loads the DataModel's Teams table.
		/// </summary>
		private DelegateCommand _viewTeamsCommand;
		public DelegateCommand ViewTeamsCommand
		{
			get
			{
				if ( _viewTeamsCommand == null )
				{
					_viewTeamsCommand = new DelegateCommand(param => ViewTeams());
				}
				return _viewTeamsCommand;
			}
		}
		private void ViewTeams()
		{
			try
			{
				// Load the DataModel's Teams table.
				DataAccess.DbContext.Teams.Load();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Views the team schedule profile, totals, and averages for the selected team.
		/// </summary>
		private DelegateCommand _viewTeamScheduleCommand;
		public DelegateCommand ViewTeamScheduleCommand
		{
			get
			{
				if ( _viewTeamScheduleCommand == null )
				{
					_viewTeamScheduleCommand = new DelegateCommand(param => ViewTeamSchedule());
				}
				return _viewTeamScheduleCommand;
			}
		}
		private void ViewTeamSchedule()
		{
			try
			{
				if ( SelectedTeam != null )
				{
					// Load data from TeamScheduleProfile, TeamScheduleTotals, and TeamScheduleAverages for selected team.
					var team = SelectedTeam.Name;

					TeamScheduleProfileResult = DataAccess.DbContext.GetTeamScheduleProfile(team);
					TeamScheduleTotalsResult = DataAccess.DbContext.GetTeamScheduleTotals(team);
					TeamScheduleAveragesResult = DataAccess.DbContext.GetTeamScheduleAverages(team);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion
	}
}
