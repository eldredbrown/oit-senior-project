﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using EldredBrown.PrepFootballApplication2013.DataModel;
using EldredBrown.PrepFootballApplication2013.Windows;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the MainWindow class.
	/// </summary>
	public class MainWindowViewModel : ViewModelBase
	{
		#region Fields

		private object _dbLock = new object();
		private GamesWindow _gamesWindow;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the MainWindowViewModel class.
		/// </summary>
		public MainWindowViewModel()
		{
		}

		#endregion

		#region Commands

		/// <summary>
		/// Shows the Games window.
		/// </summary>
		private DelegateCommand _ShowGamesCommand;
		public DelegateCommand ShowGamesCommand
		{
			get
			{
				if ( _ShowGamesCommand == null )
				{
					_ShowGamesCommand = new DelegateCommand(param => ShowGames());
				}
				return _ShowGamesCommand;
			}
		}
		private void ShowGames()
		{
			try
			{ 
				// Show the Games window.
				_gamesWindow = new GamesWindow();
				_gamesWindow.Show();

				// Load the EntityDataModel representation of the Games table.
				DataAccess.DbContext.Games.Load();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex, "Exception");
			}
		}

		/// <summary>
		/// Runs a weekly update.
		/// </summary>
		private DelegateCommand _WeeklyUpdateCommand;
		public DelegateCommand WeeklyUpdateCommand
		{
			get
			{
				if ( _WeeklyUpdateCommand == null )
				{
					_WeeklyUpdateCommand = new DelegateCommand(param => RunWeeklyUpdate(),
						 param => { return DataAccess.DbContext.Games.Count() > 0; });
				}
				return _WeeklyUpdateCommand;
			}
		}
		private void RunWeeklyUpdate()
		{
			try
			{
				// I experimented with farming this long running operation out to a separate thread to improve UI responsiveness,
				// but I eventually concluded that I actually DON'T want the UI to be responsive while this update operation
				// is running. An attempt to view data that's in the process of changing may cause the application to crash, and
				// the data won't mean anything, anyway.
				MessageBoxResult dlgResult = MessageBox.Show(
					"This operation will make the UI unresponsive for a minute or two. Are you sure you want to continue?",
					Globals.Constants.AppName, MessageBoxButton.YesNo, MessageBoxImage.Question);
				if ( dlgResult == MessageBoxResult.No )
				{
					return;
				}

				foreach ( var classification in DataAccess.DbContext.Classifications )
				{
					var classificationTotalsRow = DataAccess.DbContext.GetClassificationTotals(classification.Name).First();

					classification.TotalGames = (decimal)classificationTotalsRow.TotalGames;
					classification.TotalAdjustedPointsFor = (decimal)classificationTotalsRow.TotalAdjustedPointsFor;
					classification.AverageAdjustedPointsFor = Arithmetic.Divide(
						(decimal)classificationTotalsRow.TotalAdjustedPointsFor, (decimal)classificationTotalsRow.TotalGames);
				}

				var srcWeekCount = (from game in DataAccess.DbContext.Games select game.Week).Max();
				var destWeekCount = (from w in DataAccess.DbContext.WeekCounts select w).FirstOrDefault();
				destWeekCount.Value = srcWeekCount;

				DataAccess.DbContext.SaveChanges();

				if ( srcWeekCount >= 3 )
				{
					UpdateSecondAndThirdLevelData();
				}

				MessageBox.Show("Weekly update completed.", Globals.Constants.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Private Methods

		private void UpdateFinalScoringDataByTeam(Team team, GetTeamScheduleTotals_Result result)
		{
			try
			{
				if ( result.ScheduleTotalGames != null )
				{
					Classification classification;

					lock ( _dbLock )
					{
						classification = (from c in DataAccess.DbContext.Classifications
												where c.Name == team.ClassificationName
												select c)
												.FirstOrDefault();
					}

					team.FinalOffensiveAverage = Arithmetic.Divide(
						 team.InClassificationAdjustedPointsFor * (decimal)result.ScheduleTotalGames
						 * classification.TotalAdjustedPointsFor * team.InClassificationGames
						 + team.InClassificationGames * (decimal)result.ScheduleTotalAdjustedPointsAgainst
						 * classification.TotalGames * team.OutOfClassificationAdjustedPointsFor,
						 team.InClassificationGames * (decimal)result.ScheduleTotalAdjustedPointsAgainst
						 * classification.TotalGames * team.Games);
					team.FinalDefensiveAverage = Arithmetic.Divide(
						 team.InClassificationAdjustedPointsAgainst * (decimal)result.ScheduleTotalGames
						 * classification.TotalAdjustedPointsFor * team.InClassificationGames
						 + team.InClassificationGames * (decimal)result.ScheduleTotalAdjustedPointsFor
						 * classification.TotalGames * team.OutOfClassificationAdjustedPointsAgainst,
						 team.InClassificationGames * (decimal)result.ScheduleTotalAdjustedPointsFor
						 * classification.TotalGames * team.Games);
					team.FinalOffensiveFactor = Arithmetic.Divide(
						 team.InClassificationAdjustedPointsFor * (decimal)result.ScheduleTotalGames,
						 team.InClassificationGames * (decimal)result.ScheduleTotalFinalAdjustedPointsAgainst);
					team.FinalDefensiveFactor = Arithmetic.Divide(
						 team.InClassificationAdjustedPointsAgainst * (decimal)result.ScheduleTotalGames,
						 team.InClassificationGames * (decimal)result.ScheduleTotalFinalAdjustedPointsFor);
					team.FinalOffensiveIndex
						= (team.FinalOffensiveAverage + team.FinalOffensiveFactor * classification.AverageAdjustedPointsFor) / 2;
					team.FinalDefensiveIndex
						= (team.FinalDefensiveAverage + team.FinalDefensiveFactor * classification.AverageAdjustedPointsFor) / 2;
					team.FinalScoringRatio = Arithmetic.Divide(
						(decimal)team.FinalOffensiveIndex, (decimal)team.FinalOffensiveIndex + (decimal)team.FinalDefensiveIndex);

					lock ( _dbLock )
					{
						DataAccess.DbContext.SaveChanges();
					}
				}
			}
			catch ( InvalidOperationException ex )
			{
				if ( ex.Message == "Nullable object must have a value." )
				{
					// Ignore exception.
				}
				else
				{
					Globals.ShowExceptionMessage(ex, "InvalidOperationException - " + team.Name);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex, "Exception - " + team.Name);
			}
		}

		private void UpdateSecondAndThirdLevelData()
		{
			try
			{
				// This looks like the place where I want to make maximum use of parallel threading.
				Parallel.ForEach(DataAccess.DbContext.Teams, team => UpdateSecondAndThirdLevelDataByTeam(team));
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		private void UpdateSecondAndThirdLevelDataByTeam(Team team)
		{
			try
			{
				GetTeamScheduleTotals_Result result = null;

				lock ( _dbLock )
				{
					result = DataAccess.DbContext.GetTeamScheduleTotals(team.Name).FirstOrDefault();
				}

				if ( result != null )
				{
					UpdateSecondLevelDataByTeam(team, result);
					UpdateThirdLevelDataByTeam(team, result);
					UpdateFinalScoringDataByTeam(team, result);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex); 
			}
		}

		private void UpdateSecondLevelDataByTeam(Team team, GetTeamScheduleTotals_Result result)
		{
			try
			{
				if ( result.TotalRatingPointsSecondLevel != null )
				{
					// Rating Points Second Level total and average
					team.RatingPointsSecondLevel = (decimal)result.TotalRatingPointsSecondLevel;
					team.RatingPointsSecondLevelAverage = Arithmetic.Divide(team.RatingPointsSecondLevel, team.RatingPointGames);
					team.RatingPointsIndex
						= Arithmetic.Divide(team.RatingPointsFirstLevel + team.RatingPointsSecondLevel, team.RatingPointGames);

					lock ( _dbLock )
					{
						DataAccess.DbContext.SaveChanges();
					}
				}
			}
			catch ( InvalidOperationException ex )
			{
				if ( ex.Message == "Nullable object must have a value." )
				{
					// Ignore exception.
				}
				else
				{
					Globals.ShowExceptionMessage(ex, "InvalidOperationException - " + team.Name);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex, "Exception - " + team.Name);
			}
		}

		private void UpdateThirdLevelDataByTeam(Team team, GetTeamScheduleTotals_Result result)
		{
			try
			{
				if ( result.TotalRatingPointsThirdLevel != null )
				{
					// Rating Points Third Level total and average
					team.RatingPointsThirdLevel = (decimal)result.TotalRatingPointsThirdLevel;
					team.RatingPointsThirdLevelAverage = Arithmetic.Divide(team.RatingPointsThirdLevel, team.RatingPointGames);

					lock ( _dbLock )
					{
						DataAccess.DbContext.SaveChanges();
					}
				}
			}
			catch ( InvalidOperationException ex )
			{
				if ( ex.Message == "Nullable object must have a value." )
				{
					// Ignore exception.
				}
				else
				{
					Globals.ShowExceptionMessage(ex, "InvalidOperationException - " + team.Name);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex, "Exception - " + team.Name);
			}
		}

		#endregion
	}
}
