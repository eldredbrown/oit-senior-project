﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the game finder window.
	/// </summary>
	class GameFinderWindowViewModel : ViewModelBase
	{
		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the GameFinderWindowViewModel class.
		/// </summary>
		public GameFinderWindowViewModel()
		{
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets or sets this window's guest value.
		/// </summary>
		private string _Guest;
		public string Guest
		{
			get { return _Guest; }
			set
			{
				if ( value != _Guest )
				{
					_Guest = value;
					OnPropertyChanged("Guest");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's host value.
		/// </summary>
		private string _Host;
		public string Host
		{
			get { return _Host; }
			set
			{
				if ( value != _Host )
				{
					_Host = value;
					OnPropertyChanged("Host");
				}
			}
		}

		#endregion

		#region Instance Methods

		/// <summary>
		/// Validates data entered into the data entry controls.
		/// </summary>
		public void ValidateDataEntry()
		{
			if ( String.IsNullOrWhiteSpace(Guest) || String.IsNullOrWhiteSpace(Host) )
			{
				throw new DataValidationException(Globals.Constants.BothTeamsNeededErrorMessage);
			}
			else if ( Guest == Host )
			{
				throw new DataValidationException(Globals.Constants.DifferentTeamsNeededErrorMessage);
			}
		}

		#endregion
	}
}
