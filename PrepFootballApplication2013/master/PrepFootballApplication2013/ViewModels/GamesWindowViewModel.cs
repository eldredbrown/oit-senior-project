﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using EldredBrown.PrepFootballApplication2013.DataModel;
using EldredBrown.PrepFootballApplication2013.ViewModels.FocusVMLib;
using EldredBrown.PrepFootballApplication2013.Windows;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the Games window.
	/// </summary>
	public class GamesWindowViewModel : ViewModelBase, IFocusMover
	{
		#region Events & Delegates

		public delegate decimal Operation(decimal lVal, decimal rVal);

		#endregion

		#region Fields

		private static GameType _gameType;

		private bool _isFindGameFilterApplied;
		private GameFinderWindow _gameFinderWindow;

		#endregion

		#region Constructors & Finalizers

		public GamesWindowViewModel()
		{
			try
			{
				SortGamesByDefaultOrder();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Instance Properties & Commands

		#region Properties

		/// <summary>
		/// Gets the visibility of the AddGame control
		/// </summary>
		private Visibility _AddGameControlVisibility;
		public Visibility AddGameControlVisibility
		{
			get { return _AddGameControlVisibility; }
			private set
			{
				if ( value != _AddGameControlVisibility )
				{
					_AddGameControlVisibility = value;
					OnPropertyChanged("AddGameControlVisibility");
				}
			}
		}

		/// <summary>
		/// Gets the visibility of the EditGame control.
		/// </summary>
		private Visibility _EditGameControlVisibility;
		public Visibility EditGameControlVisibility
		{
			get { return _EditGameControlVisibility; }
			private set
			{
				if ( value != _EditGameControlVisibility )
				{
					_EditGameControlVisibility = value;
					OnPropertyChanged("EditGameControlVisibility");
				}
			}
		}

		/// <summary>
		/// Gets this window's games collection. 
		/// </summary>
		private ObservableCollection<Game> _Games;
		public ObservableCollection<Game> Games
		{
			get { return _Games; }
			private set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _Games )
				{
					_Games = value;
					OnPropertyChanged("Games");
					RequestUpdate = true;
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's guest value.
		/// </summary>
		private string _Guest;
		public string Guest
		{
			get { return _Guest; }
			set
			{
				if ( value != _Guest )
				{
					_Guest = value;
					OnPropertyChanged("Guest");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's guest adjusted score value.
		/// </summary>
		private int _GuestAdjustedScore;
		public int GuestAdjustedScore
		{
			get { return _GuestAdjustedScore; }
			set
			{
				if ( value != _GuestAdjustedScore )
				{
					_GuestAdjustedScore = value;
					OnPropertyChanged("GuestAdjustedScore");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's guest score value.
		/// </summary>
		private int _GuestScore;
		public int GuestScore
		{
			get { return _GuestScore; }
			set
			{
				if ( value != _GuestScore )
				{
					_GuestScore = value;
					OnPropertyChanged("GuestScore");
					GuestAdjustedScore = _GuestScore;
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's host value.
		/// </summary>
		private string _Host;
		public string Host
		{
			get { return _Host; }
			set
			{
				if ( value != _Host )
				{
					_Host = value;
					OnPropertyChanged("Host");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's host adjusted score value.
		/// </summary>
		private int _HostAdjustedScore;
		public int HostAdjustedScore
		{
			get { return _HostAdjustedScore; }
			set
			{
				if ( value != _HostAdjustedScore )
				{
					_HostAdjustedScore = value;
					OnPropertyChanged("HostAdjustedScore");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's host score value.
		/// </summary>
		private int _HostScore;
		public int HostScore
		{
			get { return _HostScore; }
			set
			{
				if ( value != _HostScore )
				{
					_HostScore = value;
					OnPropertyChanged("HostScore");
					HostAdjustedScore = _HostScore;
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether this game is a district game.
		/// </summary>
		private bool _IsDistrictGame;
		public bool IsDistrictGame
		{
			get { return _IsDistrictGame; }
			set
			{
				if ( value != _IsDistrictGame )
				{
					_IsDistrictGame = value;
					OnPropertyChanged("IsDistrictGame");

					// A game cannot be both a district game and a playoff game,
					// so if IsDistrictGame is true, then set IsPlayoffGame to false.
					if ( _IsDistrictGame )
					{
						IsPlayoffGame = false;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether this game can be a district game.
		/// </summary>
		private bool _IsDistrictGameEnabled;
		public bool IsDistrictGameEnabled
		{
			get { return _IsDistrictGameEnabled; }
			set
			{
				if ( value != _IsDistrictGameEnabled )
				{
					_IsDistrictGameEnabled = value;
					OnPropertyChanged("IsDistrictGameEnabled");
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether this game was forfeited.
		/// </summary>
		private bool _IsForfeit;
		public bool IsForfeit
		{
			get { return _IsForfeit; }
			set
			{
				if ( value != _IsForfeit )
				{
					_IsForfeit = value;
					OnPropertyChanged("IsForfeit");
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether the Games collection is read-only.
		/// </summary>
		private bool _IsGamesReadOnly;
		public bool IsGamesReadOnly
		{
			get { return _IsGamesReadOnly; }
			set
			{
				if ( value != _IsGamesReadOnly )
				{
					_IsGamesReadOnly = value;
					OnPropertyChanged("IsGamesReadOnly");
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether this game is a playoff game.
		/// </summary>
		private bool _IsPlayoffGame;
		public bool IsPlayoffGame
		{
			get { return _IsPlayoffGame; }
			set
			{
				if ( value != _IsPlayoffGame )
				{
					_IsPlayoffGame = value;
					OnPropertyChanged("IsPlayoffGame");

					// A game cannot be both a district game and a playoff game,
					// so if IsPlayoffGame is true, then set IsDistrictGame to false.
					if ( _IsPlayoffGame )
					{
						IsDistrictGame = false;
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether this game can be a playoff game.
		/// </summary>
		private bool _IsPlayoffGameEnabled;
		public bool IsPlayoffGameEnabled
		{
			get { return _IsPlayoffGameEnabled; }
			set
			{
				if ( value != _IsPlayoffGameEnabled )
				{
					_IsPlayoffGameEnabled = value;
					OnPropertyChanged("IsPlayoffGameEnabled");
				}
			}
		}

		/// <summary>
		/// Gets or sets the value that indicates whether the ShowAllGames function is enabled.
		/// </summary>
		private bool _IsShowAllGamesEnabled;
		public bool IsShowAllGamesEnabled
		{
			get { return _IsShowAllGamesEnabled; }
			set
			{
				if ( value != _IsShowAllGamesEnabled )
				{
					_IsShowAllGamesEnabled = value;
					OnPropertyChanged("IsShowAllGamesEnabled");
				}
			}
		}

		/// <summary>
		/// Gets or sets the notes entered for this game, if any.
		/// </summary>
		private string _Notes;
		public string Notes
		{
			get { return _Notes; }
			set
			{
				if ( value != _Notes )
				{
					_Notes = value;
					OnPropertyChanged("Notes");
				}
			}
		}

		/// <summary>
		/// Gets the visibility of the RemoveGame control.
		/// </summary>
		private Visibility _RemoveGameControlVisibility;
		public Visibility RemoveGameControlVisibility
		{
			get { return _RemoveGameControlVisibility; }
			private set
			{
				if ( value != _RemoveGameControlVisibility )
				{
					_RemoveGameControlVisibility = value;
					OnPropertyChanged("RemoveGameControlVisibility");
				}
			}
		}

		/// <summary>
		/// Gets or sets this window's selected game
		/// </summary>
		private Game _SelectedGame;
		public Game SelectedGame
		{
			get { return _SelectedGame; }
			set
			{
				if ( value != _SelectedGame )
				{
					_SelectedGame = value;
					OnPropertyChanged("SelectedGame");

					if ( _SelectedGame == null )
					{
						ClearDataEntryControls();
					}
					else
					{
						PopulateDataEntryControls();
					}
				}
			}
		}

		/// <summary>
		/// Gets/sets this window's week value.
		/// </summary>
		private int _Week;
		public int Week
		{
			get { return _Week; }
			set
			{
				if ( value != _Week )
				{
					_Week = value;
					OnPropertyChanged("Week");
				}
			}
		}

		#endregion

		#region Commands

		/// <summary>
		/// Adds a new game to the database.
		/// </summary>
		private DelegateCommand _AddGameCommand;
		public DelegateCommand AddGameCommand
		{
			get
			{
				if ( _AddGameCommand == null )
				{
					_AddGameCommand = new DelegateCommand(param => AddGame());
				}
				return _AddGameCommand;
			}
		}
		private void AddGame()
		{
			try
			{
				ValidateDataEntry();
			}
			catch ( DataValidationException ex )
			{
				Globals.ShowExceptionMessage(ex);
				return;
			}

			try
			{
				var newGame = StoreNewGameValues();

				// Add this game to the Game's table.
				DataAccess.DbContext.Games.Add(newGame);

				EditTeams(newGame, Direction.Up);
				SaveChanges();

				// Sort Games collection so that the most recently added games appear at the top.
				// I find this good for bookmarking my progress.
				var games = (from game in DataAccess.DbContext.Games
								 select game)
								.OrderByDescending(g => g.Week)
								.ThenByDescending(g => g.ID);
				LoadGames(games);
				SelectedGame = null;
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits an existing game in the database.
		/// </summary>
		private DelegateCommand _EditGameCommand;
		public DelegateCommand EditGameCommand
		{
			get
			{
				if ( _EditGameCommand == null )
				{
					_EditGameCommand = new DelegateCommand(param => EditGame());
				}
				return _EditGameCommand;
			}
		}
		private void EditGame()
		{
			try
			{
				ValidateDataEntry();
			}
			catch ( DataValidationException ex )
			{
				Globals.ShowExceptionMessage(ex);
				return;
			}

			try
			{
				var oldGame = StoreOldGameValues();
				EditTeams(oldGame, Direction.Down);

				var newGame = StoreNewGameValues();

				// Edit selected row of Games table.
				// Replace property values of Game instance selected in DataGrid with values from newGame.
				SelectedGame.Week = newGame.Week;
				SelectedGame.Guest = newGame.Guest;
				SelectedGame.GuestScore = newGame.GuestScore;
				SelectedGame.GuestAdjustedScore = newGame.GuestAdjustedScore;
				SelectedGame.Host = newGame.Host;
				SelectedGame.HostScore = newGame.HostScore;
				SelectedGame.HostAdjustedScore = newGame.HostAdjustedScore;
				SelectedGame.IsDistrictGame = newGame.IsDistrictGame;
				SelectedGame.IsPlayoffGame = newGame.IsPlayoffGame;
				SelectedGame.IsForfeit = newGame.IsForfeit;
				SelectedGame.Notes = newGame.Notes;
				SelectedGame.WinnerScore = newGame.WinnerScore;
				SelectedGame.WinnerAdjustedScore = newGame.WinnerAdjustedScore;
				SelectedGame.WinnerRatingPoints = newGame.WinnerRatingPoints;
				SelectedGame.LoserScore = newGame.LoserScore;
				SelectedGame.LoserAdjustedScore = newGame.LoserAdjustedScore;

				EditTeams(newGame, Direction.Up);

				// Save changes and refresh view object bound to the Games table.
				SaveChanges();

				if ( _isFindGameFilterApplied )
				{
					ApplyFindGameFilter();
				}
				else
				{
					SortGamesByDefaultOrder();
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Searches for a specified game in the database.
		/// </summary>
		private DelegateCommand _FindGameCommand;
		public DelegateCommand FindGameCommand
		{
			get
			{
				if ( _FindGameCommand == null )
				{
					_FindGameCommand = new DelegateCommand(param => FindGame());
				}
				return _FindGameCommand;
			}
		}
		private void FindGame()
		{
			try
			{
				_gameFinderWindow = new GameFinderWindow();
				var dlgResult = _gameFinderWindow.ShowDialog();

				if ( dlgResult == true )
				{
					ApplyFindGameFilter();
					IsGamesReadOnly = true;
					IsShowAllGamesEnabled = true;

					if ( Games.Count == 0 )
					{
						SelectedGame = null;
					}

					AddGameControlVisibility = Visibility.Hidden;
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Removes an existing game from the database.
		/// </summary>
		private DelegateCommand _RemoveGameCommand;
		public DelegateCommand RemoveGameCommand
		{
			get
			{
				if ( _RemoveGameCommand == null )
				{
					_RemoveGameCommand = new DelegateCommand(param => RemoveGame());
				}
				return _RemoveGameCommand;
			}
		}
		private void RemoveGame()
		{
			try
			{
				var oldGame = StoreOldGameValues();

				// Delete matching row in the dataset.Games table
				DataAccess.DbContext.Games.Remove(SelectedGame);

				EditTeams(oldGame, Direction.Down);
				SaveChanges();
				SelectedGame = null;
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}

		}

		/// <summary>
		/// Shows all the games currently in the database.
		/// </summary>
		private DelegateCommand _ShowAllGamesCommand;
		public DelegateCommand ShowAllGamesCommand
		{
			get
			{
				if ( _ShowAllGamesCommand == null )
				{
					_ShowAllGamesCommand = new DelegateCommand(param => ShowAllGames());
				}
				return _ShowAllGamesCommand;
			}
		}
		private void ShowAllGames()
		{
			try
			{
				Games = DataAccess.DbContext.Games.Local;
				_isFindGameFilterApplied = false;
				IsGamesReadOnly = false;
				IsShowAllGamesEnabled = false;

				SelectedGame = null;
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Validates Name names entered into team fields to determine whether this game can be a district game or playoff game.
		/// </summary>
		private DelegateCommand _ValidateTeamsCommand;
		public DelegateCommand ValidateTeamsCommand
		{
			get
			{
				if ( _ValidateTeamsCommand == null )
				{
					_ValidateTeamsCommand = new DelegateCommand(param => ValidateTeams());
				}
				return _ValidateTeamsCommand;
			}
		}
		private void ValidateTeams()
		{
			try
			{
				Team guest = (from team in DataAccess.DbContext.Teams
								  where team.Name == Guest
								  select team)
								 .FirstOrDefault();
				Team host = (from team in DataAccess.DbContext.Teams
								 where team.Name == Host
								 select team)
								 .FirstOrDefault();

				if ( IsGameOutOfClass(guest, host) )
				{
					// One of the teams is out of state, or both teams are in different classes, or one of the teams is an independent.
					IsDistrictGameEnabled = false;
					IsPlayoffGameEnabled = false;

					IsDistrictGame = false;
					IsPlayoffGame = false;
				}
				else if ( guest.District != host.District )
				{
					// Both teams are in state, in the same class, but in different districts.
					IsDistrictGameEnabled = false;
					IsPlayoffGameEnabled = true;

					IsDistrictGame = false;
					if ( SelectedGame == null )
					{
						IsPlayoffGame = false;
					}
					else
					{
						IsPlayoffGame = SelectedGame.IsPlayoffGame;
					}
				}
				else
				{
					// Both teams are in the same district.
					IsDistrictGameEnabled = true;
					IsPlayoffGameEnabled = true;

					if ( SelectedGame == null )
					{
						IsDistrictGame = true;
						IsPlayoffGame = false;
					}
					else
					{
						IsDistrictGame = SelectedGame.IsDistrictGame;
						IsPlayoffGame = SelectedGame.IsPlayoffGame;
					}
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Views the Games database table.
		/// </summary>
		private DelegateCommand _ViewGamesCommand;
		public DelegateCommand ViewGamesCommand
		{
			get
			{
				if ( _ViewGamesCommand == null )
				{
					_ViewGamesCommand = new DelegateCommand(param => ViewGames());
				}
				return _ViewGamesCommand;
			}
		}
		private void ViewGames()
		{
			try
			{
				// Load the DataModel's Games table.
				DataAccess.DbContext.Games.Load();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#endregion

		#region IFocusMover Members

		public event EventHandler<MoveFocusEventArgs> MoveFocus;

		#endregion

		#region Private Methods

		#region Helper Methods

		#region Static Helpers

		/// <summary>
		/// Edits each team's data (games, points for, points against, adjusted points for, adjusted points against) from all games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="winnerScore"></param>
		/// <param name="loserScore"></param>
		/// <param name="winnerAdjustedScore"></param>
		/// <param name="loserAdjustedScore"></param>
		private static void EditDataFromAllGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				EditWinLossDataFromAllGames(winner, loser, operation, currentGame);

				if ( _gameType != GameType.ForfeitForFailureToAppear )
				{
					if ( winner != null )
					{
						winner.Games = operation(winner.Games, 1);
						winner.PointsFor = operation(winner.PointsFor, currentGame.WinnerScore);
						winner.PointsAgainst = operation(winner.PointsAgainst, currentGame.LoserScore);
						winner.AdjustedPointsFor = operation(winner.AdjustedPointsFor, currentGame.WinnerAdjustedScore);
						winner.AdjustedPointsAgainst = operation(winner.AdjustedPointsAgainst, currentGame.LoserAdjustedScore);
					}

					if ( loser != null )
					{
						loser.Games = operation(loser.Games, 1);
						loser.PointsFor = operation(loser.PointsFor, currentGame.LoserScore);
						loser.PointsAgainst = operation(loser.PointsAgainst, currentGame.WinnerScore);
						loser.AdjustedPointsFor = operation(loser.AdjustedPointsFor, currentGame.LoserAdjustedScore);
						loser.AdjustedPointsAgainst = operation(loser.AdjustedPointsAgainst, currentGame.WinnerAdjustedScore);
					}
				}
			}
			catch(Exception ex)
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (games, points for, points against) from district games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		/// <param name="winnerScore"></param>
		/// <param name="loserScore"></param>
		private static void EditDataFromDistrictGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				EditWinLossDataFromDistrictGames(winner, loser, operation, currentGame);

				if ( _gameType != GameType.ForfeitForFailureToAppear )
				{
					winner.DistrictGames = operation(winner.DistrictGames, 1);
					winner.DistrictPointsFor = operation(winner.DistrictPointsFor, currentGame.WinnerScore);
					winner.DistrictPointsAgainst = operation(winner.DistrictPointsAgainst, currentGame.LoserScore);

					loser.DistrictGames = operation(loser.DistrictGames, 1);
					loser.DistrictPointsFor = operation(loser.DistrictPointsFor, currentGame.LoserScore);
					loser.DistrictPointsAgainst = operation(loser.DistrictPointsAgainst, currentGame.WinnerScore);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (games, adjusted points for, adjusted point against)
		/// from all games against opponents in their classification.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="winnerAdjustedScore"></param>
		/// <param name="loserAdjustedScore"></param>
		private static void EditDataFromGamesInClassification(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				if ( _gameType != GameType.ForfeitForFailureToAppear )
				{
					winner.InClassificationGames = operation(winner.InClassificationGames, 1);
					winner.InClassificationAdjustedPointsFor
						= operation(winner.InClassificationAdjustedPointsFor, currentGame.WinnerAdjustedScore);
					winner.InClassificationAdjustedPointsAgainst
						= operation(winner.InClassificationAdjustedPointsAgainst, currentGame.LoserAdjustedScore);

					loser.InClassificationGames = operation(loser.InClassificationGames, 1);
					loser.InClassificationAdjustedPointsFor
						= operation(loser.InClassificationAdjustedPointsFor, currentGame.LoserAdjustedScore);
					loser.InClassificationAdjustedPointsAgainst
						= operation(loser.InClassificationAdjustedPointsAgainst, currentGame.WinnerAdjustedScore);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (games, adjusted points for, adjusted point against) from all games
		/// against teams outside the state or outside their classification
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="winnerAdjustedScore"></param>
		/// <param name="loserAdjustedScore"></param>
		private static void EditDataFromGamesOutOfClassification(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				if ( _gameType != GameType.ForfeitForFailureToAppear )
				{
					if ( winner != null )
					{
						winner.OutOfClassificationGames = operation(winner.OutOfClassificationGames, 1);
						winner.OutOfClassificationAdjustedPointsFor
							= operation(winner.OutOfClassificationAdjustedPointsFor, currentGame.WinnerAdjustedScore);
						winner.OutOfClassificationAdjustedPointsAgainst
							= operation(winner.OutOfClassificationAdjustedPointsAgainst, currentGame.LoserAdjustedScore);
					}

					if ( loser != null )
					{
						loser.OutOfClassificationGames = operation(loser.OutOfClassificationGames, 1);
						loser.OutOfClassificationAdjustedPointsFor
							= operation(loser.OutOfClassificationAdjustedPointsFor, currentGame.LoserAdjustedScore);
						loser.OutOfClassificationAdjustedPointsAgainst
							= operation(loser.OutOfClassificationAdjustedPointsAgainst, currentGame.WinnerAdjustedScore);
					}
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (games, points for, points against) from playoff games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		/// <param name="winnerScore"></param>
		/// <param name="loserScore"></param>
		private static void EditDataFromPlayoffGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				EditWinLossDataFromPlayoffGames(winner, loser, operation, currentGame);

				if ( _gameType == GameType.NonForfeit )
				{
					winner.PlayoffGames = operation(winner.PlayoffGames, 1);
					winner.PlayoffPointsFor = operation(winner.PlayoffPointsFor, currentGame.WinnerScore);
					winner.PlayoffPointsAgainst = operation(winner.PlayoffPointsAgainst, currentGame.LoserScore);

					loser.PlayoffGames = operation(loser.PlayoffGames, 1);
					loser.PlayoffPointsFor = operation(loser.PlayoffPointsFor, currentGame.LoserScore);
					loser.PlayoffPointsAgainst = operation(loser.PlayoffPointsAgainst, currentGame.WinnerScore);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits the DataModel's Teams table with data from the specified game.
		/// </summary>
		/// <param name="direction"></param>
		private static void EditTeams(Game currentGame, Direction direction)
		{
			try
			{
				Operation operation;

				// Decide whether the teams need to be edited up or down.
				// Up for new game, down then up for edited game, down for deleted game.
				switch ( direction )
				{
					case Direction.Up:
						operation = new Operation(Arithmetic.Add);
						break;

					case Direction.Down:
						operation = new Operation(Arithmetic.Subtract);
						break;

					default:
						throw new ArgumentException("direction");
				}

				ProcessGame(currentGame, operation);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (wins, losses, and first level rating points) from all games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		private static void EditWinLossDataFromAllGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				if ( winner != null )
				{
					winner.Wins = operation(winner.Wins, 1);

					if ( _gameType == GameType.NonForfeit )
					{
						winner.RatingPointGames = operation(winner.RatingPointGames, 1);
						winner.RatingPointsFirstLevel = operation(winner.RatingPointsFirstLevel, currentGame.WinnerRatingPoints);
						winner.RatingPointsFirstLevelAverage = Arithmetic.Divide(winner.RatingPointsFirstLevel, winner.RatingPointGames);
					}
				}

				if ( loser != null )
				{
					loser.Losses = operation(loser.Losses, 1);

					loser.RatingPointGames = operation(loser.RatingPointGames, 1);
					// No need to increment/decrement loser.RatingPointsFirstLevel, 
					// for the operation will always be y = x + 0 or y = x - 0.
					loser.RatingPointsFirstLevelAverage = Arithmetic.Divide(loser.RatingPointsFirstLevel, loser.RatingPointGames);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (wins, losses) from district games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		private static void EditWinLossDataFromDistrictGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				winner.DistrictWins = operation(winner.DistrictWins, 1);
				loser.DistrictLosses = operation(loser.DistrictLosses, 1);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Edits each team's data (wins, losses) from playoff games.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		private static void EditWinLossDataFromPlayoffGames(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				if ( _gameType != GameType.ForfeitForRulesViolation )
				{
					winner.PlayoffWins = operation(winner.PlayoffWins, 1);
					loser.PlayoffLosses = operation(loser.PlayoffLosses, 1);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Determines if a game is out of class based on properties of guest and host arguments.
		/// </summary>
		/// <param name="teamA"></param>
		/// <param name="teamB"></param>
		/// <returns></returns>
		private static bool IsGameOutOfClass(Team teamA, Team teamB)
		{
			return (teamA == null) || (teamB == null)
				|| (teamA.DistrictID == Globals.Constants.OutOfStateDistrictString)
				|| (teamB.DistrictID == Globals.Constants.OutOfStateDistrictString)
				|| (teamA.ClassificationName != teamB.ClassificationName)
				|| (teamA.DistrictID == Globals.Constants.IndependentString)
				|| (teamB.DistrictID == Globals.Constants.IndependentString);
		}

		/// <summary>
		/// Processes a game.
		/// </summary>
		/// <param name="operation"></param>
		private static void ProcessGame(Game currentGame, Operation operation)
		{
			try
			{
				_gameType = currentGame.GetGameType();

				if ( _gameType == GameType.ForfeitForRulesViolation )
				{
					// In a forfeit for a rules violation, the team originally declared the winner is required to
					// have their win exchanged for a loss, with the win going to their opponent. With this in mind,
					// the next few steps make the winner the loser and the loser the winner.
					string temp = currentGame.Loser;
					currentGame.Loser = currentGame.Winner;
					currentGame.Winner = temp;

					decimal tempScore = currentGame.LoserScore;
					currentGame.LoserScore = currentGame.WinnerScore;
					currentGame.WinnerScore = tempScore;

					decimal tempAdjustedScore = currentGame.LoserAdjustedScore;
					currentGame.LoserAdjustedScore = currentGame.WinnerAdjustedScore;
					currentGame.WinnerAdjustedScore = tempAdjustedScore;
				}

				// Now it's time to find the winner's and loser's rows in the Teams table so I can update them.
				SortedList<string, Team> winnerAndLoser = currentGame.GetWinnerAndLoser();
				Team winner = winnerAndLoser["Winner"];
				Team loser = winnerAndLoser["Loser"];

				ProcessTeams(winner, loser, operation, currentGame);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Processes the two teams involved in the specified game.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		/// <param name="operation"></param>
		/// <param name="currentGame"></param>
		private static void ProcessTeams(Team winner, Team loser, Operation operation, Game currentGame)
		{
			try
			{
				if ( (winner == null) && (loser == null) )
				{
					throw new Exception("Neither team could be found in the database.");
				}

				EditDataFromAllGames(winner, loser, operation, currentGame);

				if ( IsGameOutOfClass(winner, loser) )
				{
					// One of the two teams is out of state, or the two teams are in different classifications, 
					// or one of the teams is an independent.
					EditDataFromGamesOutOfClassification(winner, loser, operation, currentGame);
				}
				else
				{
					// Both teams are in state and in the same classification.
					EditDataFromGamesInClassification(winner, loser, operation, currentGame);

					if ( currentGame.IsDistrictGame )
					{
						EditDataFromDistrictGames(winner, loser, operation, currentGame);
					}
					else if ( currentGame.IsPlayoffGame )
					{
						EditDataFromPlayoffGames(winner, loser, operation, currentGame);
					}
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Saves all changes made to the database.
		/// </summary>
		private static void SaveChanges()
		{
			try
			{
				// Save all changes to the database.
				DataAccess.DbContext.SaveChanges();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Instance Helpers

		/// <summary>
		/// Applies the filter set in the GameFinderWindowViewModel
		/// </summary>
		private void ApplyFindGameFilter()
		{
			try
			{
				var guest = (_gameFinderWindow.DataContext as GameFinderWindowViewModel).Guest;
				var host = (_gameFinderWindow.DataContext as GameFinderWindowViewModel).Host;
				var games = (from game in DataAccess.DbContext.Games
								 where (game.Guest == guest && game.Host == host)
								 select game);
				LoadGames(games);

				if ( !_isFindGameFilterApplied )
				{
					_isFindGameFilterApplied = true;
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Resets data in all data entry fields to their default values.
		/// </summary>
		private void ClearDataEntryControls()
		{
			try
			{
				Guest = String.Empty;
				GuestScore = 0;
				GuestAdjustedScore = 0;
				Host = String.Empty;
				HostScore = 0;
				HostAdjustedScore = 0;
				IsDistrictGame = false;
				IsPlayoffGame = false;
				IsForfeit = false;
				Notes = String.Empty;

				AddGameControlVisibility = Visibility.Visible;
				EditGameControlVisibility = Visibility.Hidden;
				RemoveGameControlVisibility = Visibility.Hidden;

				// Set focus to Guest field.
				OnMoveFocus("Guest");
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Loads this GamesWindowViewModel object's Games collection.
		/// </summary>
		/// <param name="games"></param>
		private void LoadGames(IQueryable<Game> games)
		{
			try
			{
				this.Games = new ObservableCollection<Game>();
				foreach ( var game in games )
				{
					this.Games.Add(game);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Populates data entry controls with data from selected game.
		/// </summary>
		private void PopulateDataEntryControls()
		{
			try
			{
				Week = (int)SelectedGame.Week;
				Guest = SelectedGame.Guest;
				GuestScore = (int)SelectedGame.GuestScore;
				GuestAdjustedScore = (int)SelectedGame.GuestAdjustedScore;
				Host = SelectedGame.Host;
				HostScore = (int)SelectedGame.HostScore;
				HostAdjustedScore = (int)SelectedGame.HostAdjustedScore;
				IsDistrictGame = SelectedGame.IsDistrictGame;
				IsPlayoffGame = SelectedGame.IsPlayoffGame;
				IsForfeit = SelectedGame.IsForfeit;
				Notes = SelectedGame.Notes;

				ValidateTeams();

				AddGameControlVisibility = Visibility.Hidden;
				EditGameControlVisibility = Visibility.Visible;
				RemoveGameControlVisibility = Visibility.Visible;
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Sorts the Games collection by Week ascending, then by Host ascending.
		/// </summary>
		private void SortGamesByDefaultOrder()
		{
			try
			{
				var games = (from game in DataAccess.DbContext.Games
								 select game)
								.OrderBy(g => g.Week)
								.ThenBy(g => g.Host);
				LoadGames(games);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Stores values from the data entry controls into a Game instance.
		/// </summary>
		/// <returns></returns>
		private Game StoreNewGameValues()
		{
			Game newGame = null;

			try
			{
				newGame = new Game
				{
					ID = 0,
					Week = this.Week,
					Guest = this.Guest,
					GuestScore = this.GuestScore,
					GuestAdjustedScore = this.GuestAdjustedScore,
					Host = this.Host,
					HostScore = this.HostScore,
					HostAdjustedScore = this.HostAdjustedScore,
					IsDistrictGame = this.IsDistrictGame,
					IsPlayoffGame = this.IsPlayoffGame,
					IsForfeit = this.IsForfeit,
					Notes = this.Notes
				};

				newGame.DecideWinnerAndLoser();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}

			return newGame;
		}

		/// <summary>
		/// Stores values from the selected game into a Game instance.
		/// </summary>
		private Game StoreOldGameValues()
		{
			Game oldGame = null;

			try
			{
				oldGame = new Game
				{
					ID = 0,
					Week = SelectedGame.Week,
					Guest = SelectedGame.Guest,
					GuestScore = SelectedGame.GuestScore,
					GuestAdjustedScore = SelectedGame.GuestAdjustedScore,
					Host = SelectedGame.Host,
					HostScore = SelectedGame.HostScore,
					HostAdjustedScore = SelectedGame.HostAdjustedScore,
					IsDistrictGame = SelectedGame.IsDistrictGame,
					IsPlayoffGame = SelectedGame.IsPlayoffGame,
					IsForfeit = SelectedGame.IsForfeit,
					Notes = SelectedGame.Notes
				};

				oldGame.DecideWinnerAndLoser();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}

			return oldGame;
		}

		/// <summary>
		/// Validates all data entered into the data entry fields.
		/// </summary>
		private void ValidateDataEntry()
		{
			if ( String.IsNullOrWhiteSpace(Guest) )
			{
				// Guest field is left empty.
				OnMoveFocus("Guest");
				throw new DataValidationException(Globals.Constants.BothTeamsNeededErrorMessage);
			}
			else if ( String.IsNullOrWhiteSpace(Host) )
			{
				// Host field is left empty.
				OnMoveFocus("Host");
				throw new DataValidationException(Globals.Constants.BothTeamsNeededErrorMessage);
			}
			else if ( Guest == Host )
			{
				// Guest and host are the same team.
				OnMoveFocus("Guest");
				throw new DataValidationException(Globals.Constants.DifferentTeamsNeededErrorMessage);
			}
			else if ( GuestScore == HostScore )
			{
				// Guest and host finish tied (no longer permitted in Oregon, not even in the state championship game, after 1997).
				throw new DataValidationException(Globals.Constants.TieScoresNotPermittedErrorMessage);
			}
			else if ( !IsForfeit && (
				(GuestScore > HostScore && GuestAdjustedScore < HostAdjustedScore)
				|| (GuestScore < HostScore && GuestAdjustedScore > HostAdjustedScore)) )
			{
				// Error in entering adjusted scores.
				throw new DataValidationException(Globals.Constants.AdjustedScoresErrorMessage);
			}
		}

		#endregion

		#endregion

		#region Event Raisers

		/// <summary>
		/// Raises MoveFocus event when focus is moved from one control to another.
		/// </summary>
		/// <param name="focusedProperty"></param>
		private void OnMoveFocus(string focusedProperty)
		{
			// Validate focusedProperty argument.
			if ( String.IsNullOrEmpty(focusedProperty) )
			{
				throw new ArgumentException("focusedProperty");
			}

			// Raise MoveFocus event.
			var handler = MoveFocus;
			if ( handler != null )
			{
				handler(this, new MoveFocusEventArgs(focusedProperty));
			}
		}

		#endregion

		#endregion
	}
}
