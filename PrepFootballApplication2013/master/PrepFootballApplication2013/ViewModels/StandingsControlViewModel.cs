﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the Standings control.
	/// </summary>
	public class StandingsControlViewModel : ViewModelBase
	{
		#region Fields

		private readonly StateViewModel _stateViewModel;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the StandingsControlViewModel class.
		/// </summary>
		public StandingsControlViewModel()
		{
			try
			{
				var classificationsSorted = (from classification in DataAccess.DbContext.Classifications
													  orderby classification.Name descending
													  select classification)
													  .ToArray();

				_stateViewModel = new StateViewModel(this, classificationsSorted);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets the classifications collection for the TreeView control contained inside this control.
		/// </summary>
		public ReadOnlyCollection<ClassificationViewModel> Classifications
		{
			get { return _stateViewModel.Classifications; }
		}

		/// <summary>
		/// Gets or sets this control's district standings collection.
		/// </summary>
		private ObservableCollection<GetStandings_Result> _DistrictStandingsResult;
		public ObservableCollection<GetStandings_Result> DistrictStandingsResult
		{
			get { return _DistrictStandingsResult; }
			set
			{
				if ( value != _DistrictStandingsResult )
				{
					_DistrictStandingsResult = value;
					OnPropertyChanged("DistrictStandingsResult");
				}
			}
		}

		#endregion
	}
}
