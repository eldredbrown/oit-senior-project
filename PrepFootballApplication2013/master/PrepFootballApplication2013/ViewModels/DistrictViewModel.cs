﻿using System;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the District tree view item.
	/// </summary>
	public class DistrictViewModel : TreeViewItemViewModel
	{
		#region Fields

		private readonly District _district;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a custom instance of the DistrictViewModel class.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="district"></param>
		public DistrictViewModel(ClassificationViewModel parent, District district)
			: base(parent, false)
		{
			// Validate arguments.
			if ( district == null )
			{
				throw new ArgumentNullException("district");
			}

			// Assign argument values to member fields.
			_district = district;
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets the name of the District instance wrapped inside this ViewModel.
		/// </summary>
		public string DistrictName
		{
			get { return _district.Name; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
		/// </summary>
		public override bool IsSelected
		{
			get
			{
				return base.IsSelected;
			}
			set
			{
				base.IsSelected = value;
			}
		}

		/// <summary>
		/// Gets this control's logical parent.
		/// </summary>
		public override TreeViewItemViewModel Parent
		{
			get { return base.Parent; }
			protected set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}

				base.Parent = value;
			}
		}

		#endregion

		#region Instance Methods

		/// <summary>
		/// Shows the standings for the selected district.
		/// Inherited from TreeViewItemViewModel.
		/// </summary>
		protected override void ShowStandings()
		{
			try
			{
				// Load standings data for the District object wrapped inside this ViewModel.
				var classificationName = (Parent as ClassificationViewModel).ClassificationName;
				var dsr = (from result in DataAccess.DbContext.GetStandings(classificationName, _district.Name)
							  select result)
							 .OrderByDescending(r => r.DistrictWinningPercentage)
							 .ThenByDescending(r => r.RatingPointsIndex);

				var districtStandingsResult = new ObservableCollection<GetStandings_Result>();
				foreach (var item in dsr)
				{
					districtStandingsResult.Add(item);
				}
				(Parent as ClassificationViewModel).ParentControl.ParentControl.DistrictStandingsResult = districtStandingsResult;
			}
			catch ( Exception ex )
			{
				(Parent as ClassificationViewModel).ParentControl.ParentControl.DistrictStandingsResult = null;
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion
	}
}
