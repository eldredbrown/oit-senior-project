﻿using System;
using System.Collections.ObjectModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// Base class for all TreeViewItem ViewModels.
	/// </summary>
	public class TreeViewItemViewModel : ViewModelBase
	{
		#region Fields

		private static readonly TreeViewItemViewModel DummyChild = new TreeViewItemViewModel();

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a custom instance of the TreeViewItemViewModel class.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="lazyLoadChildren"></param>
		protected TreeViewItemViewModel(TreeViewItemViewModel parent, bool lazyLoadChildren)
		{
			try
			{
				this.Parent = parent;

				this.Children = new ObservableCollection<TreeViewItemViewModel>();
				if ( lazyLoadChildren )
				{
					this.Children.Add(DummyChild);
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		// This is used to create the DummyChild instance.
		private TreeViewItemViewModel()
			: this(parent:null, lazyLoadChildren:true)
		{
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets this control's logical child items.
		/// </summary>
		public virtual ObservableCollection<TreeViewItemViewModel> Children { get; private set; }

		/// <summary>
		/// Gets or sets a value indicating whether the TreeViewItem associated with this object is expanded.
		/// </summary>
		private bool _IsExpanded;
		public virtual bool IsExpanded
		{
			get { return _IsExpanded; }
			set
			{
				if ( value != _IsExpanded )
				{
					_IsExpanded = value;
					OnPropertyChanged("IsExpanded");
				}

				// Expand all the way up to the root.
				if ( _IsExpanded && Parent != null )
				{
					Parent.IsExpanded = true;
				}

				// Lazy load the child items, if necessary.
				if ( HasDummyChild() )
				{
					Children.Remove(DummyChild);
					LoadChildren();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
		/// </summary>
		private bool _IsSelected;
		public virtual bool IsSelected
		{
			get { return _IsSelected; }
			set
			{
				if ( value != _IsSelected )
				{
					_IsSelected = value;
					OnPropertyChanged("IsSelected");
				}

				ShowStandings();
			}
		}

		/// <summary>
		/// Gets this control's logical parent.
		/// </summary>
		public virtual TreeViewItemViewModel Parent { get; protected set; }

		#endregion

		#region Instance Methods

		/// <summary>
		/// Invoked when the child items need to be loaded on demand.
		/// Derived classes can override this to populate the Children collection.
		/// </summary>
		protected virtual void LoadChildren()
		{
		}

		/// <summary>
		/// Invoked when the district standings are to be shown.
		/// Derived classes can override this to provide custom definition of this method.
		/// </summary>
		protected virtual void ShowStandings()
		{
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Gets a value indicating whether this control's children have been populated yet.
		/// </summary>
		private bool HasDummyChild()
		{
			return (Children.Count == 1 && Children[0] == DummyChild);
		}

		#endregion
	}
}
