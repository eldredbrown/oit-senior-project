﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.PrepFootballApplication2013.DataModel;

namespace EldredBrown.PrepFootballApplication2013.ViewModels
{
	/// <summary>
	/// ViewModel logic for the state TreeView control.
	/// </summary>
	public class StateViewModel : ViewModelBase
	{
		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new custom instance of the StateViewModel class.
		/// </summary>
		/// <param name="parentControl"></param>
		/// <param name="classes"></param>
		public StateViewModel(StandingsControlViewModel parentControl, Classification[] classifications)
		{
			try
			{
				this.ParentControl = parentControl;

				this.Classifications = new ReadOnlyCollection<ClassificationViewModel>(
					 (from classification in classifications
					  select new ClassificationViewModel(this, classification))
					 .ToList());
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion

		#region Instance Properties

		/// <summary>
		/// Gets this control's classifications collection.
		/// </summary>
		private ReadOnlyCollection<ClassificationViewModel> _Classifications;
		public ReadOnlyCollection<ClassificationViewModel> Classifications
		{
			get { return _Classifications; }
			private set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _Classifications )
				{
					_Classifications = value;
				}
			}
		}

		/// <summary>
		/// Gets this control's parent control.
		/// </summary>
		private StandingsControlViewModel _ParentControl;
		public StandingsControlViewModel ParentControl
		{
			get { return _ParentControl; }
			private set
			{
				if ( value == null )
				{
					throw new ArgumentNullException("value");
				}
				else if ( value != _ParentControl )
				{
					_ParentControl = value;
				}
			}
		}

		#endregion
	}
}
