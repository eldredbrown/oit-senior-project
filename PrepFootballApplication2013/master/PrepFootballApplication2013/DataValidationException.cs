﻿using System;
using System.Runtime.Serialization;

namespace EldredBrown.PrepFootballApplication2013
{
	class DataValidationException : Exception
	{
		public DataValidationException(string message)
			: base(message)
		{
		}

		public DataValidationException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		public DataValidationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
