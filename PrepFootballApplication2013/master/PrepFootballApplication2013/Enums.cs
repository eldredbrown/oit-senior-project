﻿
namespace EldredBrown.PrepFootballApplication2013
{
	public enum Direction
	{
		Down,
		Up
	}

	public enum GameType
	{
		ForfeitForFailureToAppear,
		ForfeitForRulesViolation,
		NonForfeit
	}

	public enum WinnerOrLoser
	{
		Loser,
		Winner
	}
}
