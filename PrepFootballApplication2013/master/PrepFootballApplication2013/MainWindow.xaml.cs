﻿using System.Windows;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			DataContext = new MainWindowViewModel();
		}
	}
}
