﻿using System.Windows.Controls;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013.UserControls
{
	/// <summary>
	/// Interaction logic for TeamsControl.xaml
	/// </summary>
	public partial class TeamsControl : UserControl
	{
		public TeamsControl()
		{
			InitializeComponent();

			DataContext = new TeamsControlViewModel();
		}
	}
}
