﻿using System.Windows.Controls;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013.UserControls
{
	/// <summary>
	/// Interaction logic for StandingsControl.xaml
	/// </summary>
	public partial class StandingsControl : UserControl
	{
		public StandingsControl()
		{
			InitializeComponent();

			DataContext = new StandingsControlViewModel();
		}
	}
}
