﻿using System.Windows.Controls;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013.UserControls
{
	/// <summary>
	/// Interaction logic for RankingsControl.xaml
	/// </summary>
	public partial class RankingsControl : UserControl
	{
		public RankingsControl()
		{
			InitializeComponent();

			DataContext = new RankingsControlViewModel();
		}
	}
}
