﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EldredBrown.PrepFootballApplication2013
{
	public static class Arithmetic
	{
		public static decimal Add(decimal lVal, decimal rVal)
		{
			return lVal + rVal;
		}

		public static decimal Divide(decimal numerator, decimal denominator)
		{
			// Rather than throw an error for division by zero, 
			// this will return a default result of zero if division by zero occurs.
			decimal result = 0;

			if ( denominator != 0 )
			{
				result = numerator / denominator;
			}

			return result;
		}

		public static decimal Multiply(decimal lVal, decimal rVal)
		{
			return lVal * rVal;
		}

		public static decimal Subtract(decimal lVal, decimal rVal)
		{
			return lVal - rVal;
		}
	}
}
