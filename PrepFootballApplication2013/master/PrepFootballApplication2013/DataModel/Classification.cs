//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EldredBrown.PrepFootballApplication2013.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Classification
    {
        public Classification()
        {
            this.Districts = new HashSet<District>();
        }
    
        public string Name { get; set; }
        public decimal Value { get; set; }
        public decimal TotalGames { get; set; }
        public decimal TotalAdjustedPointsFor { get; set; }
        public decimal AverageAdjustedPointsFor { get; set; }
    
        public virtual ICollection<District> Districts { get; set; }
    }
}
