//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EldredBrown.PrepFootballApplication2013.DataModel
{
    using System;
    
    public partial class GetStandings_Result
    {
        public string Name { get; set; }
        public decimal DistrictWins { get; set; }
        public decimal DistrictLosses { get; set; }
        public Nullable<decimal> DistrictWinningPercentage { get; set; }
        public decimal DistrictPointsFor { get; set; }
        public decimal DistrictPointsAgainst { get; set; }
        public decimal TotalWins { get; set; }
        public decimal TotalLosses { get; set; }
        public decimal TotalPointsFor { get; set; }
        public decimal TotalPointsAgainst { get; set; }
        public decimal RatingPointsIndex { get; set; }
    }
}
