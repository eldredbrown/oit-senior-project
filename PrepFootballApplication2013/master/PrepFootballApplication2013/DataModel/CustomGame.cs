﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;

namespace EldredBrown.PrepFootballApplication2013.DataModel
{
	/// <summary>
	/// Extension of the DataModel's auto-generated Game class.
	/// </summary>
	public partial class Game
	{
		#region Fields

		private const decimal LoserRatingPoints = 0;

		#endregion

		#region Constructors & Finalizers

		/// <summary>
		/// Initializes a new default instance of the Game class.
		/// </summary>
		public Game()
		{
		}

		#endregion

		#region Instance Properties

		public decimal LoserAdjustedScore { get; set; }
		public decimal LoserScore { get; set; }
		public decimal WinnerAdjustedScore { get; set; }
		public decimal WinnerRatingPoints { get; set; }
		public decimal WinnerScore { get; set; }

		#endregion

		#region Instance Methods

		/// <summary>
		/// Decides this game's winner and loser.
		/// </summary>
		public void DecideWinnerAndLoser()
		{
			try
			{
				// Declare the winner to be the team that scored more points in the game.
				if ( GuestScore > HostScore )
				{
					Winner = Guest;
					WinnerScore = GuestScore;
					WinnerAdjustedScore = GuestAdjustedScore;
					Loser = Host;
					LoserScore = HostScore;
					LoserAdjustedScore = HostAdjustedScore;
				}
				else if ( HostScore > GuestScore )
				{
					Winner = Host;
					WinnerScore = HostScore;
					WinnerAdjustedScore = HostAdjustedScore;
					Loser = Guest;
					LoserScore = GuestScore;
					LoserAdjustedScore = GuestAdjustedScore;
				}
				else
				{
					throw new DataValidationException(Globals.Constants.TieScoresNotPermittedErrorMessage);
				}

				AssignRatingPoints();
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		/// <summary>
		/// Gets a value indicating whether this game is a forfeit for one team's failure to appear, a forfeit for 
		/// one team's rules violation (e.g., use of an ineligible player), or not a forfeit.
		/// </summary>
		/// <returns></returns>
		public GameType GetGameType()
		{
			GameType gameType;

			if ( IsForfeit )
			{
				if ( WinnerScore == 1 && LoserScore == 0 )
				{
					gameType = GameType.ForfeitForFailureToAppear;
				}
				else
				{
					gameType = GameType.ForfeitForRulesViolation;
				}
			}
			else
			{
				gameType = GameType.NonForfeit;
			}

			return gameType;
		}

		/// <summary>
		/// Gets this game's winner and loser.
		/// </summary>
		/// <param name="winner"></param>
		/// <param name="loser"></param>
		public SortedList<string, Team> GetWinnerAndLoser()
		{
			SortedList<string, Team> list = null;

			try
			{
				list = new SortedList<string, Team>();

				var winner = (from team in DataAccess.DbContext.Teams
								  where team.Name == this.Winner
								  select team).FirstOrDefault();
				list.Add("Winner", winner);

				var loser = (from team in DataAccess.DbContext.Teams
								 where team.Name == this.Loser
								 select team).FirstOrDefault();
				list.Add("Loser", loser);
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}

			return list;
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Assigns rating points to each team for RPI calculations.
		/// </summary>
		private void AssignRatingPoints()
		{
			try
			{
				// Get this game's winner and loser.
				SortedList<string, Team> winnerAndLoser = GetWinnerAndLoser();
				Team winner = winnerAndLoser["Winner"];
				Team loser = winnerAndLoser["Loser"];

				if ( winner == null || IsForfeit )
				{
					// The winning team is not in the database, so just assign this team no rating points.
					WinnerRatingPoints = 0;
				}
				else if ( loser == null )
				{
					// The losing team is not in the database, so just assume that they are in the same class.
					WinnerRatingPoints = (from classification in DataAccess.DbContext.Classifications
												 where classification.Name == winner.ClassificationName
												 select classification.Value).FirstOrDefault();
				}
				else
				{
					// Assign to the winning team the rating value of the losing team's class.
					WinnerRatingPoints = (from classification in DataAccess.DbContext.Classifications
												 where classification.Name == loser.ClassificationName
												 select classification.Value).FirstOrDefault();
				}

				// Assign rating points to correct teams depending on whether the guest won or the host won.
				if ( this.Winner == Guest )
				{
					GuestRatingPoints = WinnerRatingPoints;
					HostRatingPoints = LoserRatingPoints;
				}
				else
				{
					GuestRatingPoints = LoserRatingPoints;
					HostRatingPoints = WinnerRatingPoints;
				}
			}
			catch ( Exception ex )
			{
				Globals.ShowExceptionMessage(ex);
			}
		}

		#endregion
	}
}
