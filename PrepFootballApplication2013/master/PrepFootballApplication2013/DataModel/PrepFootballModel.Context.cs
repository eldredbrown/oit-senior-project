﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EldredBrown.PrepFootballApplication2013.DataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class PrepFootballEntities : DbContext
    {
        public PrepFootballEntities()
            : base("name=PrepFootballEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Classification> Classifications { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<WeekCount> WeekCounts { get; set; }
    
        public virtual ObjectResult<GetClassificationTotals_Result> GetClassificationTotals(string className)
        {
            var classNameParameter = className != null ?
                new ObjectParameter("className", className) :
                new ObjectParameter("className", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetClassificationTotals_Result>("GetClassificationTotals", classNameParameter);
        }
    
        public virtual ObjectResult<GetRankingsDefensive_Result> GetRankingsDefensive(string className)
        {
            var classNameParameter = className != null ?
                new ObjectParameter("className", className) :
                new ObjectParameter("className", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRankingsDefensive_Result>("GetRankingsDefensive", classNameParameter);
        }
    
        public virtual ObjectResult<GetRankingsOffensive_Result> GetRankingsOffensive(string className)
        {
            var classNameParameter = className != null ?
                new ObjectParameter("className", className) :
                new ObjectParameter("className", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRankingsOffensive_Result>("GetRankingsOffensive", classNameParameter);
        }
    
        public virtual ObjectResult<GetRankingsTotal_Result> GetRankingsTotal(string className)
        {
            var classNameParameter = className != null ?
                new ObjectParameter("className", className) :
                new ObjectParameter("className", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRankingsTotal_Result>("GetRankingsTotal", classNameParameter);
        }
    
        public virtual ObjectResult<GetStandings_Result> GetStandings(string className, string distName)
        {
            var classNameParameter = className != null ?
                new ObjectParameter("className", className) :
                new ObjectParameter("className", typeof(string));
    
            var distNameParameter = distName != null ?
                new ObjectParameter("distName", distName) :
                new ObjectParameter("distName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetStandings_Result>("GetStandings", classNameParameter, distNameParameter);
        }
    
        public virtual ObjectResult<GetTeamScheduleAverages_Result> GetTeamScheduleAverages(string team)
        {
            var teamParameter = team != null ?
                new ObjectParameter("team", team) :
                new ObjectParameter("team", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeamScheduleAverages_Result>("GetTeamScheduleAverages", teamParameter);
        }
    
        public virtual ObjectResult<GetTeamScheduleProfile_Result> GetTeamScheduleProfile(string team)
        {
            var teamParameter = team != null ?
                new ObjectParameter("team", team) :
                new ObjectParameter("team", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeamScheduleProfile_Result>("GetTeamScheduleProfile", teamParameter);
        }
    
        public virtual ObjectResult<GetTeamScheduleTotals_Result> GetTeamScheduleTotals(string team)
        {
            var teamParameter = team != null ?
                new ObjectParameter("team", team) :
                new ObjectParameter("team", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeamScheduleTotals_Result>("GetTeamScheduleTotals", teamParameter);
        }
    }
}
