﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows;

namespace EldredBrown.PrepFootballApplication2013
{
	internal static class Globals
	{
		internal struct Constants
		{
			internal const string AppName = "Prep Football Application";

			internal const string IndependentString = "Ind";
			internal const string OutOfStateDistrictString = "OS";

			internal const string AdjustedScoresErrorMessage =
				"Unless the game is a forfeit, the winning team must be awarded a higher adjusted score than the losing team.";
			internal const string BothTeamsNeededErrorMessage = "Please enter names for both teams.";
			internal const string DifferentTeamsNeededErrorMessage = "Please enter a different name for each team.";
			internal const string TieScoresNotPermittedErrorMessage = "Tie scores are not permitted.";
		}

		/// <summary>
		/// Shows the message for any exception of a type that doesn't have its own specific ShowMessage method.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="caption"></param>
		internal static void ShowExceptionMessage(Exception ex, string caption="Exception")
		{
			MessageBox.Show(ex.Message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
		}

		/// <summary>
		/// Shows a DataValidationException message.
		/// </summary>
		/// <param name="message"></param>
		internal static void ShowExceptionMessage(DataValidationException ex)
		{
			ShowExceptionMessage(ex, "DataValidationException");
		}
	}
}
