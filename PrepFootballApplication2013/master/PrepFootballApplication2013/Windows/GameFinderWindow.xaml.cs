﻿using System.Windows;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013.Windows
{
	/// <summary>
	/// Interaction logic for GameFinder.xaml
	/// </summary>
	public partial class GameFinderWindow : Window
	{
		public GameFinderWindow()
		{
			InitializeComponent();

			DataContext = new GameFinderWindowViewModel();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			GuestTextBox.Focus();
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			var vm = (DataContext as GameFinderWindowViewModel);
			try
			{
				// The Guest and Host properties in the underlying ViewModel are not updated automatically when a press
				// of the Enter key clicks the OK button automatically, so we need to update these directly as follows:
				vm.Guest = GuestTextBox.Text;
				vm.Host = HostTextBox.Text;
				vm.ValidateDataEntry();
			}
			catch ( DataValidationException ex )
			{
				Globals.ShowExceptionMessage(ex);
				GuestTextBox.Focus();
				return;
			}

			DialogResult = true;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
