﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using EldredBrown.PrepFootballApplication2013.ViewModels;

namespace EldredBrown.PrepFootballApplication2013.Windows
{
	/// <summary>
	/// Interaction logic for Games.xaml
	/// </summary>
	public partial class GamesWindow : Window
	{
		public GamesWindow()
		{
			InitializeComponent();

			DataContext = new GamesWindowViewModel();
		}

		private void GamesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if ( GamesDataGrid.SelectedItem == CollectionView.NewItemPlaceholder )
			{
				// Prepare to add a new game.
				(DataContext as GamesWindowViewModel).SelectedGame = null;
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			(DataContext as GamesWindowViewModel).SelectedGame = null;
			GuestTextBox.Focus();
		}
	}
}
