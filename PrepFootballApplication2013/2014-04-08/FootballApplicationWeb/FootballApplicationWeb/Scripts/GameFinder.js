﻿var bothTeamsNeededErrorMessage = "Please enter names for both teams.";
var differentTeamsNeededErrorMessage = "Please enter a different name for each team.";

(function ()
{
	var ValidateForm = function ()
	{
		var guestTextBox = document.getElementById("GuestTextBox");
		var guest = guestTextBox.textContent;

		var hostTextBox = document.getElementById("HostTextBox");
		var host = hostTextBox.textContent;

		if (guest == null || guest == "" || host == null || host == "")
		{
			alert(bothTeamsNeededErrorMessage);
		}
		else if (guest === host)
		{
			alert(differentTeamsNeededErrorMessage);
		}
	}

	var gameFinderForm = document.getElementById("GameFinderForm");
	gameFinderForm.addEventListener("submit", ValidateForm, false);
})();
