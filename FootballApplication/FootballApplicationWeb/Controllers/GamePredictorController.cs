﻿using System.Threading.Tasks;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Services;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class GamePredictorController : Controller
    {
        private readonly IGamePredictorService _service;
        private readonly ISharedService _sharedService;

        public GamePredictorController(IGamePredictorService service, ISharedService sharedService)
        {
            _service = service;
            _sharedService = sharedService;
        }

        // GET: GamePredictor
        public async Task<ActionResult> PredictGame()
        {
            await _service.GetGuestAndHostSeasonIds();

            var seasonViewModels = await _sharedService.GetAllSeasons();

            ViewBag.GuestSeasonID = new SelectList(seasonViewModels, "ID", "ID", GamePredictorService.GuestSeasonID);

            var guestViewModels = await _service.GetTeamSeasons((int)GamePredictorService.GuestSeasonID);
            ViewBag.GuestName = new SelectList(guestViewModels, "TeamName", "TeamName");

            ViewBag.HostSeasonID = new SelectList(seasonViewModels, "ID", "ID", GamePredictorService.HostSeasonID);

            var hostViewModels = await _service.GetTeamSeasons((int)GamePredictorService.HostSeasonID);
            ViewBag.HostName = new SelectList(hostViewModels, "TeamName", "TeamName");

            return View();
        }

        /// <summary>
        /// Calculates the predicted score of a future or hypothetical game.
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PredictGame([Bind(Include = "GuestSeasonID,GuestName,HostSeasonID,HostName")] GamePredictionViewModel matchup)
        {
            //try
            //{
            //var matchup = _helper.ValidateDataEntry();
            var seasonViewModels = await _sharedService.GetAllSeasons();

            // Show predicted guest score.
            var guestSeasonViewModel = await _service.GetTeamSeason(matchup.GuestName, matchup.GuestSeasonID);

            ViewBag.GuestSeasonID = new SelectList(seasonViewModels, "ID", "ID", guestSeasonViewModel.SeasonID);

            var guestViewModels = await _service.GetTeamSeasons((int)GamePredictorService.GuestSeasonID);
            ViewBag.GuestName = new SelectList(guestViewModels, "TeamName", "TeamName", guestSeasonViewModel.TeamName);

            //matchup.GuestScore = (int)((guestSeason.OffensiveFactor * hostSeason.DefensiveAverage + hostSeason.DefensiveFactor * guestSeason.OffensiveAverage) / 2d);

            // Show predicted host score.
            var hostSeasonViewModel = await _service.GetTeamSeason(matchup.HostName, matchup.HostSeasonID);

            ViewBag.HostSeasonID = new SelectList(seasonViewModels, "ID", "ID", hostSeasonViewModel.SeasonID);

            var hostViewModels = await _service.GetTeamSeasons((int)GamePredictorService.HostSeasonID);
            ViewBag.HostName = new SelectList(hostViewModels, "TeamName", "TeamName", hostSeasonViewModel.TeamName);

            //matchup.HostScore = (int)((hostSeason.OffensiveFactor * guestSeason.DefensiveAverage + guestSeason.DefensiveFactor * hostSeason.OffensiveAverage) / 2d);

            return View(matchup);
            //}
            //catch (DataValidationException ex)
            //{
            //    //_globals.ShowExceptionMessage(ex);
            //}
            //catch (Exception ex)
            //{
            //    //_globals.ShowExceptionMessage(ex);
            //}
        }

        public ActionResult ApplyFilter(int? guestSeasonID, int? hostSeasonID)
        {
            _service.ApplyFilter(guestSeasonID, hostSeasonID);
            return RedirectToAction("PredictGame");
        }
    }
}
