﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Services;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class GamesController : Controller
    {
        private const int FirstWeek = 1;

        private readonly IGamesService _service;
        private readonly ISharedService _sharedService;

        public GamesController(IGamesService service, ISharedService sharedService)
        {
            _service = service;
            _sharedService = sharedService;
        }

        private static GameViewModel _oldGameViewModel;

        // GET: Games
        public async Task<ActionResult> Index(int? seasonID, int? week, string guestSearchString, string hostSearchString)
        {
            var seasonViewModels = await _sharedService.GetAllSeasons();
            _service.SetSelectedSeason(seasonViewModels, seasonID);
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", GamesService.SelectedSeason);

            var weekViewModels = await _service.GetWeeks(GamesService.SelectedSeason);
            _service.SetSelectedWeek(week);
            ViewBag.Week = new SelectList(weekViewModels, "ID", "ID", GamesService.SelectedWeek);

            var gameViewModels = await _service.GetGames(GamesService.SelectedSeason, GamesService.SelectedWeek.ID, guestSearchString, hostSearchString);
            return View(gameViewModels);
        }

        // GET: Games/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var gameViewModel = await _service.FindGameAsync(id);
            if (gameViewModel == null)
            {
                return HttpNotFound();
            }

            return View(gameViewModel);
        }

        // GET: Games/Create
        public async Task<ActionResult> Create()
        {
            var seasonViewModels = await _sharedService.GetAllSeasons();
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", GamesService.SelectedSeason);
            ViewBag.Week = GamesService.SelectedWeek.ID;

            var teamViewModels = await _service.GetAllTeams();
            ViewBag.GuestName = new SelectList(teamViewModels, "Name", "Name");
            ViewBag.HostName = new SelectList(teamViewModels, "Name", "Name");

            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,SeasonID,Week,GuestName,GuestScore,HostName,HostScore,WinnerName,LoserName,IsPlayoffGame,Notes")] GameViewModel gameViewModel)
        {
            if (ModelState.IsValid)
            {
                _service.SetSelectedWeek(gameViewModel.Week);

                await _service.AddGame(gameViewModel);

                return RedirectToAction("Create", new { seasonID = gameViewModel.SeasonID });
            }

            var seasonViewModels = await _sharedService.GetAllSeasons();
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", gameViewModel.SeasonID);

            ViewBag.Week = GamesService.SelectedWeek.ID;

            var teamViewModels = await _service.GetAllTeams();
            ViewBag.GuestName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.GuestName);
            ViewBag.HostName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.HostName);

            return View(gameViewModel);
        }

        // GET: Games/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var gameViewModel = await _service.FindGameAsync(id);
            if (gameViewModel == null)
            {
                return HttpNotFound();
            }
            _oldGameViewModel = gameViewModel;

            var seasonViewModels = await _sharedService.GetAllSeasons();
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", gameViewModel.SeasonID);

            var teamViewModels = await _service.GetAllTeams();
            ViewBag.GuestName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.GuestName);
            ViewBag.HostName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.HostName);

            return View(gameViewModel);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,SeasonID,Week,GuestName,GuestScore,HostName,HostScore,WinnerName,LoserName,IsPlayoffGame,Notes")] GameViewModel gameViewModel)
        {
            if (ModelState.IsValid)
            {
                await _service.EditGame(_oldGameViewModel, gameViewModel);
                return RedirectToAction("Index", new { seasonID = gameViewModel.SeasonID });
            }

            var seasonViewModels = await _sharedService.GetAllSeasons();
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", gameViewModel.SeasonID);

            var teamViewModels = await _service.GetAllTeams();
            ViewBag.GuestName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.GuestName);
            ViewBag.HostName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.HostName);
            ViewBag.WinnerName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.WinnerName);
            ViewBag.LoserName = new SelectList(teamViewModels, "Name", "Name", gameViewModel.LoserName);

            return View(gameViewModel);
        }

        // GET: Games/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var gameViewModel = await _service.FindGameAsync(id);
            if (gameViewModel == null)
            {
                return HttpNotFound();
            }

            return View(gameViewModel);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            await _service.DeleteGame(id);
            return RedirectToAction("Index", new { seasonID = GamesService.SelectedSeason });
        }
    }
}
