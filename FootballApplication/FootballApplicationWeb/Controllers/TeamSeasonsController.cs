﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Services;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class TeamSeasonsController : Controller
    {
        private readonly ITeamSeasonsService _service;
        private readonly ISharedService _sharedService;

        public TeamSeasonsController(ITeamSeasonsService service, ISharedService sharedService)
        {
            _service = service;
            _sharedService = sharedService;
        }

        // GET: TeamSeasons
        public async Task<ActionResult> Index(int? seasonID, string sortOrder)
        {
            var seasonViewModels = await _sharedService.GetAllSeasons();
            _service.SetSelectedSeason(seasonViewModels, seasonID, sortOrder);

            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", TeamSeasonsService.SelectedSeason);

            ViewBag.TeamSortParm = sortOrder == "team_desc" ? "team_asc" : "team_desc";
            ViewBag.WinsSortParm = sortOrder == "wins_desc" ? "wins_asc" : "wins_desc";
            ViewBag.LossesSortParm = sortOrder == "losses_asc" ? "losses_desc" : "losses_asc";
            ViewBag.TiesSortParm = sortOrder == "ties_asc" ? "ties_desc" : "ties_asc";
            ViewBag.WinningPercentageSortParm = sortOrder == "win_pct_desc" ? "win_pct_asc" : "win_pct_desc";
            ViewBag.PointsForSortParm = sortOrder == "pf_desc" ? "pf_asc" : "pf_desc";
            ViewBag.PointsAgainstSortParm = sortOrder == "pa_asc" ? "pa_desc" : "pa_asc";
            ViewBag.PythagoreanWinsSortParm = sortOrder == "pyth_w_desc" ? "pyth_w_asc" : "pyth_w_desc";
            ViewBag.PythagoreanLossesSortParm = sortOrder == "pyth_l_asc" ? "pyth_l_desc" : "pyth_l_asc";
            ViewBag.OffensiveAverageSortParm = sortOrder == "off_avg_asc" ? "off_avg_desc" : "off_avg_asc";
            ViewBag.OffensiveFactorSortParm = sortOrder == "off_factor_asc" ? "off_factor_desc" : "off_factor_asc";
            ViewBag.OffensiveIndexSortParm = sortOrder == "off_index_asc" ? "off_index_desc" : "off_index_asc";
            ViewBag.DefensiveAverageSortParm = sortOrder == "def_avg_asc" ? "def_avg_desc" : "def_avg_asc";
            ViewBag.DefensiveFactorSortParm = sortOrder == "def_factor_asc" ? "def_factor_desc" : "def_factor_asc";
            ViewBag.DefensiveIndexSortParm = sortOrder == "def_index_asc" ? "def_index_desc" : "def_index_asc";
            ViewBag.FinalPythagoreanWinningPercentageSortParm = sortOrder == "fin_pyth_pct_asc" ? "fin_pyth_pct_desc" : "fin_pyth_pct_asc";

            var teamSeasonViewModels = await _service.GetTeamSeasons(TeamSeasonsService.SelectedSeason, sortOrder);

            return View(teamSeasonViewModels);
        }

        // GET: TeamSeasons/Details/5
        public async Task<ActionResult> Details(string teamName, int? seasonID)
        {
            if ((String.IsNullOrEmpty(teamName)) && (seasonID == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var viewModel = await _service.GetTeamSeasonDetails(teamName, seasonID);

            if ((viewModel.TeamSeason == null) || (viewModel.TeamSeasonScheduleProfile == null)
                || (viewModel.TeamSeasonScheduleTotals == null) || (viewModel.TeamSeasonScheduleAverages == null))
            {
                return HttpNotFound();
            }

            return View(viewModel);
        }

        /// <summary>
        /// Updates the rankings for the selected season
        /// </summary>
        public async Task UpdateRankings()
        {
            try
            {
                await _service.UpdateRankings();
                Response.Redirect(Url.Action("Index", "TeamSeasons"));
            }
            catch (Exception)
            {
                //_globals.ShowExceptionMessage(ex);
            }
        }
    }
}
