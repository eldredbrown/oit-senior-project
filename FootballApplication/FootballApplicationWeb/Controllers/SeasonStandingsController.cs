﻿using System.Threading.Tasks;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Services;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class SeasonStandingsController : Controller
    {
        private readonly ISeasonStandingsService _service;
        private readonly ISharedService _sharedService;

        public SeasonStandingsController(ISeasonStandingsService service, ISharedService sharedService)
        {
            _service = service;
            _sharedService = sharedService;
        }

        // GET: SeasonStandings
        public async Task<ActionResult> Index(int? seasonID, bool? groupByDivision)
        {
            var seasonViewModels = await _sharedService.GetAllSeasons();
            _service.SetSelectedSeason(seasonViewModels, seasonID);
            ViewBag.SeasonID = new SelectList(seasonViewModels, "ID", "ID", SeasonStandingsService.SelectedSeason);

            if (groupByDivision == null)
            {
                groupByDivision = false;
            }
            ViewBag.GroupByDivision = groupByDivision;

            var seasonStandingsViewModels = _service.GetSeasonStandings(SeasonStandingsService.SelectedSeason, groupByDivision);

            return View(seasonStandingsViewModels);
        }
    }
}
