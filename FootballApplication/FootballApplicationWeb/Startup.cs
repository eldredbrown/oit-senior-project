﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EldredBrown.FootballApplicationWeb.Startup))]
namespace EldredBrown.FootballApplicationWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
