﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Repositories;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public class SharedService : ISharedService
    {
        private ISharedRepository _repository;

        public SharedService(ISharedRepository repository)
        {
            _repository = repository;
        }

        // Violation of DRY rule with this also appearing in GamesController and GamePredictorController classes.
        // TODO: Refactor this into a shared location.
        public async Task<IEnumerable<SeasonViewModel>> GetAllSeasons()
        {
            var seasonViewModels = new List<SeasonViewModel>();

            var seasons = await _repository.GetAllSeasons();
            foreach (var season in seasons)
            {
                seasonViewModels.Add(new SeasonViewModel(season));
            }

            return seasonViewModels;
        }

        public void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID, ref int selectedSeason)
        {
            if (seasonID == null)
            {
                if (SharedGlobals.SelectedSeason == null)
                {
                    selectedSeason = seasons.First().ID;
                }
                else
                {
                    selectedSeason = (int)SharedGlobals.SelectedSeason;
                }
            }
            else
            {
                selectedSeason = (int)seasonID;
            }

            SharedGlobals.SelectedSeason = selectedSeason;
        }
    }
}