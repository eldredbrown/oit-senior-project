﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public interface IGamesService
    {
        Task AddGame(GameViewModel newGameViewModel);
        Task DeleteGame(int id);
        Task EditGame(GameViewModel oldGameViewModel, GameViewModel newGameViewModel);
        Task<GameViewModel> FindGameAsync(int? id);
        Task<IEnumerable<TeamViewModel>> GetAllTeams();
        Task<IEnumerable<GameViewModel>> GetGames(int selectedSeason, string selectedWeek, string guestSearchString, string hostSearchString);
        Task<IEnumerable<WeekViewModel>> GetWeeks(int selectedSeason);
        void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID);
        void SetSelectedWeek(int? week);
    }
}