﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public interface ISharedService
    {
        Task<IEnumerable<SeasonViewModel>> GetAllSeasons();
        void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID, ref int selectedSeason);
    }
}