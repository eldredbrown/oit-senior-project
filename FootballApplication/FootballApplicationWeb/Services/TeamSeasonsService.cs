﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Repositories;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public class TeamSeasonsService : ITeamSeasonsService
    {
        private readonly ITeamSeasonsRepository _repository;

        public TeamSeasonsService(ITeamSeasonsRepository repository)
        {
            _repository = repository;
        }

        public static int SelectedSeason { get; set; }

        public async Task<IEnumerable<TeamSeasonViewModel>> GetTeamSeasons(int seasonID, string sortOrder)
        {
            var teamSeasonViewModels = new List<TeamSeasonViewModel>();

            var teamSeasons = await _repository.GetTeamSeasons(seasonID, sortOrder);
            foreach (var teamSeason in teamSeasons)
            {
                teamSeasonViewModels.Add(new TeamSeasonViewModel(teamSeason));
            }

            return teamSeasonViewModels;
        }

        public async Task<TeamSeasonViewModel> GetTeamSeason(string teamName, int? seasonID)
        {
            var teamSeason = await _repository.FindTeamSeasonAsync(teamName, seasonID);
            return new TeamSeasonViewModel(teamSeason);
        }

        public async Task<TeamSeasonDetailsViewModel> GetTeamSeasonDetails(string teamName, int? seasonID)
        {
            return new TeamSeasonDetailsViewModel
            {
                TeamSeason = await GetTeamSeason(teamName, seasonID),
                TeamSeasonScheduleProfile = GetTeamSeasonScheduleProfile(teamName, seasonID),
                TeamSeasonScheduleTotals = GetTeamSeasonScheduleTotals(teamName, seasonID),
                TeamSeasonScheduleAverages = GetTeamSeasonScheduleAverages(teamName, seasonID),
            };
        }

        public TeamSeasonScheduleAveragesViewModel GetTeamSeasonScheduleAverages(string teamName, int? seasonID)
        {
            var teamSeasonScheduleAverages = _repository.GetTeamSeasonScheduleAverages(teamName, seasonID).FirstOrDefault();

            return new TeamSeasonScheduleAveragesViewModel(teamSeasonScheduleAverages);
        }

        public IEnumerable<TeamSeasonScheduleProfileViewModel> GetTeamSeasonScheduleProfile(string teamName, int? seasonID)
        {
            var teamSeasonScheduleProfileViewModels = new List<TeamSeasonScheduleProfileViewModel>();

            var teamSeasonScheduleProfile = _repository.GetTeamSeasonScheduleProfile(teamName, seasonID).ToList();
            foreach (var item in teamSeasonScheduleProfile)
            {
                teamSeasonScheduleProfileViewModels.Add(new TeamSeasonScheduleProfileViewModel(item));
            }

            return teamSeasonScheduleProfileViewModels;
        }

        public TeamSeasonScheduleTotalsViewModel GetTeamSeasonScheduleTotals(string teamName, int? seasonID)
        {
            var teamSeasonScheduleTotals = _repository.GetTeamSeasonScheduleTotals(teamName, seasonID).FirstOrDefault();

            return new TeamSeasonScheduleTotalsViewModel(teamSeasonScheduleTotals);
        }

        public void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID, string sortOrder)
        {
            if (seasonID == null)
            {
                if (String.IsNullOrEmpty(sortOrder))
                {
                    if (SharedGlobals.SelectedSeason == null)
                    {
                        SelectedSeason = seasons.First().ID;
                    }
                    else
                    {
                        SelectedSeason = (int)SharedGlobals.SelectedSeason;
                    }
                }
            }
            else
            {
                SelectedSeason = (int)seasonID;
            }

            SharedGlobals.SelectedSeason = SelectedSeason;
        }

        public async Task UpdateRankings()
        {
            await _repository.UpdateRankings(SelectedSeason);
        }
    }
}