﻿using System.Collections.Generic;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Repositories;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public class SeasonStandingsService : ISeasonStandingsService
    {
        private readonly ISeasonStandingsRepository _repository;
        private readonly ISharedService _sharedService;

        public SeasonStandingsService(ISeasonStandingsRepository repository, ISharedService sharedService)
        {
            _repository = repository;
            _sharedService = sharedService;
        }

        public static int SelectedSeason;

        public IEnumerable<SeasonStandingsResultViewModel> GetSeasonStandings(int? selectedSeason, bool? groupByDivision)
        {
            var seasonStandingsViewModels = new List<SeasonStandingsResultViewModel>();

            var seasonStandingResults = _repository.GetSeasonStandings(selectedSeason, groupByDivision);
            foreach (var result in seasonStandingResults)
            {
                seasonStandingsViewModels.Add(new SeasonStandingsResultViewModel(result));
            }

            return seasonStandingsViewModels;
        }

        public void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID)
        {
            _sharedService.SetSelectedSeason(seasons, seasonID, ref SelectedSeason);
        }
    }
}