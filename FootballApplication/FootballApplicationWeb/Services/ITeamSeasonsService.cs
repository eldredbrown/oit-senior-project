﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public interface ITeamSeasonsService
    {
        Task<IEnumerable<TeamSeasonViewModel>> GetTeamSeasons(int seasonID, string sortOrder);
        Task<TeamSeasonViewModel> GetTeamSeason(string teamName, int? seasonID);
        Task<TeamSeasonDetailsViewModel> GetTeamSeasonDetails(string teamName, int? seasonID);
        TeamSeasonScheduleAveragesViewModel GetTeamSeasonScheduleAverages(string teamName, int? seasonID);
        IEnumerable<TeamSeasonScheduleProfileViewModel> GetTeamSeasonScheduleProfile(string teamName, int? seasonID);
        TeamSeasonScheduleTotalsViewModel GetTeamSeasonScheduleTotals(string teamName, int? seasonID);
        void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID, string sortOrder);
        Task UpdateRankings();
    }
}
