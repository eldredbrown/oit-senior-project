﻿using System.Collections.Generic;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public interface ISeasonStandingsService
    {
        IEnumerable<SeasonStandingsResultViewModel> GetSeasonStandings(int? selectedSeason, bool? groupByDivision);
        void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID);
    }
}