﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Repositories;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public class GamesService : IGamesService
    {
        private readonly IGamesRepository _repository;
        private readonly ISharedService _sharedService;

        public GamesService(IGamesRepository repository, ISharedService sharedService)
        {
            _repository = repository;
            _sharedService = sharedService;
        }

        public static int SelectedSeason;
        public static WeekViewModel SelectedWeek { get; set; }

        public async Task AddGame(GameViewModel newGameViewModel)
        {
            var newGame = new Game(newGameViewModel);
            newGame.DecideWinnerAndLoser();

            await _repository.AddGame(newGame);
        }

        public async Task DeleteGame(int id)
        {
            await _repository.DeleteGame(id);
        }

        public async Task EditGame(GameViewModel oldGameViewModel, GameViewModel newGameViewModel)
        {
            var oldGame = new Game(oldGameViewModel);
            oldGame.DecideWinnerAndLoser();

            var newGame = new Game(newGameViewModel);
            newGame.DecideWinnerAndLoser();

            await _repository.EditGame(oldGame, newGame);
        }

        ///// <summary>
        ///// Applies the filter set in the GameFinderWindowViewModel
        ///// </summary>
        //public void ApplyFindGameFilter()
        //{
        //    try
        //    {
        //        var dataContext = _context.GameFinder.DataContext as GameFinderWindowViewModel;
        //        var guest = dataContext.GuestName;
        //        var host = dataContext.HostName;
        //        var games = (from game in _context.DbContextContext.Games
        //                     where ((game.GuestName == guest) && (game.HostName == host) && (game.SeasonID == Globals.SelectedSeason))
        //                     select game);

        //        LoadGames(games);

        //        _context.IsFindGameFilterApplied = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }
        //}

        ///// <summary>
        ///// Resets data in all data entry fields to their default values.
        ///// </summary>
        //public void ClearDataEntryControls()
        //{
        //    try
        //    {
        //        _context.GuestName = String.Empty;
        //        _context.GuestScore = 0;
        //        _context.HostName = String.Empty;
        //        _context.HostScore = 0;
        //        _context.IsPlayoffGame = false;
        //        _context.Notes = String.Empty;

        //        _context.AddGameControlVisibility = Visibility.Visible;
        //        _context.EditGameControlVisibility = Visibility.Hidden;
        //        _context.RemoveGameControlVisibility = Visibility.Hidden;

        //        // Set focus to GuestName field.
        //        _context.MoveFocusTo("GuestName");
        //    }
        //    catch (Exception ex)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }
        //}

        /////// <summary>
        /////// Edits each seasonTeam's data (games, points for, points against) from playoff games.
        /////// </summary>
        /////// <param name = "winner"></param>
        /////// <param name = "loser"></param>
        /////// <param name = "operation"></param>
        /////// <param name = "game"></param>
        /////// <param name = "winnerScore"></param>
        /////// <param name = "loserScore"></param>
        ////public static void EditDataFromPlayoffGames(Team winner, Team loser, Operation operation, Game game)
        ////{
        ////	try
        ////	{
        ////		EditWinLossDataFromPlayoffGames(winner, loser, operation, game);
        ////	}
        ////	catch ( Exception ex )
        ////	{
        ////		// _globals.ShowExceptionMessage(ex);
        ////	}
        ////}

        public async Task<GameViewModel> FindGameAsync(int? id)
        {
            var game = await _repository.FindGameAsync(id);
            return new GameViewModel(game);
        }

        public async Task<IEnumerable<GameViewModel>> GetGames(int selectedSeason, string selectedWeek, string guestSearchString, string hostSearchString)
        {
            var gameViewModels = new List<GameViewModel>();

            var games = await _repository.GetGames(selectedSeason, selectedWeek, guestSearchString, hostSearchString);
            foreach (var game in games)
            {
                gameViewModels.Add(new GameViewModel(game));
            }

            return gameViewModels;
        }

        public async Task<IEnumerable<TeamViewModel>> GetAllTeams()
        {
            var teamViewModels = new List<TeamViewModel>();

            var teams = await _repository.GetAllTeams();
            foreach (var team in teams)
            {
                teamViewModels.Add(new TeamViewModel(team));
            }

            return teamViewModels;
        }

        public async Task<IEnumerable<WeekViewModel>> GetWeeks(int selectedSeason)
        {
            var weekViewModels = new List<WeekViewModel>
            {
                new WeekViewModel(null)
            };

            var weeks = await _repository.GetWeeks(selectedSeason);
            foreach (var week in weeks)
            {
                weekViewModels.Add(new WeekViewModel(week));
            }

            return weekViewModels;
        }

        /// <summary>
        /// Edits each seasonTeam's data (games, points for, points against, adjusted points for, adjusted points against) from all games.
        /// </summary>
        /// <param name = "game"></param>
        /// <param name = "operation"></param>
        /// <summary>
        /// Loads _context GamesWindowViewModel object's Games collection.
        /// </summary>
        /// <param name = "games"></param>
        //public void LoadGames(IQueryable<Game> games)
        //{
        //    try
        //    {
        //        _context.Games = new ObservableCollection<Game>();
        //        foreach (var game in games)
        //        {
        //            _context.Games.Add(game);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }
        //}

        ///// <summary>
        ///// Populates data entry controls with data from selected game.
        ///// </summary>
        //public void PopulateDataEntryControls()
        //{
        //    try
        //    {
        //        var selectedGame = _context.SelectedGame;
        //        _context.Week = selectedGame.Week;
        //        _context.GuestName = selectedGame.GuestName;
        //        _context.GuestScore = selectedGame.GuestScore;
        //        _context.HostName = selectedGame.HostName;
        //        _context.HostScore = selectedGame.HostScore;
        //        _context.IsPlayoffGame = selectedGame.IsPlayoffGame;
        //        _context.Notes = selectedGame.Notes;

        //        _context.AddGameControlVisibility = Visibility.Hidden;
        //        _context.EditGameControlVisibility = Visibility.Visible;
        //        _context.RemoveGameControlVisibility = Visibility.Visible;
        //    }
        //    catch (Exception ex)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }
        //}

        public void SetSelectedSeason(IEnumerable<SeasonViewModel> seasons, int? seasonID)
        {
            _sharedService.SetSelectedSeason(seasons, seasonID, ref SelectedSeason);
        }

        public void SetSelectedWeek(int? week)
        {
            SelectedWeek = new WeekViewModel(week);
        }

        ///// <summary>
        ///// Sorts the Games collection by Week ascending, then by HostName ascending.
        ///// </summary>
        //public void SortGamesByDefaultOrder()
        //{
        //    try
        //    {
        //        var games = (from game in _context.DbContextContext.Games
        //                     where game.SeasonID == Globals.SelectedSeason
        //                     orderby game.Week descending
        //                     orderby game.ID descending
        //                     select game).
        //                     AsQueryable();

        //        LoadGames(games);
        //    }
        //    catch (Exception ex)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }
        //}
    }
}
