﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;
using EldredBrown.FootballApplicationWeb.Repositories;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public class GamePredictorService : IGamePredictorService
    {
        private readonly IGamePredictorRepository _repository;
        private readonly ISharedRepository _sharedRepository;

        public GamePredictorService(IGamePredictorRepository repository, ISharedRepository sharedRepository)
        {
            _repository = repository;
            _sharedRepository = sharedRepository;
        }

        public static int? GuestSeasonID { get; set; }
        public static int? HostSeasonID { get; set; }

        public void ApplyFilter(int? guestSeasonID, int? hostSeasonID)
        {
            if (guestSeasonID != null)
            {
                GuestSeasonID = guestSeasonID;
            }

            if (hostSeasonID != null)
            {
                HostSeasonID = hostSeasonID;
            }
        }

        public async Task GetGuestAndHostSeasonIds()
        {
            var seasons = await _sharedRepository.GetAllSeasons();
            var firstSeasonID = seasons.First().ID;

            // Get guest season.
            if (GuestSeasonID == null)
            {
                GuestSeasonID = firstSeasonID;
            }

            // Get host season.
            if (HostSeasonID == null)
            {
                HostSeasonID = firstSeasonID;
            }
        }

        public async Task<TeamSeasonViewModel> GetTeamSeason(string teamName, int seasonID)
        {
            var teamSeason = await _repository.GetTeamSeason(teamName, seasonID);
            return new TeamSeasonViewModel(teamSeason);
        }

        public async Task<IEnumerable<TeamSeasonViewModel>> GetTeamSeasons(int seasonID)
        {
            var teamSeasonViewModels = new List<TeamSeasonViewModel>();

            var teamSeasons = await _repository.GetTeamSeasons(seasonID);
            foreach (var teamSeason in teamSeasons)
            {
                teamSeasonViewModels.Add(new TeamSeasonViewModel(teamSeason));
            }

            return teamSeasonViewModels;
        }
    }
}