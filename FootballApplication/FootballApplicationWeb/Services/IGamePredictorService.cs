﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Services
{
    public interface IGamePredictorService
    {
        void ApplyFilter(int? guestSeasonID, int? hostSeasonID);
        Task GetGuestAndHostSeasonIds();
        Task<TeamSeasonViewModel> GetTeamSeason(string teamName, int seasonID);
        Task<IEnumerable<TeamSeasonViewModel>> GetTeamSeasons(int seasonID);
    }
}
