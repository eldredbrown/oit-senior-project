﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class GamesController : Controller
    {
        private FootballDbEntities db = new FootballDbEntities();
        //private static int? selectedWeek;

        // GET: Games
        public ActionResult Index(int? seasonID, int? week, string guestSearchString, string hostSearchString)
        {
            var seasonList = (from season in db.Seasons
                              orderby season.ID descending
                              select season.ID)
                              .ToList();
            ViewBag.SeasonID = new SelectList(seasonList, Globals.SelectedSeason);

            var weekList = new List<int>();
            var weekQuery = from g in db.Games
                            orderby g.Week
                            select g.Week;
            weekList.AddRange(weekQuery.Distinct());
            ViewBag.Week = new SelectList(weekList);

            var games = db.Games.Include(g => g.Season).Include(g => g.Guest).Include(g => g.Host).Include(g => g.Winner).Include(g => g.Loser);

            if (seasonID != null)
            {
                games = games.Where(g => g.SeasonID == seasonID);
            }

            if (week != null)
            {
                games = games.Where(g => g.Week == week);
            }

            if (!String.IsNullOrEmpty(guestSearchString) && !String.IsNullOrEmpty(hostSearchString))
            {
                games = games.Where(g => g.GuestName.Contains(guestSearchString) && g.HostName.Contains(hostSearchString));
            }

            return View(games.ToList());
        }

        // GET: Games/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID");
            ViewBag.GuestName = new SelectList(db.Teams, "Name", "Name");
            ViewBag.HostName = new SelectList(db.Teams, "Name", "Name");
            ViewBag.WinnerName = new SelectList(db.Teams, "Name", "Name");
            ViewBag.LoserName = new SelectList(db.Teams, "Name", "Name");
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SeasonID,Week,GuestName,GuestScore,HostName,HostScore,WinnerName,LoserName,IsPlayoffGame,Notes")] Game game)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Games.Add(game);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", game.SeasonID);
            ViewBag.GuestName = new SelectList(db.Teams, "Name", "Name", game.GuestName);
            ViewBag.HostName = new SelectList(db.Teams, "Name", "Name", game.HostName);
            ViewBag.WinnerName = new SelectList(db.Teams, "Name", "Name", game.WinnerName);
            ViewBag.LoserName = new SelectList(db.Teams, "Name", "Name", game.LoserName);
            return View(game);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", game.SeasonID);
            ViewBag.GuestName = new SelectList(db.Teams, "Name", "Name", game.GuestName);
            ViewBag.HostName = new SelectList(db.Teams, "Name", "Name", game.HostName);
            ViewBag.WinnerName = new SelectList(db.Teams, "Name", "Name", game.WinnerName);
            ViewBag.LoserName = new SelectList(db.Teams, "Name", "Name", game.LoserName);
            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SeasonID,Week,GuestName,GuestScore,HostName,HostScore,WinnerName,LoserName,IsPlayoffGame,Notes")] Game game)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(game).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", game.SeasonID);
            ViewBag.GuestName = new SelectList(db.Teams, "Name", "Name", game.GuestName);
            ViewBag.HostName = new SelectList(db.Teams, "Name", "Name", game.HostName);
            ViewBag.WinnerName = new SelectList(db.Teams, "Name", "Name", game.WinnerName);
            ViewBag.LoserName = new SelectList(db.Teams, "Name", "Name", game.LoserName);
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Find(id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Game game = db.Games.Find(id);
                db.Games.Remove(game);
                db.SaveChanges();
            }
            catch (RetryLimitExceededException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
