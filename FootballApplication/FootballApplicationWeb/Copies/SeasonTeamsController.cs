﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Controllers
{
    public class SeasonTeamsController : Controller
    {
        private FootballDbEntities db = new FootballDbEntities();

        // GET: SeasonTeams
        public ActionResult Index(int? seasonID, int? currentFilter, string sortOrder)
        {
            var seasonList = (from season in db.Seasons
                              orderby season.ID descending
                              select season.ID)
                              .ToList();
            ViewBag.SeasonID = new SelectList(seasonList);
            //ViewBag.SeasonID = new SelectList(seasonList, Globals.SelectedSeason);

            if (seasonID == null)
            {
                seasonID = currentFilter;
            }

            ViewBag.CurrentFilter = seasonID;

            ViewBag.TeamSortParm = sortOrder == "team_desc" ? "team_asc" : "team_desc";
            ViewBag.WinsSortParm = sortOrder == "wins_desc" ? "wins_asc" : "wins_desc";
            ViewBag.LossesSortParm = sortOrder == "losses_asc" ? "losses_desc" : "losses_asc";
            ViewBag.TiesSortParm = sortOrder == "ties_asc" ? "ties_desc" : "ties_asc";
            ViewBag.WinningPercentageSortParm = sortOrder == "win_pct_desc" ? "win_pct_asc" : "win_pct_desc";
            ViewBag.PointsForSortParm = sortOrder == "pf_desc" ? "pf_asc" : "pf_desc";
            ViewBag.PointsAgainstSortParm = sortOrder == "pa_asc" ? "pa_desc" : "pa_asc";
            ViewBag.PythagoreanWinsSortParm = sortOrder == "pyth_w_desc" ? "pyth_w_asc" : "pyth_w_desc";
            ViewBag.PythagoreanLossesSortParm = sortOrder == "pyth_l_asc" ? "pyth_l_desc" : "pyth_l_asc";

            var seasonTeams = db.SeasonTeams.Include(t => t.Team).Include(t => t.Season).Include(t => t.League).Include(t => t.Conference).Include(t => t.Division);

            if (seasonID != null)
            {
                seasonTeams = seasonTeams.Where(x => x.SeasonID == seasonID);

                //// TODO: Persist selected season to a Globals class field.
                //Globals.SelectedSeason = (int)seasonID;
            }

            //if (Globals.SelectedSeason != null)
            //{
            //    seasonTeams = seasonTeams.Where(x => x.SeasonID == Globals.SelectedSeason);
            //}

            switch (sortOrder)
            {
                case "team_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.Team.Name);
                    break;
                case "team_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.Team.Name);
                    break;
                case "wins_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.Wins);
                    break;
                case "wins_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.Wins);
                    break;
                case "losses_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.Losses);
                    break;
                case "losses_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.Losses);
                    break;
                case "ties_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.Ties);
                    break;
                case "ties_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.Ties);
                    break;
                case "win_pct_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.WinningPercentage);
                    break;
                case "win_pct_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.WinningPercentage);
                    break;
                case "pf_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.PointsFor);
                    break;
                case "pf_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.PointsFor);
                    break;
                case "pa_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.PointsAgainst);
                    break;
                case "pa_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.PointsAgainst);
                    break;
                case "pyth_w_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.PythagoreanWins);
                    break;
                case "pyth_w_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.PythagoreanWins);
                    break;
                case "pyth_l_asc":
                    seasonTeams = seasonTeams.OrderBy(ts => ts.PythagoreanLosses);
                    break;
                case "pyth_l_desc":
                    seasonTeams = seasonTeams.OrderByDescending(ts => ts.PythagoreanLosses);
                    break;
                default:
                    seasonTeams = seasonTeams.OrderBy(s => s.Team.Name);
                    break;
            }

            return View(seasonTeams.ToList());
        }

        // GET: SeasonTeams/Details/5
        public ActionResult Details(int? seasonID, string teamName)
        {
            if ((seasonID == null) && (String.IsNullOrEmpty(teamName)))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(seasonID, teamName);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Create
        public ActionResult Create()
        {
            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID");
            ViewBag.TeamName = new SelectList(db.Teams, "Name", "Name");
            ViewBag.LeagueName = new SelectList(db.Leagues, "Name", "LongName");
            ViewBag.ConferenceName = new SelectList(db.Conferences, "Name", "LongName");
            ViewBag.DivisionName = new SelectList(db.Divisions, "Name", "LeagueName");
            return View();
        }

        // POST: SeasonTeams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SeasonID,TeamName,LeagueName,ConferenceName,DivisionName,Games,Wins,Losses,Ties,WinningPercentage,PointsFor,PointsAgainst,PythagoreanWins,PythagoreanLosses,OffensiveAverage,OffensiveFactor,OffensiveIndex,DefensiveAverage,DefensiveFactor,DefensiveIndex,FinalPythagoreanWinningPercentage")] SeasonTeam seasonTeam)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.SeasonTeams.Add(seasonTeam);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", seasonTeam.SeasonID);
            ViewBag.TeamName = new SelectList(db.Teams, "Name", "Name", seasonTeam.TeamName);
            ViewBag.LeagueName = new SelectList(db.Leagues, "Name", "LongName", seasonTeam.LeagueName);
            ViewBag.ConferenceName = new SelectList(db.Conferences, "Name", "LongName", seasonTeam.ConferenceName);
            ViewBag.DivisionName = new SelectList(db.Divisions, "Name", "LeagueName", seasonTeam.DivisionName);
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", seasonTeam.SeasonID);
            ViewBag.TeamName = new SelectList(db.Teams, "Name", "Name", seasonTeam.TeamName);
            ViewBag.LeagueName = new SelectList(db.Leagues, "Name", "LongName", seasonTeam.LeagueName);
            ViewBag.ConferenceName = new SelectList(db.Conferences, "Name", "LongName", seasonTeam.ConferenceName);
            ViewBag.DivisionName = new SelectList(db.Divisions, "Name", "LeagueName", seasonTeam.DivisionName);
            return View(seasonTeam);
        }

        // POST: SeasonTeams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SeasonID,TeamName,LeagueName,ConferenceName,DivisionName,Games,Wins,Losses,Ties,WinningPercentage,PointsFor,PointsAgainst,PythagoreanWins,PythagoreanLosses,OffensiveAverage,OffensiveFactor,OffensiveIndex,DefensiveAverage,DefensiveFactor,DefensiveIndex,FinalPythagoreanWinningPercentage")] SeasonTeam seasonTeam)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(seasonTeam).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.SeasonID = new SelectList(db.Seasons, "ID", "ID", seasonTeam.SeasonID);
            ViewBag.TeamName = new SelectList(db.Teams, "Name", "Name", seasonTeam.TeamName);
            ViewBag.LeagueName = new SelectList(db.Leagues, "Name", "LongName", seasonTeam.LeagueName);
            ViewBag.ConferenceName = new SelectList(db.Conferences, "Name", "LongName", seasonTeam.ConferenceName);
            ViewBag.DivisionName = new SelectList(db.Divisions, "Name", "LeagueName", seasonTeam.DivisionName);
            return View(seasonTeam);
        }

        // GET: SeasonTeams/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
            if (seasonTeam == null)
            {
                return HttpNotFound();
            }
            return View(seasonTeam);
        }

        // POST: SeasonTeams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                SeasonTeam seasonTeam = db.SeasonTeams.Find(id);
                db.SeasonTeams.Remove(seasonTeam);
                db.SaveChanges();
            }
            catch (RetryLimitExceededException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
