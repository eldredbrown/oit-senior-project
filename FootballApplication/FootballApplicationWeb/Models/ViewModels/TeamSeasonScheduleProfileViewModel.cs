﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class TeamSeasonScheduleProfileViewModel
    {
        public TeamSeasonScheduleProfileViewModel(usp_GetTeamSeasonScheduleProfile_Result result)
        {
            this.Opponent = result.Opponent;
            this.GamePointsFor = result.GamePointsFor;
            this.GamePointsAgainst = result.GamePointsAgainst;
            this.OpponentWins = result.OpponentWins;
            this.OpponentLosses = result.OpponentLosses;
            this.OpponentTies = result.OpponentTies;
            this.OpponentWinningPercentage = result.OpponentWinningPercentage;
            this.OpponentWeightedGames = result.OpponentWeightedGames;
            this.OpponentWeightedPointsFor = result.OpponentWeightedPointsFor;
            this.OpponentWeightedPointsAgainst = result.OpponentWeightedPointsAgainst;
        }

        public string Opponent { get; set; }
        public Nullable<double> GamePointsFor { get; set; }
        public Nullable<double> GamePointsAgainst { get; set; }
        public Nullable<double> OpponentWins { get; set; }
        public Nullable<double> OpponentLosses { get; set; }
        public Nullable<double> OpponentTies { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.000}")]
        public Nullable<double> OpponentWinningPercentage { get; set; }

        public Nullable<double> OpponentWeightedGames { get; set; }
        public Nullable<double> OpponentWeightedPointsFor { get; set; }
        public Nullable<double> OpponentWeightedPointsAgainst { get; set; }
    }
}