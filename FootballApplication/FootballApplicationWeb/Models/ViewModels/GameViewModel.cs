﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class GameViewModel
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public GameViewModel()
        {
        }

        /// <summary>
        /// Constructor for mapping Game objects
        /// </summary>
        /// <param name="game">The Game object to be mapped</param>
        public GameViewModel(Game game)
        {
            this.ID = game.ID;
            this.SeasonID = game.SeasonID;
            this.Week = game.Week;
            this.GuestName = game.GuestName;
            this.GuestScore = game.GuestScore;
            this.HostName = game.HostName;
            this.HostScore = game.HostScore;
            this.WinnerName = game.WinnerName;
            this.LoserName = game.LoserName;
            this.IsPlayoffGame = game.IsPlayoffGame;
            this.Notes = game.Notes;
        }

        public int ID { get; set; }

        [Required(ErrorMessage = "A season is required.")]
        [DisplayName("Season")]
        public int SeasonID { get; set; }

        [Required(ErrorMessage = "A week is required.")]
        public int Week { get; set; }

        [Required(ErrorMessage = "The guest is required.")]
        [DisplayName("Guest")]
        [CompareForInequality("HostName")]
        public string GuestName { get; set; }

        [Required(ErrorMessage = "The guest's score is required.")]
        [DisplayName("Guest Score")]
        public double GuestScore { get; set; }

        [Required(ErrorMessage = "The host is required.")]
        [DisplayName("Host")]
        [CompareForInequality("GuestName")]
        public string HostName { get; set; }

        [Required(ErrorMessage = "The host's score is required.")]
        [DisplayName("Host Score")]
        public double HostScore { get; set; }

        [DisplayName("Winner")]
        public string WinnerName { get; set; }

        [DisplayName("Loser")]
        public string LoserName { get; set; }

        [DisplayName("Playoff Game?")]
        public bool IsPlayoffGame { get; set; }

        public string Notes { get; set; }
    }
}