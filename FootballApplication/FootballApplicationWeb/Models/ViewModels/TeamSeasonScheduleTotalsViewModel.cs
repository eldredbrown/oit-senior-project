﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class TeamSeasonScheduleTotalsViewModel
    {
        public TeamSeasonScheduleTotalsViewModel(usp_GetTeamSeasonScheduleTotals_Result result)
        {
            this.Games = result.Games;
            this.PointsFor = result.PointsFor;
            this.PointsAgainst = result.PointsAgainst;
            this.ScheduleWins = result.ScheduleWins;
            this.ScheduleLosses = result.ScheduleLosses;
            this.ScheduleTies = result.ScheduleTies;
            this.ScheduleWinningPercentage = result.ScheduleWinningPercentage;
            this.ScheduleGames = result.ScheduleGames;
            this.SchedulePointsFor = result.SchedulePointsFor;
            this.SchedulePointsAgainst = result.SchedulePointsAgainst;
        }

        public Nullable<int> Games { get; set; }
        public Nullable<double> PointsFor { get; set; }
        public Nullable<double> PointsAgainst { get; set; }
        public Nullable<double> ScheduleWins { get; set; }
        public Nullable<double> ScheduleLosses { get; set; }
        public Nullable<double> ScheduleTies { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.000}")]
        public Nullable<double> ScheduleWinningPercentage { get; set; }

        public Nullable<double> ScheduleGames { get; set; }
        public Nullable<double> SchedulePointsFor { get; set; }
        public Nullable<double> SchedulePointsAgainst { get; set; }
    }
}