﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class TeamSeasonDetailsViewModel
    {
        public TeamSeasonViewModel TeamSeason { get; set; }
        public IEnumerable<TeamSeasonScheduleProfileViewModel> TeamSeasonScheduleProfile { get; set; }
        public TeamSeasonScheduleTotalsViewModel TeamSeasonScheduleTotals { get; set; }
        public TeamSeasonScheduleAveragesViewModel TeamSeasonScheduleAverages { get; set; }
    }
}
