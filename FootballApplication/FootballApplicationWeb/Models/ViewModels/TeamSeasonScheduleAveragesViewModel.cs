﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class TeamSeasonScheduleAveragesViewModel
    {
        public TeamSeasonScheduleAveragesViewModel(usp_GetTeamSeasonScheduleAverages_Result result)
        {
            this.PointsFor = result.PointsFor;
            this.PointsAgainst = result.PointsAgainst;
            this.SchedulePointsFor = result.SchedulePointsFor;
            this.SchedulePointsAgainst = result.SchedulePointsAgainst;
        }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public Nullable<double> PointsFor { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public Nullable<double> PointsAgainst { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public Nullable<double> SchedulePointsFor { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}")]
        public Nullable<double> SchedulePointsAgainst { get; set; }
    }
}