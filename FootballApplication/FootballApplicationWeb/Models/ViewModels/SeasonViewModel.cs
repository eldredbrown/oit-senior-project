﻿namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class SeasonViewModel
    {
        /// <summary>
        /// Constructor for mapping Season objects
        /// </summary>
        /// <param name="season">The Season object to be mapped</param>
        public SeasonViewModel(Season season)
        {
            this.ID = season.ID;
        }

        public int ID { get; set; }
    }
}