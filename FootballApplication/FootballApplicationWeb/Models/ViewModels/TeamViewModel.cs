﻿namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class TeamViewModel
    {
        /// <summary>
        /// Constructor for mapping Team objects
        /// </summary>
        /// <param name="team">The Team object to be mapped</param>
        public TeamViewModel(Team team)
        {
            this.Name = team.Name;
        }

        public string Name { get; set; }
    }
}