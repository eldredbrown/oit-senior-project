﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace EldredBrown.FootballApplicationWeb.Models.ViewModels
{
    public class SeasonStandingsResultViewModel
    {
        public SeasonStandingsResultViewModel(usp_GetSeasonStandings_Result result)
        {
            this.Team = result.Team;
            this.Conference = result.Conference;
            this.Division = result.Division;
            this.Wins = result.Wins;
            this.Losses = result.Losses;
            this.Ties = result.Ties;
            this.WinningPercentage = result.WinningPercentage;
            this.PointsFor = result.PointsFor;
            this.PointsAgainst = result.PointsAgainst;
            this.AvgPointsFor = result.AvgPointsFor;
            this.AvgPointsAgainst = result.AvgPointsAgainst;
        }

        public string Team { get; set; }
        public string Conference { get; set; }
        public string Division { get; set; }
        public Nullable<double> Wins { get; set; }
        public Nullable<double> Losses { get; set; }
        public Nullable<double> Ties { get; set; }

        [DisplayName("W-Pct.")]
        [DisplayFormat(DataFormatString = "{0:#.000}")]
        public Nullable<double> WinningPercentage { get; set; }

        public Nullable<double> PointsFor { get; set; }
        public Nullable<double> PointsAgainst { get; set; }

        [DisplayName("OA")]
        [DisplayFormat(DataFormatString = "{0:N1}")]
        public Nullable<double> AvgPointsFor { get; set; }

        [DisplayName("OA")]
        [DisplayFormat(DataFormatString = "{0:N1}")]
        public Nullable<double> AvgPointsAgainst { get; set; }
    }
}