﻿using System;
using EldredBrown.FootballApplicationShared.Interfaces;
using EldredBrown.FootballApplicationWeb.Models.ViewModels;

namespace EldredBrown.FootballApplicationWeb.Models
{
    public partial class Game
    {
        /// <summary>
        /// Default constructor
        /// Initalizes a new instance of the Game class with default values
        /// </summary>
        public Game()
        {
        }

        /// <summary>
        /// Copy constructor
        /// Initializes a new instance of the Game class by copying an existing Game instance
        /// </summary>
        /// <param name="src"></param>
        public Game(Game src)
        {
            this.ID = 0;
            this.SeasonID = src.SeasonID;
            this.Week = src.Week;
            this.GuestName = src.GuestName;
            this.GuestScore = src.GuestScore;
            this.HostName = src.HostName;
            this.HostScore = src.HostScore;
            this.WinnerName = src.WinnerName;
            this.WinnerScore = src.WinnerScore;
            this.LoserName = src.LoserName;
            this.LoserScore = src.LoserScore;
            this.IsPlayoffGame = src.IsPlayoffGame;
            this.Notes = src.Notes;
        }

        /// <summary>
        /// Mapping constructor
        /// Initializes a new instance of the Game class by mapping values from a GameViewModel object
        /// </summary>
        /// <param name="gameViewModel">The GameViewModel object to be mapped</param>
        public Game(GameViewModel gameViewModel)
        {
            this.ID = gameViewModel.ID;
            this.SeasonID = gameViewModel.SeasonID;
            this.Week = gameViewModel.Week;
            this.GuestName = gameViewModel.GuestName;
            this.GuestScore = gameViewModel.GuestScore;
            this.HostName = gameViewModel.HostName;
            this.HostScore = gameViewModel.HostScore;
            this.WinnerName = gameViewModel.WinnerName;
            this.LoserName = gameViewModel.LoserName;
            this.IsPlayoffGame = gameViewModel.IsPlayoffGame;
            this.Notes = gameViewModel.Notes;
        }

        /// <summary>
        /// Decides this game's winner and loser.
        /// </summary>
        public virtual void DecideWinnerAndLoser()
        {
            try
            {
                // Declare the winner to be the team that scored more points in the game.
                if (GuestScore > HostScore)
                {
                    WinnerName = GuestName;
                    WinnerScore = GuestScore;
                    LoserName = HostName;
                    LoserScore = HostScore;
                }
                else if (HostScore > GuestScore)
                {
                    WinnerName = HostName;
                    WinnerScore = HostScore;
                    LoserName = GuestName;
                    LoserScore = GuestScore;
                }
                else
                {
                    Winner = null;
                    Loser = null;
                }
            }
            catch (Exception)
            {
                //_globals.ShowExceptionMessage(ex);
            }
        }
    }

    public partial class TeamSeason: ITeamSeason
    {
    }
}