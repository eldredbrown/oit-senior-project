﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public class GamesRepository : IGamesRepository
    {
        private FootballDbEntities _dbContext;

        private readonly ICalculator _calculator;

        public GamesRepository(FootballDbEntities dbContext, ICalculator calculator)
        {
            _dbContext = dbContext;
            _calculator = calculator;
        }

        public async Task AddGame(Game newGame)
        {
            _dbContext.Games.Add(newGame);

            await AddGameToTeams(newGame);

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Adds data of a new game to the database TeamSeasons table
        /// </summary>
        public async Task AddGameToTeams(Game newGame)
        {
            try
            {
                //newGame = StoreNewGameValues(newGame);
                await EditTeams(newGame, Direction.Up);
            }
            catch (Exception)
            {
                // _globals.ShowExceptionMessage(ex);
            }
        }

        public async Task DeleteGame(int id)
        {
            var oldGame = await _dbContext.Games.FindAsync(id);
            var copyGame = new Game(oldGame);

            _dbContext.Games.Remove(oldGame);

            await DeleteGameFromTeams(copyGame);

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Removes data of an existing game from the database TeamSeasons table
        /// </summary>
        public async Task DeleteGameFromTeams(Game oldGame)
        {
            try
            {
                await EditTeams(oldGame, Direction.Down);
            }
            catch (Exception)
            {
                //_globals.ShowExceptionMessage(ex);
            }
        }

        public async Task EditGame(Game oldGame, Game newGame)
        {
            _dbContext.Entry(newGame).State = EntityState.Modified;

            await EditGameInTeams(oldGame, newGame);

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Edits data of an existing game in the database TeamSeasons table
        /// </summary>
        public async Task EditGameInTeams(Game oldGame, Game newGame)
        {
            try
            {
                //oldGame = StoreOldGameValues(oldGame);
                await EditTeams(oldGame, Direction.Down);

                //newGame = StoreNewGameValues(newGame);
                await EditTeams(newGame, Direction.Up);
            }
            catch (Exception)
            {
                //_globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Edits all scoring data for the specified Team
        /// </summary>
        /// <param name = "teamName"></param>
        /// <param name = "operation"></param>
        /// <param name = "teamScore"></param>
        /// <param name = "opponentScore"></param>
        public async Task EditScoringDataByTeam(Game game, string teamName, Operation operation, double teamScore, double opponentScore)
        {
            var teamSeason = await GetGameTeamSeason(game, teamName);

            teamSeason.PointsFor = operation(teamSeason.PointsFor, teamScore);
            teamSeason.PointsAgainst = operation(teamSeason.PointsAgainst, opponentScore);

            var pythPct = _calculator.CalculatePythagoreanWinningPercentage(teamSeason);
            if (pythPct == null)
            {
                teamSeason.PythagoreanWins = 0;
                teamSeason.PythagoreanLosses = 0;
            }
            else
            {
                teamSeason.PythagoreanWins = _calculator.Multiply((double)pythPct, teamSeason.Games);
                teamSeason.PythagoreanLosses = _calculator.Multiply((double)(1 - pythPct), teamSeason.Games);
            }
        }

        /// <summary>
        /// Edits the DataModel's Teams table with data from the specified game.
        /// </summary>
        /// <param name = "game"></param>
        /// <param name = "direction"></param>
        public async Task EditTeams(Game game, Direction direction)
        {
            try
            {
                Operation operation;

                // Decide whether the teams need to be edited up or down.
                // Up for new game, down then up for edited game, down for deleted game.
                switch (direction)
                {
                    case Direction.Up:
                        operation = new Operation(_calculator.Add);
                        break;

                    case Direction.Down:
                        operation = new Operation(_calculator.Subtract);
                        break;

                    default:
                        throw new ArgumentException("direction");
                }

                await ProcessGame(game, operation);
            }
            catch (Exception)
            {
                // _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Edits each seasonTeam's data (wins, losses, and ties) from all games.
        /// </summary>
        /// <param name = "game"></param>
        /// <param name = "operation"></param>
        public async Task EditWinLossData(Game game, Operation operation)
        {
            try
            {
                var guestSeason = await GetGameTeamSeason(game, game.GuestName);
                guestSeason.Games = (int)operation(guestSeason.Games, 1);

                var hostSeason = await GetGameTeamSeason(game, game.HostName);
                hostSeason.Games = (int)operation(hostSeason.Games, 1);

                var winner = game.WinnerName;
                var loser = game.LoserName;
                TeamSeason winnerSeason = null;
                TeamSeason loserSeason = null;
                if (!(String.IsNullOrEmpty(winner) || String.IsNullOrEmpty(loser)))
                {
                    winnerSeason = await GetGameTeamSeason(game, winner);
                    loserSeason = await GetGameTeamSeason(game, loser);
                }

                if ((winnerSeason == null) && (loserSeason == null))
                {
                    guestSeason.Ties = (int)operation(guestSeason.Ties, 1);
                    hostSeason.Ties = (int)operation(hostSeason.Ties, 1);
                }
                else
                {
                    winnerSeason.Wins = (int)operation(winnerSeason.Wins, 1);
                    loserSeason.Losses = (int)operation(loserSeason.Losses, 1);
                }

                guestSeason.WinningPercentage = _calculator.CalculateWinningPercentage(guestSeason);
                hostSeason.WinningPercentage = _calculator.CalculateWinningPercentage(hostSeason);
            }
            catch (Exception)
            {
                // _globals.ShowExceptionMessage(ex);
            }
        }

        public async Task<Game> FindGameAsync(int? id)
        {
            return await _dbContext.Games.FindAsync(id);
        }

        public async Task<Team> FindTeamAsync(string name)
        {
            return await _dbContext.Teams.Where(t => t.Name == name).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            return await _dbContext.Teams.ToListAsync();
        }

        public async Task<IEnumerable<Game>> GetGames(int selectedSeason, string selectedWeek, string guestSearchString, string hostSearchString)
        {
            var games = _dbContext.Games.Include(g => g.Season)
                                .Include(g => g.Guest)
                                .Include(g => g.Host)
                                .Include(g => g.Winner)
                                .Include(g => g.Loser)
                                .Where(g => g.SeasonID == selectedSeason);

            if (!String.IsNullOrEmpty(selectedWeek))
            {
                games = games.Where(g => g.Week.ToString() == selectedWeek);
            }

            if (!String.IsNullOrEmpty(guestSearchString) && !String.IsNullOrEmpty(hostSearchString))
            {
                games = games.Where(g => g.GuestName.Contains(guestSearchString))
                             .Where(g => g.HostName.Contains(hostSearchString));
            }

            return await games.ToListAsync();
        }

        /// <summary>
        /// Gets the TeamSeason object for the specified team and the selected season
        /// </summary>
        /// <param name="teamName"></param>
        /// <returns></returns>
        public async Task<TeamSeason> GetGameTeamSeason(Game game, string teamName)
        {
            return await _dbContext.TeamSeasons.Where(ts => (ts.TeamName == teamName))
                                       .Where(ts => (ts.SeasonID == game.SeasonID))
                                       .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<int>> GetWeeks(int selectedSeason)
        {
            return await _dbContext.Games.OrderBy(g => g.Week)
                                 .Where(g => g.SeasonID == selectedSeason)
                                 .Select(g => g.Week)
                                 .Distinct()
                                 .ToListAsync();
        }

        public async Task ProcessGame(Game game, Operation operation)
        {
            try
            {
                await EditWinLossData(game, operation);

                await EditScoringDataByTeam(game, game.GuestName, operation, game.GuestScore, game.HostScore);
                await EditScoringDataByTeam(game, game.HostName, operation, game.HostScore, game.GuestScore);
            }
            catch (Exception)
            {
                // _globals.ShowExceptionMessage(ex);
            }
        }

        ///// <summary>
        ///// Stores values from the data entry controls into a Game instance.
        ///// </summary>
        ///// <returns></returns>
        //public async Task<Game> StoreNewGameValues(Game game)
        //{
        //    Game newGame = new Game();

        //    try
        //    {
        //        newGame.ID = 0;
        //        newGame.SeasonID = game.SeasonID;
        //        newGame.Week = game.Week;
        //        newGame.Guest = FindTeamAsync(game.GuestName);
        //        newGame.GuestScore = game.GuestScore;
        //        newGame.Host = FindTeamAsync(game.HostName);
        //        newGame.HostScore = game.HostScore;
        //        newGame.Winner = game.Winner;
        //        newGame.WinnerScore = game.WinnerScore;
        //        newGame.Loser = game.Loser;
        //        newGame.LoserScore = game.LoserScore;
        //        newGame.IsPlayoffGame = game.IsPlayoffGame;
        //        newGame.Notes = game.Notes;
        //    }
        //    catch (Exception)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }

        //    return newGame;
        //}

        ///// <summary>
        ///// Stores values from the selected game into a Game instance.
        ///// </summary>
        //public async Task<Game> StoreOldGameValues(Game game)
        //{
        //    var oldGame = new Game();

        //    try
        //    {
        //        oldGame.ID = 0;
        //        oldGame.SeasonID = game.SeasonID;
        //        oldGame.Week = game.Week;
        //        oldGame.GuestScore = game.GuestScore;
        //        oldGame.HostScore = game.HostScore;
        //        oldGame.WinnerScore = game.WinnerScore;
        //        oldGame.LoserScore = game.LoserScore;
        //        oldGame.IsPlayoffGame = game.IsPlayoffGame;
        //        oldGame.Notes = game.Notes;
        //    }
        //    catch (Exception)
        //    {
        //        // _globals.ShowExceptionMessage(ex);
        //    }

        //    return oldGame;
        //}

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }
    }
}
