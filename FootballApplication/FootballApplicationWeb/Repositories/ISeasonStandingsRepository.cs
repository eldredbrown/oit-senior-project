﻿using System.Data.Entity.Core.Objects;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public interface ISeasonStandingsRepository
    {
        ObjectResult<usp_GetSeasonStandings_Result> GetSeasonStandings(int? selectedSeason, bool? groupByDivision);
    }
}