﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public interface ITeamSeasonsRepository
    {
        Task<TeamSeason> FindTeamSeasonAsync(string teamName, int? seasonID);
        Task<IEnumerable<TeamSeason>> GetTeamSeasons(int seasonID, string sortOrder);
        ObjectResult<usp_GetTeamSeasonScheduleAverages_Result> GetTeamSeasonScheduleAverages(string teamName, int? seasonID);
        ObjectResult<usp_GetTeamSeasonScheduleProfile_Result> GetTeamSeasonScheduleProfile(string teamName, int? seasonID);
        ObjectResult<usp_GetTeamSeasonScheduleTotals_Result> GetTeamSeasonScheduleTotals(string teamName, int? seasonID);
        Task UpdateRankings(int selectedSeason);
        void UpdateRankingsByTeamSeason(int selectedSeason, TeamSeason teamSeason);
    }
}