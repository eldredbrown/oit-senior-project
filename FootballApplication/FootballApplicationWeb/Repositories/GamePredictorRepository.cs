﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public class GamePredictorRepository : IGamePredictorRepository
    {
        private FootballDbEntities _dbContext;

        public GamePredictorRepository(FootballDbEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TeamSeason> GetTeamSeason(string teamName, int seasonID)
        {
            return await _dbContext.TeamSeasons.Where(ts => (ts.TeamName == teamName) && (ts.SeasonID == seasonID))
                                       .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TeamSeason>> GetTeamSeasons(int seasonID)
        {
            return await _dbContext.TeamSeasons.OrderBy(ts => ts.TeamName).Where(ts => ts.SeasonID == seasonID).ToListAsync();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }
    }
}