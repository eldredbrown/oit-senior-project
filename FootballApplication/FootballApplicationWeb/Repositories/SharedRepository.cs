﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public class SharedRepository : ISharedRepository
    {
        private FootballDbEntities _dbContext;

        public SharedRepository(FootballDbEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<Season>> GetAllSeasons()
        {
            return await _dbContext.Seasons.OrderByDescending(s => s.ID).ToListAsync();
        }
    }
}
