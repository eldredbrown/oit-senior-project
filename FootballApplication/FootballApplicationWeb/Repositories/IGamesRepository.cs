﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public interface IGamesRepository
    {
        Task AddGame(Game newGame);
        Task AddGameToTeams(Game newGame);
        Task DeleteGame(int id);
        Task DeleteGameFromTeams(Game oldGame);
        Task EditGame(Game oldGame, Game newGame);
        Task EditGameInTeams(Game oldGame, Game newGame);
        Task EditScoringDataByTeam(Game game, string teamName, Operation operation, double teamScore, double opponentScore);
        Task EditTeams(Game game, Direction direction);
        Task EditWinLossData(Game game, Operation operation);
        Task<Game> FindGameAsync(int? id);
        Task<Team> FindTeamAsync(string name);
        Task<IEnumerable<Team>> GetAllTeams();
        Task<IEnumerable<Game>> GetGames(int selectedSeason, string selectedWeek, string guestSearchString, string hostSearchString);
        Task<TeamSeason> GetGameTeamSeason(Game game, string teamName);
        Task<IEnumerable<int>> GetWeeks(int selectedSeason);
        Task ProcessGame(Game game, Operation operation);
    }
}