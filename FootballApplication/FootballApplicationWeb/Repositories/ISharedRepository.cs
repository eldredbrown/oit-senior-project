﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public interface ISharedRepository
    {
        Task<IEnumerable<Season>> GetAllSeasons();
    }
}