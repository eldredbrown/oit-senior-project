﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public class SeasonStandingsRepository : ISeasonStandingsRepository
    {
        private FootballDbEntities _dbContext;

        public SeasonStandingsRepository(FootballDbEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public ObjectResult<usp_GetSeasonStandings_Result> GetSeasonStandings(int? selectedSeason, bool? groupByDivision)
        {
            return _dbContext.usp_GetSeasonStandings(selectedSeason, groupByDivision);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }
    }
}