﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public interface IGamePredictorRepository
    {
        Task<TeamSeason> GetTeamSeason(string teamName, int seasonID);
        Task<IEnumerable<TeamSeason>> GetTeamSeasons(int seasonID);
    }
}