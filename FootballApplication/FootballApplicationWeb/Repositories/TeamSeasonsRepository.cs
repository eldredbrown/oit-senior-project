﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models;

namespace EldredBrown.FootballApplicationWeb.Repositories
{
    public class TeamSeasonsRepository : ITeamSeasonsRepository
    {
        private FootballDbEntities _dbContext;

        public TeamSeasonsRepository(FootballDbEntities dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TeamSeason> FindTeamSeasonAsync(string teamName, int? seasonID)
        {
            return await _dbContext.TeamSeasons.FindAsync(teamName, seasonID);
        }

        public async Task<IEnumerable<TeamSeason>> GetTeamSeasons(int seasonID, string sortOrder)
        {
            var teamSeasons = _dbContext.TeamSeasons.Include(t => t.Team)
                                            .Include(t => t.Season)
                                            .Include(t => t.League)
                                            .Include(t => t.Conference)
                                            .Include(t => t.Division)
                                            .Where(x => x.SeasonID == seasonID);

            switch (sortOrder)
            {
                case "team_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.TeamName);
                    break;
                case "team_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.TeamName);
                    break;
                case "wins_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.Wins);
                    break;
                case "wins_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.Wins);
                    break;
                case "losses_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.Losses);
                    break;
                case "losses_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.Losses);
                    break;
                case "ties_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.Ties);
                    break;
                case "ties_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.Ties);
                    break;
                case "win_pct_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.WinningPercentage);
                    break;
                case "win_pct_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.WinningPercentage);
                    break;
                case "pf_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.PointsFor);
                    break;
                case "pf_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.PointsFor);
                    break;
                case "pa_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.PointsAgainst);
                    break;
                case "pa_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.PointsAgainst);
                    break;
                case "pyth_w_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.PythagoreanWins);
                    break;
                case "pyth_w_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.PythagoreanWins);
                    break;
                case "pyth_l_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.PythagoreanLosses);
                    break;
                case "pyth_l_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.PythagoreanLosses);
                    break;
                case "off_avg_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.OffensiveAverage);
                    break;
                case "off_avg_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.OffensiveAverage);
                    break;
                case "off_factor_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.OffensiveFactor);
                    break;
                case "off_factor_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.OffensiveFactor);
                    break;
                case "off_index_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.OffensiveIndex);
                    break;
                case "off_index_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.OffensiveIndex);
                    break;
                case "def_avg_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.DefensiveAverage);
                    break;
                case "def_avg_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.DefensiveAverage);
                    break;
                case "def_factor_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.DefensiveFactor);
                    break;
                case "def_factor_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.DefensiveFactor);
                    break;
                case "def_index_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.DefensiveIndex);
                    break;
                case "def_index_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.DefensiveIndex);
                    break;
                case "fin_pyth_pct_asc":
                    teamSeasons = teamSeasons.OrderBy(ts => ts.FinalPythagoreanWinningPercentage);
                    break;
                case "fin_pyth_pct_desc":
                    teamSeasons = teamSeasons.OrderByDescending(ts => ts.FinalPythagoreanWinningPercentage);
                    break;
                default:
                    teamSeasons = teamSeasons.OrderBy(ts => ts.TeamName);
                    break;
            }

            return await teamSeasons.ToListAsync();
        }

        public ObjectResult<usp_GetTeamSeasonScheduleAverages_Result> GetTeamSeasonScheduleAverages(string teamName, int? seasonID)
        {
            return _dbContext.usp_GetTeamSeasonScheduleAverages(teamName, seasonID);
        }

        public ObjectResult<usp_GetTeamSeasonScheduleProfile_Result> GetTeamSeasonScheduleProfile(string teamName, int? seasonID)
        {
            return _dbContext.usp_GetTeamSeasonScheduleProfile(teamName, seasonID);
        }

        public ObjectResult<usp_GetTeamSeasonScheduleTotals_Result> GetTeamSeasonScheduleTotals(string teamName, int? seasonID)
        {
            return _dbContext.usp_GetTeamSeasonScheduleTotals(teamName, seasonID);
        }

        public async Task UpdateRankings(int selectedSeason)
        {
            var leagueSeasonTotals = _dbContext.usp_GetLeagueSeasonTotals(selectedSeason).FirstOrDefault();

            var leagueSeason = await _dbContext.LeagueSeasons.Where(ls => ls.SeasonID == selectedSeason).FirstOrDefaultAsync();
            leagueSeason.TotalGames = (double)leagueSeasonTotals.TotalGames;
            leagueSeason.TotalPoints = (double)leagueSeasonTotals.TotalPoints;
            leagueSeason.AveragePoints = (double)leagueSeasonTotals.AveragePoints;
            await _dbContext.SaveChangesAsync();

            var teamSeasons = await _dbContext.TeamSeasons.Where(ts => ts.SeasonID == selectedSeason).ToListAsync();

            // This looks like the place where I want to make maximum use of parallel threading.
            //Parallel.ForEach(teamSeasons, teamSeason => UpdateRankingsByTeamSeason(teamSeason));

            foreach (var teamSeason in teamSeasons)
            {
                UpdateRankingsByTeamSeason(selectedSeason, teamSeason);
            }

            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the rankings for the specified team and the selected season
        /// </summary>
        /// <param name="seasonTeam"></param>
        public virtual void UpdateRankingsByTeamSeason(int selectedSeason, TeamSeason teamSeason)
        {
            var dbLock = new object();
            var calculator = new Calculator();

            try
            {
                usp_GetTeamSeasonScheduleTotals_Result teamSeasonScheduleTotals = null;
                usp_GetTeamSeasonScheduleAverages_Result teamSeasonScheduleAverages = null;

                lock (dbLock)
                {
                    teamSeasonScheduleTotals = _dbContext.usp_GetTeamSeasonScheduleTotals(teamSeason.TeamName, selectedSeason).FirstOrDefault();
                    teamSeasonScheduleAverages = _dbContext.usp_GetTeamSeasonScheduleAverages(teamSeason.TeamName, selectedSeason).FirstOrDefault();
                }

                if ((teamSeasonScheduleTotals != null) && (teamSeasonScheduleAverages != null) && (teamSeasonScheduleTotals.ScheduleGames != null))
                {
                    LeagueSeason leagueSeason;
                    lock (dbLock)
                    {
                        leagueSeason = _dbContext.LeagueSeasons.Where(ls => ls.LeagueName == teamSeason.LeagueName)
                                                       .Where(ls => ls.SeasonID == teamSeason.SeasonID)
                                                       .FirstOrDefault();
                    }

                    teamSeason.OffensiveAverage = calculator.Divide(teamSeason.PointsFor, teamSeason.Games);
                    teamSeason.DefensiveAverage = calculator.Divide(teamSeason.PointsAgainst, teamSeason.Games);

                    teamSeason.OffensiveFactor =
                        calculator.Divide((double)teamSeason.OffensiveAverage, (double)teamSeasonScheduleAverages.PointsAgainst);
                    teamSeason.DefensiveFactor =
                        calculator.Divide((double)teamSeason.DefensiveAverage, (double)teamSeasonScheduleAverages.PointsFor);

                    teamSeason.OffensiveIndex = (teamSeason.OffensiveAverage + teamSeason.OffensiveFactor * leagueSeason.AveragePoints) / 2;
                    teamSeason.DefensiveIndex = (teamSeason.DefensiveAverage + teamSeason.DefensiveFactor * leagueSeason.AveragePoints) / 2;

                    teamSeason.FinalPythagoreanWinningPercentage = calculator.CalculatePythagoreanWinningPercentage(teamSeason);
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == "Nullable object must have a value.")
                {
                    // Ignore exception.
                }
                else
                {
                    //_globals.ShowExceptionMessage(ex, "InvalidOperationException: " + TeamSeason.Team.Name);
                }
            }
            catch (Exception)
            {
                //_globals.ShowExceptionMessage(ex.InnerException, "Exception: " + TeamSeason.Team.Name);
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }
        }
    }
}
