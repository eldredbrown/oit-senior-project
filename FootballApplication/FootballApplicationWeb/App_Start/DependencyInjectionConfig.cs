﻿using System;
using System.Reflection;
using System.Web.Mvc;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWeb.Models;
using EldredBrown.FootballApplicationWeb.Repositories;
using EldredBrown.FootballApplicationWeb.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;

namespace EldredBrown.FootballApplicationWeb.App_Start
{
    public static class DependencyInjectionConfig
    {
        public static void Register()
        {
            // Create the container as usual.
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            // Register your types, for instance:
            container.Register<ITeamSeasonsService, TeamSeasonsService>(Lifestyle.Scoped);
            container.Register<IGamesService, GamesService>(Lifestyle.Scoped);
            container.Register<ISeasonStandingsService, SeasonStandingsService>(Lifestyle.Scoped);
            container.Register<IGamePredictorService, GamePredictorService>(Lifestyle.Scoped);
            container.Register<ISharedService, SharedService>(Lifestyle.Scoped);
            container.Register<ITeamSeasonsRepository, TeamSeasonsRepository>(Lifestyle.Scoped);
            container.Register<IGamesRepository, GamesRepository>(Lifestyle.Scoped);
            container.Register<ISeasonStandingsRepository, SeasonStandingsRepository>(Lifestyle.Scoped);
            container.Register<IGamePredictorRepository, GamePredictorRepository>(Lifestyle.Scoped);
            container.Register<ISharedRepository, SharedRepository>(Lifestyle.Scoped);
            container.Register<ICalculator, Calculator>(Lifestyle.Scoped);
            container.Register<FootballDbEntities, FootballDbEntities>(Lifestyle.Scoped);

            // This is an extension method from the integration package.
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}