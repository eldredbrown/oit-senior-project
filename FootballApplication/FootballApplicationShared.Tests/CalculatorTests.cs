﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EldredBrown.FootballApplicationShared.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void CalculatorTest()
        {
            // Arrange
            // Act
            var testObject = new Calculator();

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(Calculator));
        }

        [TestMethod]
        public void AddTest()
        {
            // Arrange
            int lVal = 1;
            int rVal = 1;

            var testObject = new Calculator();

            // Act
            var result = testObject.Add(lVal, rVal);

            // Asert
            Assert.AreEqual(2, result);
        }

        //[TestMethod]
        //public void CalculatePythagoreanWinningPercentageTest_ResultNull()
        //{
        //    // Arrange
        //    var teamSeason = new TeamSeason();
        //    teamSeason.PointsFor = 0;
        //    teamSeason.PointsAgainst = 0;

        //    var testObject = new Calculator();

        //    // Act
        //    var result = testObject.CalculatePythagoreanWinningPercentage(teamSeason);

        //    // Assert
        //    Assert.IsNull(result);
        //}

        //[TestMethod]
        //public void CalculatePythagoreanWinningPercentageTest_ResultNotNull()
        //{
        //    // Arrange
        //    var teamSeason = new TeamSeason();
        //    teamSeason.PointsFor = 3;
        //    teamSeason.PointsAgainst = 3;

        //    var testObject = new Calculator();

        //    // Act
        //    var result = testObject.CalculatePythagoreanWinningPercentage(teamSeason);

        //    // Assert
        //    Assert.AreEqual(0.5, result);
        //}

        [TestMethod]
        public void DivideTest_DenominatorZero_ReturnsNull()
        {
            // Arrange
            int numerator = 1;
            int denominator = 0;

            var testObject = new Calculator();

            // Act
            var result = testObject.Divide(numerator, denominator);

            // Assert
            Assert.IsNull(result);
        }

        [TestMethod]
        public void DivideTest_DenominatorNotZero_ReturnsCorrectValue()
        {
            // Arrange
            int numerator = 1;
            int denominator = 2;

            var testObject = new Calculator();

            // Act
            var result = testObject.Divide(numerator, denominator);

            // Assert
            Assert.AreEqual(0.5, result);
        }

        [TestMethod]
        public void MultiplyTest()
        {
            // Arrange
            double lVal = 2;
            double rVal = 2;

            var testObject = new Calculator();

            // Act
            var result = testObject.Multiply(lVal, rVal);

            // Assert
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void SubtractTest()
        {
            // Arrange
            int lVal = 1;
            int rVal = 1;

            var testObject = new Calculator();

            // Act
            var result = testObject.Subtract(lVal, rVal);

            // Assert
            Assert.AreEqual(0, result);
        }
    }
}