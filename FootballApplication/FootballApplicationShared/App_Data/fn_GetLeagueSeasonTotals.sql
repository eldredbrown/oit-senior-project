﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-12-09
-- Description:	A function to compute and return the NFL's total scoring
-- Revision History:
--	2017-01-19	Eldred Brown	Added parameter to restrict result to one season
-- =============================================
CREATE FUNCTION fn_GetLeagueSeasonTotals(@seasonID int)
RETURNS @tbl TABLE
(
	TotalGames		float,
	TotalPoints		float,
	AveragePoints	float,
	WeekCount		int
)
AS
BEGIN
	INSERT @tbl

	--IF ((SELECT SUM(Games) FROM Teams) = 0)
	---- Prevent division by zero.
	--BEGIN
	--	SELECT
	--		SUM(Games) AS TotalGames,
	--		SUM(PointsFor) AS TotalPoints,
	--		NULL AS AveragePoints,
	--		ROUND(AVG(Games), 0) AS WeekCount
	--	FROM
	--		TeamSeasons
	--  WHERE
	--		SeasonID = @seasonID
	--END
	--ELSE
	--BEGIN
	SELECT
		SUM(Games) AS TotalGames,
		SUM(PointsFor) AS TotalPoints,
		ROUND(SUM(PointsFor) / SUM(Games), 2) AS AveragePoints,
		ROUND(AVG(Games), 0) AS WeekCount
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
	--END

	RETURN
END
GO
