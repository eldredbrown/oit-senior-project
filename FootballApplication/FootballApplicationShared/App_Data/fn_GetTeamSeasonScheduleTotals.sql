﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-21
-- Description:	A function to compute and return the totals of a team's 
--				schedule final data
-- Revision History:
--	2017-01-05	Eldred Brown
--	*	Added parameter to restrict results to a single season
-- =============================================
CREATE FUNCTION fn_GetTeamSeasonScheduleTotals
(	
	-- Add the parameters for the function here
	@teamName varchar(50),
	@seasonID int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		COUNT(Q1.Opponent) AS Games,
		SUM(Q1.GamePointsFor) AS PointsFor,
		SUM(Q1.GamePointsAgainst) AS PointsAgainst,
		SUM(Q2.OpponentWins) AS ScheduleWins,
		SUM(Q2.OpponentLosses) AS ScheduleLosses,
		SUM(Q2.OpponentTies) AS ScheduleTies,
		(2 * SUM(Q2.OpponentWins) + SUM(Q2.OpponentTies)) / (2 * (SUM(Q2.OpponentWins) + SUM(Q2.OpponentLosses) + SUM(Q2.OpponentTies))) AS ScheduleWinningPercentage,
		SUM(Q2.OpponentWeightedGames) AS ScheduleGames,
		SUM(Q2.OpponentWeightedPointsFor) AS SchedulePointsFor,
		SUM(Q2.OpponentWeightedPointsAgainst) AS SchedulePointsAgainst
	FROM
		fn_GetTeamSeasonScheduleProfile(@teamName, @seasonID) AS Q1
		INNER JOIN
		fn_GetTeamSeasonScheduleData(@teamName, @seasonID) AS Q2
		  ON Q2.Opponent = Q1.Opponent
)
GO
