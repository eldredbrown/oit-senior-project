﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-21
-- Description:	A procedure to compute and return the totals of a team's 
--				schedule final data
-- Revision History:
--	2017-01-05	Eldred Brown
--	*	Added parameter to restrict results to a single season
-- =============================================
CREATE PROCEDURE usp_GetTeamSeasonScheduleTotals 
	-- Add the parameters for the stored procedure here
	@teamName varchar(50),
	@seasonID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Validate arguments.
	IF EXISTS (
		-- Verify that @teamName is the name of a valid team.
		SELECT * FROM Teams WHERE Name = @teamName
	)
	AND EXISTS (
		-- Verify that @seasonID is the ID of a valid season.
		SELECT * FROM Seasons WHERE ID = @seasonID
	)
	BEGIN
		-- Insert statements for procedure here
		SELECT * FROM fn_GetTeamSeasonScheduleTotals(@teamName, @seasonID)
	END
END
GO
