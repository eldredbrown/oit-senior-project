﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-12-06
-- Description:	A procedure to compute and return the NFL's total scoring
-- =============================================
CREATE PROCEDURE usp_GetLeagueSeasonTotals
	-- Add the parameters for the stored procedure here
	@seasonID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM fn_GetLeagueSeasonTotals(@seasonID)
END
GO
