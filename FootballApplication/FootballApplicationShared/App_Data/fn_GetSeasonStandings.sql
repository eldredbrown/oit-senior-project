﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2017-01-15
-- Description:	A function to compute and return a conference's standings
-- Revision History:
-- =============================================
CREATE FUNCTION fn_GetSeasonStandings
(
	@seasonID int
)
RETURNS @tbl TABLE
(
	Team				nvarchar(50),
	Conference			nchar(3),
	Division			nvarchar(25),
	Wins				float,
	Losses				float,
	Ties				float,
	WinningPercentage	float,
	PointsFor			float,
	PointsAgainst		float,
	AvgPointsFor		float,
	AvgPointsAgainst	float
)
AS
BEGIN
	INSERT @tbl

	SELECT
		TeamName as Team,
		ConferenceName as Conference,
		DivisionName as Division,
		Wins,
		Losses,
		Ties,
		WinningPercentage,
		PointsFor,
		PointsAgainst,
		PointsFor / Games AS AvgPointsFor,
		PointsAgainst / Games AS AvgPointsAgainst
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
		AND
		Games > 0
	UNION
	SELECT
		TeamName as Team,
		ConferenceName as Conference,
		DivisionName as Division,
		Wins,
		Losses,
		Ties,
		NULL as WinningPercentage,
		PointsFor,
		PointsAgainst,
		NULL as AvgPointsFor,
		NULL  as AvgPointsAgainst
	FROM
		TeamSeasons
	WHERE
		SeasonID = @seasonID
		AND
		Games = 0

RETURN

END
GO
