﻿-- =============================================
-- Author:		Eldred Brown
-- Create date: 2016-11-21
-- Description:	A function to return a table showing how all 
--				of a team's opponents performed against their other opponents
-- Revision History:
--	2017-01-05	Eldred Brown
--	*	Added parameter to restrict results to a single season
-- =============================================
CREATE FUNCTION fn_GetTeamSeasonScheduleData 
(	
	-- Add the parameters for the function here
	@teamName varchar(50),
	@seasonID int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT
		Q.Opponent AS Opponent,
		TeamSeasons.Wins AS OpponentWins,
		TeamSeasons.Losses AS OpponentLosses,
		TeamSeasons.Ties AS OpponentTies,
		(2 * TeamSeasons.Wins + TeamSeasons.Ties) / (2 * (TeamSeasons.Wins + TeamSeasons.Losses + TeamSeasons.Ties)) AS OpponentWinningPercentage,
		(TeamSeasons.Games - 1) AS OpponentWeightedGames,
		(TeamSeasons.PointsFor - Q.GamePointsAgainst) AS OpponentWeightedPointsFor,
		(TeamSeasons.PointsAgainst - Q.GamePointsFor) AS OpponentWeightedPointsAgainst
	FROM
		TeamSeasons
		INNER JOIN
		fn_GetTeamSeasonGames(@teamName, @seasonID) AS Q
			ON TeamSeasons.TeamName = Q.Opponent
	WHERE
		TeamSeasons.SeasonID = @seasonID
)
