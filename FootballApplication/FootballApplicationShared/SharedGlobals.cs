﻿using System;
using System.Windows;

namespace EldredBrown.FootballApplicationShared
{
    public class SharedGlobals
	{
        /// <summary>
        /// Gets or sets the selected season ID
        /// </summary>
        internal static int? SelectedSeason { get; set; }
    }
}
