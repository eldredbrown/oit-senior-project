﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF;
using EldredBrown.FootballApplicationWPF.Models;
using NSubstitute;

namespace FootballApplicationWPFTests
{
    internal static class SubFootballDbEntities
    {
        internal static void SubSeasons(this FootballDbEntities dbContext)
        {
            var seasons = new List<Season>
            {
                new Season { ID = 2016 }
            }.AsQueryable();

            var subSeasons = Substitute.For<DbSet<Season>, IQueryable<Season>>();

            // Setup all IQueryable methods using what you have from "seasons"
            (subSeasons as IQueryable<Season>).ElementType.Returns(seasons.ElementType);
            (subSeasons as IQueryable<Season>).Expression.Returns(seasons.Expression);
            (subSeasons as IQueryable<Season>).Provider.Returns(seasons.Provider);
            (subSeasons as IQueryable<Season>).GetEnumerator().Returns(seasons.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Seasons.Returns(subSeasons);
            dbContext.Seasons.Local.Returns(new ObservableCollection<Season>(seasons));
        }

        internal static void SubLeagues(this FootballDbEntities dbContext)
        {
            var leagues = new List<League>
            {
                new League { Name = "NFL", FirstSeasonID = 1923, LastSeasonID = null }
            }.AsQueryable();

            var subLeagues = Substitute.For<DbSet<League>, IQueryable<League>>();

            // Setup all IQueryable methods using what you have from "leagues"
            (subLeagues as IQueryable<League>).ElementType.Returns(leagues.ElementType);
            (subLeagues as IQueryable<League>).Expression.Returns(leagues.Expression);
            (subLeagues as IQueryable<League>).Provider.Returns(leagues.Provider);
            (subLeagues as IQueryable<League>).GetEnumerator().Returns(leagues.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Leagues.Returns(subLeagues);
            dbContext.Leagues.Local.Returns(new ObservableCollection<League>(leagues));
        }

        internal static void SubConferences(this FootballDbEntities dbContext, bool isEmpty = false)
        {
            var conferencesList = new List<Conference>();
            if (!isEmpty)
            {
                conferencesList.Add(new Conference { Name = "AFC" });
                conferencesList.Add(new Conference { Name = "NFC" });
            }
            var conferences = conferencesList.AsQueryable();

            var subConferences = Substitute.For<DbSet<Conference>, IQueryable<Conference>>();

            // Setup all IQueryable methods using what you have from "conferences"
            (subConferences as IQueryable<Conference>).ElementType.Returns(conferences.ElementType);
            (subConferences as IQueryable<Conference>).Expression.Returns(conferences.Expression);
            (subConferences as IQueryable<Conference>).Provider.Returns(conferences.Provider);
            (subConferences as IQueryable<Conference>).GetEnumerator().Returns(conferences.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Conferences.Returns(subConferences);
            dbContext.Conferences.Local.Returns(new ObservableCollection<Conference>(conferences));
        }

        internal static void SubDivisions(this FootballDbEntities dbContext, bool isEmpty = false)
        {
            var divisionsList = new List<Division>();
            if (!isEmpty)
            {
                divisionsList.Add(new Division { Name = "Div1" });
                divisionsList.Add(new Division { Name = "Div2" });
            }
            var divisions = divisionsList.AsQueryable();

            var subDivisions = Substitute.For<DbSet<Division>, IQueryable<Division>>();

            // Setup all IQueryable methods using what you have from "conferences"
            (subDivisions as IQueryable<Division>).ElementType.Returns(divisions.ElementType);
            (subDivisions as IQueryable<Division>).Expression.Returns(divisions.Expression);
            (subDivisions as IQueryable<Division>).Provider.Returns(divisions.Provider);
            (subDivisions as IQueryable<Division>).GetEnumerator().Returns(divisions.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Divisions.Returns(subDivisions);
            dbContext.Divisions.Local.Returns(new ObservableCollection<Division>(divisions));
        }

        internal static void SubTeams(this FootballDbEntities dbContext)
        {
            var teams = new List<Team>
            {
                new Team { Name = "Team1" },
                new Team { Name = "Team2" },
                new Team { Name = "Team3" }
            }.AsQueryable();

            var subTeams = Substitute.For<DbSet<Team>, IQueryable<Team>>();

            // Setup all IQueryable methods using what you have from "teams"
            (subTeams as IQueryable<Team>).ElementType.Returns(teams.ElementType);
            (subTeams as IQueryable<Team>).Expression.Returns(teams.Expression);
            (subTeams as IQueryable<Team>).Provider.Returns(teams.Provider);
            (subTeams as IQueryable<Team>).GetEnumerator().Returns(teams.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Teams.Returns(subTeams);
            dbContext.Teams.Local.Returns(new ObservableCollection<Team>(teams));
        }

        internal static void SubGames(this FootballDbEntities dbContext, int[] weeks = null)
        {
            if (weeks == null)
            {
                weeks = new int[] { 1, 1, 1 };
            }

            var games = new List<Game>
            {
                new Game { Week = weeks[0], GuestName = "Guest1", HostName = "Host1" },
                new Game { Week = weeks[1], GuestName = "Guest2", HostName = "Host2" },
                new Game { Week = weeks[1], GuestName = "Guest3", HostName = "Host3" }
            }.AsQueryable();

            var subGames = Substitute.For<DbSet<Game>, IQueryable<Game>>();

            // Setup all IQueryable methods using what you have from "games"
            (subGames as IQueryable<Game>).ElementType.Returns(games.ElementType);
            (subGames as IQueryable<Game>).Expression.Returns(games.Expression);
            (subGames as IQueryable<Game>).Provider.Returns(games.Provider);
            (subGames as IQueryable<Game>).GetEnumerator().Returns(games.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.Games.Returns(subGames);
            dbContext.Games.Local.Returns(new ObservableCollection<Game>(games));
        }

        internal static void SubLeagueSeasons(this FootballDbEntities dbContext)
        {
            var leagueSeasons = new List<LeagueSeason>
            {
                new LeagueSeason { LeagueName = "NFL", SeasonID = 2016, TotalGames = 256, TotalPoints = 5120, AveragePoints = 20 }
            }.AsQueryable();

            var subLeagueSeasons = Substitute.For<DbSet<LeagueSeason>, IQueryable<LeagueSeason>>();

            // Setup all IQueryable methods using what you have from "teams"
            (subLeagueSeasons as IQueryable<LeagueSeason>).ElementType.Returns(leagueSeasons.ElementType);
            (subLeagueSeasons as IQueryable<LeagueSeason>).Expression.Returns(leagueSeasons.Expression);
            (subLeagueSeasons as IQueryable<LeagueSeason>).Provider.Returns(leagueSeasons.Provider);
            (subLeagueSeasons as IQueryable<LeagueSeason>).GetEnumerator().Returns(leagueSeasons.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.LeagueSeasons.Returns(subLeagueSeasons);
            dbContext.LeagueSeasons.Local.Returns(new ObservableCollection<LeagueSeason>(leagueSeasons));
        }

        internal static void SubTeamSeasons(this FootballDbEntities dbContext)
        {
            var teams = new List<TeamSeason>
            {
                new TeamSeason
                {
                    Team = new Team { Name = "Team1" },
                    SeasonID = 2016,
                    PointsFor = 1,
                    PointsAgainst = 1,
                    OffensiveAverage = 25,
                    DefensiveAverage = 15,
                    OffensiveFactor = 1.25,
                    DefensiveFactor = 0.75
                },
                new TeamSeason
                {
                    Team = new Team { Name = "Team2" },
                    SeasonID = 2016,
                    PointsFor = 1,
                    PointsAgainst = 1,
                    OffensiveAverage = 15,
                    DefensiveAverage = 25,
                    OffensiveFactor = 0.75,
                    DefensiveFactor = 1.25
                },
                new TeamSeason { Team = new Team { Name = "Team1" }, SeasonID = 2015, PointsFor = 1, PointsAgainst = 1 },
                new TeamSeason { Team = new Team { Name = "Team2" }, SeasonID = 2015, PointsFor = 1, PointsAgainst = 1 },
                new TeamSeason { Team = new Team { Name = "Team1" }, SeasonID = 2014, PointsFor = 1, PointsAgainst = 1 },
                new TeamSeason { Team = new Team { Name = "Team2" }, SeasonID = 2014, PointsFor = 1, PointsAgainst = 1 }
            }.AsQueryable();

            var subTeams = Substitute.For<DbSet<TeamSeason>, IQueryable<TeamSeason>>();

            // Setup all IQueryable methods using what you have from "teams"
            (subTeams as IQueryable<TeamSeason>).ElementType.Returns(teams.ElementType);
            (subTeams as IQueryable<TeamSeason>).Expression.Returns(teams.Expression);
            (subTeams as IQueryable<TeamSeason>).Provider.Returns(teams.Provider);
            (subTeams as IQueryable<TeamSeason>).GetEnumerator().Returns(teams.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.TeamSeasons.Returns(subTeams);
            dbContext.TeamSeasons.Local.Returns(new ObservableCollection<TeamSeason>(teams));
        }

        internal static void SubWeekCounts(this FootballDbEntities dbContext)
        {
            var weekCounts = new List<WeekCount>
            {
                new WeekCount { ID = 1, Count = 1 }
            }.AsQueryable();

            var subWeekCounts = Substitute.For<DbSet<WeekCount>, IQueryable<WeekCount>>();

            // Setup all IQueryable methods using what you have from "weekCounts"
            (subWeekCounts as IQueryable<WeekCount>).ElementType.Returns(weekCounts.ElementType);
            (subWeekCounts as IQueryable<WeekCount>).Expression.Returns(weekCounts.Expression);
            (subWeekCounts as IQueryable<WeekCount>).Provider.Returns(weekCounts.Provider);
            (subWeekCounts as IQueryable<WeekCount>).GetEnumerator().Returns(weekCounts.GetEnumerator());

            // Do the wiring between DbContext and DbSet
            dbContext.WeekCounts.Returns(subWeekCounts);
        }

        internal static void SubGetLeagueSeasonTotals(this FootballDbEntities dbContext)
        {
            var leagueSeasonTotals = new List<usp_GetLeagueSeasonTotals_Result>
            {
                new usp_GetLeagueSeasonTotals_Result
                {
                    TotalGames = 256,
                    TotalPoints = 5120,
                    AveragePoints = 0
                }
            }.AsQueryable();

            var subLeagueSeasonTotals = Substitute.For<ObjectResult<usp_GetLeagueSeasonTotals_Result>, IQueryable<usp_GetLeagueSeasonTotals_Result>>();

            // Setup all IQueryable methods using what you have from "leagueTotals"
            (subLeagueSeasonTotals as IQueryable<usp_GetLeagueSeasonTotals_Result>).ElementType.Returns(leagueSeasonTotals.ElementType);
            (subLeagueSeasonTotals as IQueryable<usp_GetLeagueSeasonTotals_Result>).Expression.Returns(leagueSeasonTotals.Expression);
            (subLeagueSeasonTotals as IQueryable<usp_GetLeagueSeasonTotals_Result>).Provider.Returns(leagueSeasonTotals.Provider);
            (subLeagueSeasonTotals as IQueryable<usp_GetLeagueSeasonTotals_Result>).GetEnumerator().Returns(leagueSeasonTotals.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetLeagueSeasonTotals(SharedGlobals.SelectedSeason).Returns(subLeagueSeasonTotals);
        }

        internal static void SubGetTeamSeasonScheduleProfile(this FootballDbEntities dbContext)
        {
            var teamScheduleProfile = new List<usp_GetTeamSeasonScheduleProfile_Result>
            {
                new usp_GetTeamSeasonScheduleProfile_Result()
            }.AsQueryable();

            var subTeamScheduleProfile = Substitute.For<ObjectResult<usp_GetTeamSeasonScheduleProfile_Result>, IQueryable<usp_GetTeamSeasonScheduleProfile_Result>>();

            // Setup all IQueryable methods using what you have from "teamScheduleProfile"
            (subTeamScheduleProfile as IQueryable<usp_GetTeamSeasonScheduleProfile_Result>).Provider.Returns(teamScheduleProfile.Provider);
            (subTeamScheduleProfile as IQueryable<usp_GetTeamSeasonScheduleProfile_Result>).Expression.Returns(teamScheduleProfile.Expression);
            (subTeamScheduleProfile as IQueryable<usp_GetTeamSeasonScheduleProfile_Result>).ElementType.Returns(teamScheduleProfile.ElementType);
            (subTeamScheduleProfile as IQueryable<usp_GetTeamSeasonScheduleProfile_Result>).GetEnumerator().Returns(teamScheduleProfile.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetTeamSeasonScheduleProfile(Arg.Any<string>(), Arg.Any<int>()).Returns(subTeamScheduleProfile);
        }

        internal static void SubGetTeamSeasonScheduleTotals(this FootballDbEntities dbContext, bool isEmpty = false, int? scheduleGames = null)
        {
            var teamScheduleTotalsList = new List<usp_GetTeamSeasonScheduleTotals_Result>();
            if (!isEmpty)
            {
                teamScheduleTotalsList.Add(new usp_GetTeamSeasonScheduleTotals_Result { ScheduleGames = scheduleGames });
            }
            var teamScheduleTotals = teamScheduleTotalsList.AsQueryable();

            var subTeamScheduleTotals = Substitute.For<ObjectResult<usp_GetTeamSeasonScheduleTotals_Result>, IQueryable<usp_GetTeamSeasonScheduleTotals_Result>>();

            // Setup all IQueryable methods using what you have from "teamScheduleTotals"
            (subTeamScheduleTotals as IQueryable<usp_GetTeamSeasonScheduleTotals_Result>).ElementType.Returns(teamScheduleTotals.ElementType);
            (subTeamScheduleTotals as IQueryable<usp_GetTeamSeasonScheduleTotals_Result>).Expression.Returns(teamScheduleTotals.Expression);
            (subTeamScheduleTotals as IQueryable<usp_GetTeamSeasonScheduleTotals_Result>).Provider.Returns(teamScheduleTotals.Provider);
            (subTeamScheduleTotals as IQueryable<usp_GetTeamSeasonScheduleTotals_Result>).GetEnumerator().Returns(teamScheduleTotals.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetTeamSeasonScheduleTotals(Arg.Any<string>(), Arg.Any<int>()).Returns(subTeamScheduleTotals);
        }

        internal static void SubGetTeamSeasonScheduleAverages(this FootballDbEntities dbContext, bool isEmpty = false)
        {
            var teamScheduleAveragesList = new List<usp_GetTeamSeasonScheduleAverages_Result>();
            if (!isEmpty)
            {
                var teamScheduleAveragesResult = new usp_GetTeamSeasonScheduleAverages_Result
                {
                    PointsFor = 15,
                    PointsAgainst = 20
                };
                teamScheduleAveragesList.Add(teamScheduleAveragesResult);
            }
            var teamScheduleAverages = teamScheduleAveragesList.AsQueryable();

            var subTeamScheduleAverages = Substitute.For<ObjectResult<usp_GetTeamSeasonScheduleAverages_Result>, IQueryable<usp_GetTeamSeasonScheduleAverages_Result>>();

            // Setup all IQueryable methods using what you have from "teamScheduleAverages"
            (subTeamScheduleAverages as IQueryable<usp_GetTeamSeasonScheduleAverages_Result>).ElementType.Returns(teamScheduleAverages.ElementType);
            (subTeamScheduleAverages as IQueryable<usp_GetTeamSeasonScheduleAverages_Result>).Expression.Returns(teamScheduleAverages.Expression);
            (subTeamScheduleAverages as IQueryable<usp_GetTeamSeasonScheduleAverages_Result>).Provider.Returns(teamScheduleAverages.Provider);
            (subTeamScheduleAverages as IQueryable<usp_GetTeamSeasonScheduleAverages_Result>).GetEnumerator().Returns(teamScheduleAverages.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetTeamSeasonScheduleAverages(Arg.Any<string>(), Arg.Any<int>()).Returns(subTeamScheduleAverages);
        }

        internal static void SubGetConferenceSeasonStandings(this FootballDbEntities dbContext)
        {
            var conferenceSeasonStandings = new List<usp_GetSeasonAssociationStandings_Result>
            {
                new usp_GetConferenceSeasonStandings_Result()
            }.AsQueryable();

            var subConferenceSeasonStandings = Substitute.For<ObjectResult<usp_GetConferenceSeasonStandings_Result>, IQueryable<usp_GetSeasonAssociationStandings_Result>>();

            // Setup all IQueryable methods using what you have from "teamScheduleAverages"
            (subConferenceSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).ElementType.Returns(conferenceSeasonStandings.ElementType);
            (subConferenceSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).Expression.Returns(conferenceSeasonStandings.Expression);
            (subConferenceSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).Provider.Returns(conferenceSeasonStandings.Provider);
            (subConferenceSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).GetEnumerator().Returns(conferenceSeasonStandings.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetConferenceSeasonStandings(Arg.Any<string>(), Arg.Any<int>()).Returns(subConferenceSeasonStandings);
        }

        internal static void SubGetDivisionSeasonStandings(this FootballDbEntities dbContext)
        {
            var divisionSeasonStandings = new List<usp_GetSeasonAssociationStandings_Result>
            {
                new usp_GetDivisionSeasonStandings_Result()
            }.AsQueryable();

            var subDivisionSeasonStandings = Substitute.For<ObjectResult<usp_GetDivisionSeasonStandings_Result>, IQueryable<usp_GetSeasonAssociationStandings_Result>>();

            // Setup all IQueryable methods using what you have from "teamScheduleAverages"
            (subDivisionSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).ElementType.Returns(divisionSeasonStandings.ElementType);
            (subDivisionSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).Expression.Returns(divisionSeasonStandings.Expression);
            (subDivisionSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).Provider.Returns(divisionSeasonStandings.Provider);
            (subDivisionSeasonStandings as IQueryable<usp_GetSeasonAssociationStandings_Result>).GetEnumerator().Returns(divisionSeasonStandings.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetDivisionSeasonStandings(Arg.Any<string>(), Arg.Any<int>()).Returns(subDivisionSeasonStandings);
        }

        internal static void SubGetTotalRankings(this FootballDbEntities dbContext, int seasonID)
        {
            var totalRankings = new List<usp_GetTotalRankings_Result>
            {
                new usp_GetTotalRankings_Result()
            }.AsQueryable();

            var subTotalRankings = Substitute.For<ObjectResult<usp_GetTotalRankings_Result>, IQueryable<usp_GetTotalRankings_Result>>();

            // Setup all IQueryable methods using what you have from "totalRankings"
            (subTotalRankings as IQueryable<usp_GetTotalRankings_Result>).ElementType.Returns(totalRankings.ElementType);
            (subTotalRankings as IQueryable<usp_GetTotalRankings_Result>).Expression.Returns(totalRankings.Expression);
            (subTotalRankings as IQueryable<usp_GetTotalRankings_Result>).Provider.Returns(totalRankings.Provider);
            (subTotalRankings as IQueryable<usp_GetTotalRankings_Result>).GetEnumerator().Returns(totalRankings.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetTotalRankings(seasonID).Returns(subTotalRankings);
        }

        internal static void SubGetOffensiveRankings(this FootballDbEntities dbContext, int seasonID)
        {
            var offensiveRankings = new List<usp_GetOffensiveRankings_Result>
            {
                new usp_GetOffensiveRankings_Result()
            }.AsQueryable();

            var subOffensiveRankings = Substitute.For<ObjectResult<usp_GetOffensiveRankings_Result>, IQueryable<usp_GetOffensiveRankings_Result>>();

            // Setup all IQueryable methods using what you have from "totalRankings"
            (subOffensiveRankings as IQueryable<usp_GetOffensiveRankings_Result>).ElementType.Returns(offensiveRankings.ElementType);
            (subOffensiveRankings as IQueryable<usp_GetOffensiveRankings_Result>).Expression.Returns(offensiveRankings.Expression);
            (subOffensiveRankings as IQueryable<usp_GetOffensiveRankings_Result>).Provider.Returns(offensiveRankings.Provider);
            (subOffensiveRankings as IQueryable<usp_GetOffensiveRankings_Result>).GetEnumerator().Returns(offensiveRankings.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetOffensiveRankings(seasonID).Returns(subOffensiveRankings);
        }

        internal static void SubGetDefensiveRankings(this FootballDbEntities dbContext, int seasonID)
        {
            var defensiveRankings = new List<usp_GetDefensiveRankings_Result>
            {
                new usp_GetDefensiveRankings_Result()
            }.AsQueryable();

            var subDefensiveRankings = Substitute.For<ObjectResult<usp_GetDefensiveRankings_Result>, IQueryable<usp_GetDefensiveRankings_Result>>();

            // Setup all IQueryable methods using what you have from "totalRankings"
            (subDefensiveRankings as IQueryable<usp_GetDefensiveRankings_Result>).ElementType.Returns(defensiveRankings.ElementType);
            (subDefensiveRankings as IQueryable<usp_GetDefensiveRankings_Result>).Expression.Returns(defensiveRankings.Expression);
            (subDefensiveRankings as IQueryable<usp_GetDefensiveRankings_Result>).Provider.Returns(defensiveRankings.Provider);
            (subDefensiveRankings as IQueryable<usp_GetDefensiveRankings_Result>).GetEnumerator().Returns(defensiveRankings.GetEnumerator());

            // Do the wiring between DbContext and ObjectResult
            dbContext.usp_GetDefensiveRankings(seasonID).Returns(subDefensiveRankings);
        }
    }
}
