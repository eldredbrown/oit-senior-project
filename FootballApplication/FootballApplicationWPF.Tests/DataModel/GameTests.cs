﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.Models.Tests
{
    [TestClass]
    public class GameTests
    {
        [TestMethod]
        public void GameTest()
        {
            // Arrange
            // Act
            var testObject = new Game();

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(Game));
        }

        [TestMethod]
        public void DecideWinnerAndLoserTest_GuestScoreGreaterThanHostScore_GuestWins()
        {
            // Arrange
            var testObject = new Game();
            testObject.Guest = new Team();
            testObject.GuestScore = 2;
            testObject.Host = new Team();
            testObject.HostScore = 1;

            // Act
            testObject.DecideWinnerAndLoser();

            // Assert
            Assert.AreEqual(testObject.Guest, testObject.Winner);
            Assert.AreEqual(testObject.GuestScore, testObject.WinnerScore);
            Assert.AreEqual(testObject.Host, testObject.Loser);
            Assert.AreEqual(testObject.HostScore, testObject.LoserScore);
        }

        [TestMethod]
        public void DecideWinnerAndLoserTest__HostScoreGreaterThanGuestScore_HostWins()
        {
            // Arrange
            var testObject = new Game();
            testObject.Guest = new Team();
            testObject.GuestScore = 1;
            testObject.Host = new Team();
            testObject.HostScore = 2;

            // Act
            testObject.DecideWinnerAndLoser();

            // Assert
            Assert.AreEqual(testObject.Host, testObject.Winner);
            Assert.AreEqual(testObject.HostScore, testObject.WinnerScore);
            Assert.AreEqual(testObject.Guest, testObject.Loser);
            Assert.AreEqual(testObject.GuestScore, testObject.LoserScore);
        }

        [TestMethod]
        public void DecideWinnerAndLoserTest_BothTeamScoresEqual_TieGame()
        {
            // Arrange
            var testObject = new Game();
            testObject.Guest = new Team();
            testObject.GuestScore = 1;
            testObject.Host = new Team();
            testObject.HostScore = 1;

            // Act
            testObject.DecideWinnerAndLoser();

            // Assert
            Assert.IsNull(testObject.Winner);
            Assert.AreEqual(0, testObject.WinnerScore);
            Assert.IsNull(testObject.Loser);
            Assert.AreEqual(0, testObject.LoserScore);
        }

        [TestMethod]
        public void DecideWinnerAndLoserTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<Game>(subGlobals);

            var subTeam = new Team();
            var ex = new Exception();
            testObject.When(x => { x.Winner = null; }).Do(x => { throw ex; });

            // Act
            testObject.DecideWinnerAndLoser();

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }
    }
}