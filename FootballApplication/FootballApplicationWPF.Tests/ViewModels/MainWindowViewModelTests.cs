﻿using System;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;
using EldredBrown.FootballApplicationWPF.Windows;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class MainWindowViewModelTests
    {
        [TestMethod]
        public void MainWindowViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new MainWindowViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void MainWindowViewModelTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGamePredictor = Substitute.For<IGamePredictorWindow>();

            IMainWindowViewModel testObject = null;
            try
            {
                // Act
                testObject = new MainWindowViewModel(subDbContext, gamePredictor: subGamePredictor);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IMainWindowViewModel));
        }

        [TestMethod]
        public void ViewSeasonsCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubSeasons();

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, gamePredictor: subGamePredictor);

            // Act
            testObject.ViewSeasonsCommand.Execute(null);

            // Assert
            var season = 2016;
            Assert.AreEqual(1, testObject.Seasons.Count);
            foreach (var seasonID in testObject.Seasons)
            {
                Assert.AreEqual(season, seasonID);
            }
            Assert.AreEqual(season, SharedGlobals.SelectedSeason);
        }

        [TestMethod]
        public void ViewSeasonsCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubSeasons();

            var ex = new Exception();
            subDbContext.Seasons.Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, globals: subGlobals, gamePredictor: subGamePredictor);

            // Act
            testObject.ViewSeasonsCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void PredictGameScoreCommandTest_NoExceptionCaught_OpensGamePredictorWindow()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subGamePredictorWindow = Substitute.For<IGamePredictorWindow>();
            subGamePredictorWindow.When(x => { x.Show(); }).Do(x => { });

            var testObject = new MainWindowViewModel(subDbContext, gamePredictor: subGamePredictorWindow);

            // Act
            testObject.PredictGameScoreCommand.Execute(null);

            // Assert
            subGamePredictorWindow.Received(1).Show();
        }

        [TestMethod]
        public void PredictGameScoreCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subGamePredictorWindow = Substitute.For<IGamePredictorWindow>();

            var ex = new Exception();
            subGamePredictorWindow.When(x => { x.Show(); }).Do(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new MainWindowViewModel(subDbContext, gamePredictor: subGamePredictorWindow, globals: subGlobals);

            // Act
            testObject.PredictGameScoreCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultNo_DoesNothing()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subCalculator = Substitute.For<ICalculator>();
            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.No);

            // Assert
            subCalculator.DidNotReceive().Divide(Arg.Any<double>(), Arg.Any<double>());
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultYesAndWeekCountLessThanThree_RunsWeeklyUpdate()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGames(weeks: new int[] { 1, 1, 1 });
            subDbContext.SubWeekCounts();
            subDbContext.SubGetLeagueSeasonTotals();

            var subHelper = Substitute.For<IMainWindowViewModelHelper>();
            subHelper.When(x => { x.UpdateRankings(); }).Do(x => { });

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(5120, 256).Returns(20);

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, helper: subHelper, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.Yes);

            // Assert
            subCalculator.Received(1).Divide(5120, 256);

            foreach (var leagueSeason in subDbContext.LeagueSeasons)
            {
                Assert.AreEqual(256, leagueSeason.TotalGames);
                Assert.AreEqual(5120, leagueSeason.TotalPoints);
                Assert.AreEqual(20, leagueSeason.AveragePoints);
            }

            var srcWeekCount = (from game in subDbContext.Games select game.Week).Max();
            var destWeekCount = (from w in subDbContext.WeekCounts select w).FirstOrDefault();
            Assert.AreEqual(srcWeekCount, destWeekCount.Count);

            subDbContext.Received(1).SaveChanges();

            subHelper.DidNotReceive().UpdateRankings();
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultYesAndWeekCountEqualsOne_RunsWeeklyUpdate()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGames(weeks: new int[] { 1, 1, 1 });
            subDbContext.SubWeekCounts();
            subDbContext.SubGetLeagueSeasonTotals();

            var subHelper = Substitute.For<IMainWindowViewModelHelper>();
            subHelper.When(x => { x.UpdateRankings(); }).Do(x => { });

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(5120, 256).Returns(20);

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, helper: subHelper, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.Yes);

            // Assert
            subCalculator.Received(1).Divide(5120, 256);

            foreach (var leagueSeason in subDbContext.LeagueSeasons)
            {
                Assert.AreEqual(256, leagueSeason.TotalGames);
                Assert.AreEqual(5120, leagueSeason.TotalPoints);
                Assert.AreEqual(20, leagueSeason.AveragePoints);
            }

            var srcWeekCount = (from game in subDbContext.Games select game.Week).Max();
            var destWeekCount = (from w in subDbContext.WeekCounts select w).FirstOrDefault();
            Assert.AreEqual(srcWeekCount, destWeekCount.Count);

            subDbContext.Received(1).SaveChanges();

            subHelper.DidNotReceive().UpdateRankings();
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultYesAndWeekCountEqualsTwo_RunsWeeklyUpdate()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGames(weeks: new int[] { 2, 2, 2 });
            subDbContext.SubWeekCounts();
            subDbContext.SubGetLeagueSeasonTotals();

            var subHelper = Substitute.For<IMainWindowViewModelHelper>();
            subHelper.When(x => { x.UpdateRankings(); }).Do(x => { });

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(5120, 256).Returns(20);

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, helper: subHelper, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.Yes);

            // Assert
            subCalculator.Received(1).Divide(5120, 256);

            foreach (var leagueSeason in subDbContext.LeagueSeasons)
            {
                Assert.AreEqual(256, leagueSeason.TotalGames);
                Assert.AreEqual(5120, leagueSeason.TotalPoints);
                Assert.AreEqual(20, leagueSeason.AveragePoints);
            }

            var srcWeekCount = (from game in subDbContext.Games select game.Week).Max();
            var destWeekCount = (from w in subDbContext.WeekCounts select w).FirstOrDefault();
            Assert.AreEqual(srcWeekCount, destWeekCount.Count);

            subDbContext.Received(1).SaveChanges();

            subHelper.DidNotReceive().UpdateRankings();
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultYesAndWeekCountEqualsThree_RunsWeeklyUpdate()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGames(weeks: new int[] { 3, 3, 3 });
            subDbContext.SubWeekCounts();
            subDbContext.SubGetLeagueSeasonTotals();

            var subHelper = Substitute.For<IMainWindowViewModelHelper>();
            subHelper.When(x => { x.UpdateRankings(); }).Do(x => { });

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(5120, 256).Returns(20);

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, helper: subHelper, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.Yes);

            // Assert
            subCalculator.Received(1).Divide(5120, 256);

            foreach (var leagueSeason in subDbContext.LeagueSeasons)
            {
                Assert.AreEqual(256, leagueSeason.TotalGames);
                Assert.AreEqual(5120, leagueSeason.TotalPoints);
                Assert.AreEqual(20, leagueSeason.AveragePoints);
            }

            var srcWeekCount = (from game in subDbContext.Games select game.Week).Max();
            var destWeekCount = (from w in subDbContext.WeekCounts select w).FirstOrDefault();
            Assert.AreEqual(srcWeekCount, destWeekCount.Count);

            subDbContext.Received(1).SaveChanges();

            subHelper.Received(1).UpdateRankings();
        }

        [TestMethod]
        public void WeeklyUpdateCommandTest_DialogResultYesAndWeekCountGreaterThanThree_RunsWeeklyUpdate()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGames(weeks: new int[] { 4, 4, 4 });
            subDbContext.SubWeekCounts();
            subDbContext.SubGetLeagueSeasonTotals();

            var subHelper = Substitute.For<IMainWindowViewModelHelper>();
            subHelper.When(x => { x.UpdateRankings(); }).Do(x => { });

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(5120, 256).Returns(20);

            var subGamePredictor = Substitute.For<IGamePredictorWindow>();
            var testObject = new MainWindowViewModel(subDbContext, helper: subHelper, calculator: subCalculator, gamePredictor: subGamePredictor);

            // Act
            testObject.WeeklyUpdateCommand.Execute(MessageBoxResult.Yes);

            // Assert
            subCalculator.Received(1).Divide(5120, 256);

            foreach (var leagueSeason in subDbContext.LeagueSeasons)
            {
                Assert.AreEqual(256, leagueSeason.TotalGames);
                Assert.AreEqual(5120, leagueSeason.TotalPoints);
                Assert.AreEqual(20, leagueSeason.AveragePoints);
            }

            var srcWeekCount = (from game in subDbContext.Games select game.Week).Max();
            var destWeekCount = (from w in subDbContext.WeekCounts select w).FirstOrDefault();
            Assert.AreEqual(srcWeekCount, destWeekCount.Count);

            subDbContext.Received(1).SaveChanges();

            subHelper.Received(1).UpdateRankings();
        }
    }
}