﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;
using EldredBrown.FootballApplicationWPF.Windows;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class GamesWindowViewModelTests
    {
        [TestMethod]
        public void GamesWindowViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new GamesWindowViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void GamesWindowViewModelTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGameFinder = Substitute.For<IGameFinderWindow>();

            IGamesWindowViewModel testObject = null;
            try
            {
                // Act
                testObject = new GamesWindowViewModel(subDbContext, gameFinder: subGameFinder);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(IGamesWindowViewModel));
        }

        [TestMethod]
        public void AddGameCommandTest_DataValidationThrowsException_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var ex = new DataValidationException("message");
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { throw ex; });

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.AddGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subGlobals.Received(1).ShowExceptionMessage(ex);
            subHelper.DidNotReceive().StoreNewGameValues();
        }

        [TestMethod]
        public void AddGameCommandTest_DataValidationDoesNotThrowException_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();

            var subNewGame = Substitute.For<IGame>();
            subDbContext.Games.When(x => { x.Add(subNewGame as Game); }).Do(x => { });

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.EditTeams(subNewGame, Direction.Up); }).Do(x => { });
            subHelper.When(x => { x.SaveChanges(); }).Do(x => { });
            subHelper.When(x => { x.SortGamesByDefaultOrder(); }).Do(x => { });
            subHelper.StoreNewGameValues().Returns(subNewGame);
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);

            // Act
            testObject.AddGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subHelper.Received(1).StoreNewGameValues();
            subHelper.Received(1).EditTeams(subNewGame, Direction.Up);
            subHelper.Received(1).SaveChanges();
            subHelper.Received(1).SortGamesByDefaultOrder();
            Assert.IsNull(testObject.SelectedGame);
        }

        [TestMethod]
        public void AddGameCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();

            var subGlobals = Substitute.For<IGlobals>();

            var subGame = Substitute.For<IGame>();
            var ex = new Exception();
            subDbContext.Games.When(x => { x.Add(subGame as Game); }).Do(x => { throw ex; });

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.EditTeams(subGame, Direction.Up); }).Do(x => { });
            subHelper.When(x => { x.SaveChanges(); }).Do(x => { });
            subHelper.When(x => { x.SortGamesByDefaultOrder(); }).Do(x => { });
            subHelper.StoreNewGameValues().Returns(subGame);
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.AddGameCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void EditGameCommandTest_DataValidationThrowsException_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            var ex = new DataValidationException("message");
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { throw ex; });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.EditGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subGlobals.Received(1).ShowExceptionMessage(ex);
            subHelper.DidNotReceive().StoreNewGameValues();
        }

        [TestMethod]
        public void EditGameCommandTest_DataValidationDoesNotThrowExceptionAndFindGameFilterApplied_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.ApplyFindGameFilter(); }).Do(x => { });

            var subNewGame = Substitute.For<IGame>();
            subNewGame.SeasonID = (int)SharedGlobals.SelectedSeason;
            subNewGame.Week = 1;
            subNewGame.Guest = new Team { Name = "Guest" };
            subNewGame.GuestScore = 7;
            subNewGame.Host = new Team { Name = "Host" };
            subNewGame.HostScore = 7;
            subNewGame.IsPlayoffGame = false;
            subNewGame.Notes = "Notes";
            subHelper.When(x => { x.EditTeams(subNewGame, Direction.Up); }).Do(x => { });

            var subOldGame = Substitute.For<IGame>();
            subHelper.When(x => { x.EditTeams(subOldGame, Direction.Down); }).Do(x => { });

            subHelper.When(x => { x.SaveChanges(); }).Do(x => { });
            subHelper.StoreNewGameValues().Returns(subNewGame);
            subHelper.StoreOldGameValues().Returns(subOldGame);
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);
            testObject.IsFindGameFilterApplied = true;
            testObject.SelectedGame = Substitute.For<IGame>();

            // Act
            testObject.EditGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subHelper.Received(1).StoreOldGameValues();
            subHelper.Received(1).EditTeams(subOldGame, Direction.Down);
            subHelper.Received(1).StoreNewGameValues();
            Assert.AreEqual(subNewGame.Week, testObject.SelectedGame.Week);
            Assert.AreEqual(subNewGame.Guest, testObject.SelectedGame.Guest);
            Assert.AreEqual(subNewGame.GuestScore, testObject.SelectedGame.GuestScore);
            Assert.AreEqual(subNewGame.Host, testObject.SelectedGame.Host);
            Assert.AreEqual(subNewGame.HostScore, testObject.SelectedGame.HostScore);
            Assert.AreEqual(subNewGame.IsPlayoffGame, testObject.SelectedGame.IsPlayoffGame);
            Assert.AreEqual(subNewGame.Notes, testObject.SelectedGame.Notes);
            subHelper.Received(1).EditTeams(subNewGame, Direction.Up);
            subHelper.Received(1).SaveChanges();
            subHelper.Received(1).ApplyFindGameFilter();
        }

        [TestMethod]
        public void EditGameCommandTest_DataValidationDoesNotThrowExceptionAndFindGameFilterNotApplied_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var subNewGame = Substitute.For<IGame>();
            subNewGame.SeasonID = (int)SharedGlobals.SelectedSeason;
            subNewGame.Week = 1;
            subNewGame.Guest = new Team { Name = "Guest" };
            subNewGame.GuestScore = 7;
            subNewGame.Host = new Team { Name = "Host" };
            subNewGame.HostScore = 7;
            subNewGame.IsPlayoffGame = false;
            subNewGame.Notes = "Notes";
            subHelper.When(x => { x.EditTeams(subNewGame, Direction.Up); }).Do(x => { });

            var subOldGame = Substitute.For<IGame>();
            subHelper.When(x => { x.EditTeams(subOldGame, Direction.Down); }).Do(x => { });

            subHelper.When(x => { x.SaveChanges(); }).Do(x => { });
            subHelper.When(x => { x.SortGamesByDefaultOrder(); }).Do(x => { });
            subHelper.StoreNewGameValues().Returns(subNewGame);
            subHelper.StoreOldGameValues().Returns(subOldGame);
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);

            testObject.IsFindGameFilterApplied = false;
            testObject.SelectedGame = Substitute.For<IGame>();

            // Act
            testObject.EditGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subHelper.Received(1).StoreOldGameValues();
            subHelper.Received(1).EditTeams(subOldGame, Direction.Down);
            subHelper.Received(1).StoreNewGameValues();
            Assert.AreEqual(subNewGame.Week, testObject.SelectedGame.Week);
            Assert.AreEqual(subNewGame.Guest, testObject.SelectedGame.Guest);
            Assert.AreEqual(subNewGame.GuestScore, testObject.SelectedGame.GuestScore);
            Assert.AreEqual(subNewGame.Host, testObject.SelectedGame.Host);
            Assert.AreEqual(subNewGame.HostScore, testObject.SelectedGame.HostScore);
            Assert.AreEqual(subNewGame.IsPlayoffGame, testObject.SelectedGame.IsPlayoffGame);
            Assert.AreEqual(subNewGame.Notes, testObject.SelectedGame.Notes);
            subHelper.Received(1).EditTeams(subNewGame, Direction.Up);
            subHelper.Received(1).SaveChanges();
            subHelper.Received(1).SortGamesByDefaultOrder();
        }

        [TestMethod]
        public void EditGameCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var subOldGame = Substitute.For<IGame>();
            var ex = new Exception();
            subHelper.When(x => { x.EditTeams(subOldGame, Direction.Down); }).Do(x => { throw ex; });

            subHelper.StoreOldGameValues().Returns(subOldGame);
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.EditGameCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void FindGameCommandTest_GameFinderDialogReturnsFalse_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            subGameFinder.ShowDialog().Returns(false);

            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);

            // Act
            testObject.FindGameCommand.Execute(null);

            // Assert
            Assert.AreSame(subGameFinder, testObject.GameFinder);
            testObject.GameFinder.Received(1).ShowDialog();
            subHelper.DidNotReceive().ApplyFindGameFilter();
        }

        [TestMethod]
        public void FindGameCommandTest_GameFinderDialogReturnsTrueAndGamesCountIsNotZero_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.ApplyFindGameFilter(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            subGameFinder.ShowDialog().Returns(true);

            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);
            testObject.Games = new ObservableCollection<Game> { new Game() };
            testObject.SelectedGame = new Game();

            // Act
            testObject.FindGameCommand.Execute(null);

            // Assert
            Assert.IsInstanceOfType(testObject.GameFinder, typeof(IGameFinderWindow));
            testObject.GameFinder.Received(1).ShowDialog();
            subHelper.Received(1).ApplyFindGameFilter();
            Assert.IsTrue(testObject.IsGamesReadOnly);
            Assert.IsTrue(testObject.IsShowAllGamesEnabled);
            Assert.IsNotNull(testObject.SelectedGame);
            Assert.AreEqual(Visibility.Hidden, testObject.AddGameControlVisibility);
        }

        [TestMethod]
        public void FindGameCommandTest_GameFinderDialogReturnsTrueAndGamesCountIsZero_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.ApplyFindGameFilter(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            subGameFinder.ShowDialog().Returns(true);

            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);
            testObject.Games = new ObservableCollection<Game> { };
            testObject.SelectedGame = new Game();

            // Act
            testObject.FindGameCommand.Execute(null);

            // Assert
            Assert.IsInstanceOfType(testObject.GameFinder, typeof(IGameFinderWindow));
            testObject.GameFinder.Received(1).ShowDialog();
            subHelper.Received(1).ApplyFindGameFilter();
            Assert.IsTrue(testObject.IsGamesReadOnly);
            Assert.IsTrue(testObject.IsShowAllGamesEnabled);
            Assert.IsNull(testObject.SelectedGame);
            Assert.AreEqual(Visibility.Hidden, testObject.AddGameControlVisibility);
        }

        [TestMethod]
        public void FindGameCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.ApplyFindGameFilter(); }).Do(x => { });

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var ex = new Exception();
            subGameFinder.ShowDialog().Returns(x => { throw ex; });

            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);
            testObject.Games = new ObservableCollection<Game> { };
            testObject.SelectedGame = new Game();

            // Act
            testObject.FindGameCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void RemoveGameCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subDbContext.Games.When(x => { x.Remove(Arg.Any<Game>()); }).Do(x => { });

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.EditTeams(Arg.Any<Game>(), Direction.Down); }).Do(x => { });
            subHelper.When(x => { x.SaveChanges(); }).Do(x => { });
            subHelper.When(x => { x.SortGamesByDefaultOrder(); }).Do(x => { });

            var subOldGame = new Game();
            subHelper.StoreOldGameValues().Returns(subOldGame);

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, helper: subHelper, gameFinder: subGameFinder);
            testObject.SelectedGame = new Game();

            // Act
            testObject.RemoveGameCommand.Execute(null);

            // Assert
            subHelper.Received(1).StoreOldGameValues();
            subDbContext.Games.Received(1).Remove(Arg.Any<Game>());
            subHelper.Received(1).EditTeams(subOldGame, Direction.Down);
            subHelper.Received(1).SaveChanges();
            Assert.IsNull(testObject.SelectedGame);
            subHelper.Received(1).SortGamesByDefaultOrder();
        }

        [TestMethod]
        public void RemoveGameCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subDbContext.Games.When(x => { x.Remove(Arg.Any<Game>()); }).Do(x => { });

            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var ex = new Exception();
            subHelper.StoreOldGameValues().Returns(x => { throw ex; });

            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);
            testObject.SelectedGame = new Game();

            // Act
            testObject.RemoveGameCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ShowAllGamesCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subDbContext.SubWeekCounts();

            var subGlobals = Substitute.For<IGlobals>();
            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            var subGameFinder = Substitute.For<IGameFinderWindow>();

            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);
            testObject.IsFindGameFilterApplied = true;
            testObject.IsGamesReadOnly = true;
            testObject.IsShowAllGamesEnabled = true;
            testObject.SelectedGame = new Game();

            // Act
            testObject.ShowAllGamesCommand.Execute(null);

            // Assert
            Assert.AreNotEqual(0, subDbContext.Games.Count());
            subHelper.Received(1).SortGamesByDefaultOrder();
            Assert.IsNull(testObject.SelectedGame);

            var week = (int)(from weekCount in subDbContext.WeekCounts
                             select weekCount)
                             .FirstOrDefault<WeekCount>()
                             .Count;
            Assert.AreEqual(week, testObject.Week);
            Assert.IsFalse(testObject.IsFindGameFilterApplied);
            Assert.IsFalse(testObject.IsGamesReadOnly);
            Assert.IsFalse(testObject.IsShowAllGamesEnabled);
            Assert.IsNull(testObject.SelectedGame);
        }

        [TestMethod]
        public void ShowAllGamesCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();

            var subGlobals = Substitute.For<IGlobals>();
            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();

            var ex = new Exception();
            subHelper.When(x => x.SortGamesByDefaultOrder()).Do(x => { throw ex; });

            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.ShowAllGamesCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ViewGamesCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subDbContext.SubWeekCounts();

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            subHelper.When(x => { x.SortGamesByDefaultOrder(); }).Do(x => { });

            var subGlobals = Substitute.For<IGlobals>();
            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModel>(subDbContext, subGlobals, subHelper, subGameFinder);

            var focusedProperty = "GuestName";
            testObject.When(x => { x.MoveFocusTo(focusedProperty); }).DoNotCallBase();

            // Act
            testObject.ViewGamesCommand.Execute(null);

            // Assert
            Assert.AreNotEqual(0, subDbContext.Games.Count());
            subHelper.Received(1).SortGamesByDefaultOrder();
            Assert.IsNull(testObject.SelectedGame);
            testObject.Received(1).MoveFocusTo(focusedProperty);

            var week = (int)(from weekCount in subDbContext.WeekCounts
                             select weekCount)
                             .FirstOrDefault<WeekCount>()
                             .Count;
            Assert.AreEqual(week, testObject.Week);
        }

        [TestMethod]
        public void ViewGamesCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subGlobals = Substitute.For<IGlobals>();

            var ex = new Exception();
            subDbContext.Games.Returns(x => { throw ex; });

            var subHelper = Substitute.For<IGamesWindowViewModelHelper>();
            var subGameFinder = Substitute.For<IGameFinderWindow>();
            var testObject = new GamesWindowViewModel(subDbContext, subGlobals, subHelper, subGameFinder);

            // Act
            testObject.ViewGamesCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void MoveFocusToTest()
        {
            //// Arrange
            //var subDbContext = Substitute.For<FootballDbEntities>();
            //var testObject = new GamesWindowViewModel(subDbContext);

            //// Act
            //// Assert
        }
    }
}