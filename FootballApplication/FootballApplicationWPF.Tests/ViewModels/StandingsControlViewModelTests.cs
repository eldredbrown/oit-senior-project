﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class StandingsControlViewModelTests
    {
        [TestMethod]
        public void StandingsControlViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new StandingsControlViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            // Assert
            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void StandingsControlViewModelTest_DbContextArgNotNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences();

            IStandingsControlViewModel testObject = null;
            try
            {
                // Act
                testObject = new StandingsControlViewModel(subDbContext);
            }
            catch (ArgumentNullException)
            {
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IStandingsControlViewModel));
        }

        [TestMethod]
        public void ViewStandingsCommandTest_NoExceptionCaught_LoadsLeaguesTreeViewViewModelWithLeagues()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagues();

            var testObject = new StandingsControlViewModel(subDbContext);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var leaguesList = new List<ITreeViewItemViewModel>();
            var leagues = new ReadOnlyCollection<ITreeViewItemViewModel>(leaguesList);
            subLeaguesTreeViewViewModel.Leagues.Returns(leagues);

            // Act
            testObject.ViewStandingsCommand.Execute(subLeaguesTreeViewViewModel);

            // Assert
            Assert.IsNull(testObject.StandingsResult);
            Assert.IsNotNull(testObject.Leagues);
        }

        [TestMethod]
        public void ViewStandingsCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagues();

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new StandingsControlViewModel(subDbContext, subGlobals);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var leaguesList = new List<ITreeViewItemViewModel>();
            var leagues = new ReadOnlyCollection<ITreeViewItemViewModel>(leaguesList);

            var ex = new Exception();
            subLeaguesTreeViewViewModel.Leagues.Returns(x => { throw ex; });

            // Act
            testObject.ViewStandingsCommand.Execute(subLeaguesTreeViewViewModel);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}