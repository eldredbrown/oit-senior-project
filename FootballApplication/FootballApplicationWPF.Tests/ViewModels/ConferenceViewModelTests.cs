﻿using System;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class ConferenceViewModelTests
    {
        [TestMethod]
        public void ConferenceViewModelTest_ConferenceArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            try
            {
                // Act
                var testObject = new ConferenceViewModel(null, subDbContext, null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("conference", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void ConferenceViewModelTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subConference = new Conference();

            ITreeViewItemViewModel testObject = null;
            try
            {
                // Act
                testObject = new ConferenceViewModel(null, subDbContext, subConference);
            }
            catch
            {
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(ITreeViewItemViewModel));
        }

        [TestMethod]
        public void LoadChildrenTest_DivisionsNotEmpty_LoadsChildren()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubDivisions(false);

            var subConference = new Conference();
            var testObject = new ConferenceViewModel(null, subDbContext, subConference);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            Assert.AreSame(children, testObject.Children);
        }

        [TestMethod]
        public void LoadChildrenTest_DivisionsEmpty_ChildrenSetToNull()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubDivisions(true);

            var subConference = new Conference();
            var testObject = new ConferenceViewModel(null, subDbContext, subConference);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            Assert.IsNull(testObject.Children);
        }

        [TestMethod]
        public void LoadChildrenTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubDivisions(true);

            var ex = new Exception();
            subDbContext.Divisions.Returns(x => { throw ex; });

            var subConference = new Conference();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new ConferenceViewModel(null, subDbContext, subConference, subGlobals);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ShowStandingsTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subParent = Substitute.For<ITreeViewItemViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetConferenceSeasonStandings();

            var subConference = new Conference();
            var testObject = new ConferenceViewModel(subParent, subDbContext, subConference);

            var subAssociationStandingsResult = new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();

            // Act
            testObject.ShowStandings(subAssociationStandingsResult);

            // Assert
            Assert.AreSame(subAssociationStandingsResult, testObject.Parent.StandingsResult);
        }

        [TestMethod]
        public void ShowStandingsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subParent = Substitute.For<ITreeViewItemViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetConferenceSeasonStandings();

            var ex = new Exception();
            subDbContext.usp_GetConferenceSeasonStandings(Arg.Any<string>(), Arg.Any<int>()).Returns(x => { throw ex; });

            var subConference = new Conference();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new ConferenceViewModel(subParent, subDbContext, subConference, subGlobals);

            var subAssociationStandingsResult = new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();

            // Act
            testObject.ShowStandings(subAssociationStandingsResult);

            // Assert
            Assert.IsNull(testObject.Parent.StandingsResult);
            subGlobals.Received(1).ShowExceptionMessage(ex.InnerException);
        }
    }
}