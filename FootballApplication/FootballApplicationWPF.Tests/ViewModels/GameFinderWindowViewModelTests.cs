﻿using EldredBrown.FootballApplicationWPF.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class GameFinderWindowViewModelTests
    {
        [TestMethod]
        public void GameFinderWindowViewModelTest()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            // Act
            var testObject = new GameFinderWindowViewModel(subDbContext);

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IGameFinderWindowViewModel));
        }

        [TestMethod]
        public void WindowLoadedCommandTest()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = Substitute.ForPartsOf<GameFinderWindowViewModel>(subDbContext, null);

            var focusedProperty = "GuestName";
            testObject.When(x => { x.MoveFocusTo(focusedProperty); }).DoNotCallBase();

            // Act
            testObject.WindowLoadedCommand.Execute(null);

            // Assert
            testObject.Received(1).MoveFocusTo(focusedProperty);
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsNull_ThrowsDataValidationException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.GuestName = null;

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsWhiteSpace_ThrowsDataValidationException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.GuestName = " ";

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsNull_ThrowsDataValidationException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.HostName = null;

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsWhiteSpace_ThrowsDataValidationException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.HostName = " ";

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameAndHostNameEqual_ThrowsDataValidationException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.GuestName = "Team";
            testObject.HostName = "Team";

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                Assert.AreEqual(WpfGlobals.Constants.DifferentTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameAndHostNameNotEqual_NoExceptionThrown()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var testObject = new GameFinderWindowViewModel(subDbContext);
            testObject.GuestName = "Guest";
            testObject.HostName = "Host";

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch
            {
                // Assert
                Assert.Fail("ValidateDataEntry threw an unexpected exception.");
            }
        }
    }
}