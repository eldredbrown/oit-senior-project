﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.Windows;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers.Tests
{
    [TestClass]
    public class GamesWindowViewModelHelperTests
    {
        public virtual TeamSeason GetGameTeamSeason(FootballDbEntities dbContext, string teamName)
        {
            TeamSeason retVal = (from teamSeason in dbContext.TeamSeasons
                                 where (teamSeason.Team.Name == teamName) && (SharedGlobals.SelectedSeason == 2016)
                                 select teamSeason)
                                 .FirstOrDefault();
            return retVal;
        }

        [TestMethod]
        public void GamesWindowViewModelHelperTest_ContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new GamesWindowViewModelHelper(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("context", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void GamesWindowViewModelHelperTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            IGamesWindowViewModelHelper testObject = null;
            try
            {
                // Act
                testObject = new GamesWindowViewModelHelper(subContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(IGamesWindowViewModelHelper));
        }

        [TestMethod]
        public void ApplyFindGameFilterTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            subContext.GameFinder = Substitute.For<IGameFinderWindow>();

            var subDataContext = Substitute.For<IGameFinderWindowViewModel>();

            var guestName = "Guest2";
            subDataContext.GuestName.Returns(guestName);

            var hostName = "Host2";
            subDataContext.HostName.Returns(hostName);

            subContext.GameFinder.DataContext = subDataContext;

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.When(x => { x.LoadGames(Arg.Any<IQueryable<Game>>()); }).DoNotCallBase();

            // Act
            testObject.ApplyFindGameFilter();

            // Assert
            testObject.Received().LoadGames(Arg.Any<IQueryable<Game>>());
            Assert.IsTrue(subContext.IsFindGameFilterApplied);
        }

        [TestMethod]
        public void ApplyFindGameFilterTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            subContext.GameFinder = Substitute.For<IGameFinderWindow>();

            var subDataContext = Substitute.For<IGameFinderWindowViewModel>();

            var guestName = "Guest2";
            subDataContext.GuestName.Returns(guestName);

            var hostName = "Host2";
            subDataContext.HostName.Returns(hostName);

            subContext.GameFinder.DataContext = subDataContext;

            var subGlobals = Substitute.For<IGlobals>();

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, subGlobals);

            var ex = new Exception();
            testObject.When(x => { x.LoadGames(Arg.Any<IQueryable<Game>>()); }).Do(x => { throw ex; });

            // Act
            testObject.ApplyFindGameFilter();

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ClearDataEntryControlsTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var focusedProperty = "GuestName";
            subContext.When(x => { x.MoveFocusTo(focusedProperty); }).Do(x => { });

            var testObject = new GamesWindowViewModelHelper(subContext);

            // Act
            testObject.ClearDataEntryControls();

            // Assert
            Assert.AreEqual(String.Empty, subContext.GuestName);
            Assert.AreEqual(0, subContext.GuestScore);
            Assert.AreEqual(String.Empty, subContext.HostName);
            Assert.AreEqual(0, subContext.HostScore);
            Assert.IsFalse(subContext.IsPlayoffGame);
            Assert.AreEqual(String.Empty, subContext.Notes);
            Assert.AreEqual(Visibility.Visible, subContext.AddGameControlVisibility);
            Assert.AreEqual(Visibility.Hidden, subContext.EditGameControlVisibility);
            Assert.AreEqual(Visibility.Hidden, subContext.RemoveGameControlVisibility);
            subContext.Received(1).MoveFocusTo(focusedProperty);
        }

        [TestMethod]
        public void ClearDataEntryControlsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var ex = new Exception();
            var focusedProperty = "GuestName";
            subContext.When(x => { x.MoveFocusTo(focusedProperty); }).Do(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, globals: subGlobals);

            // Act
            testObject.ClearDataEntryControls();

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void EditScoringDataByTeamTest_PythPctNull_TeamPythWinsAndLossesSetToZero()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, null);

            var subTeam = new Team { Name = "Team1" };
            var subTeamSeason = new TeamSeason
            {
                TeamName = subTeam.Name,
                SeasonID = (int)SharedGlobals.SelectedSeason,
                Games = 2,
                PointsFor = 1,
                PointsAgainst = 1
            };
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(subTeamSeason);

            subCalculator.CalculatePythagoreanWinningPercentage(Arg.Any<TeamSeason>()).Returns((double?)null);
            subCalculator.Multiply(Arg.Any<double>(), Arg.Any<double>()).Returns(1);

            var subOperation = new Operation(subCalculator.Add);
            var teamScore = 1;
            var opponentScore = 1;

            // Act
            testObject.EditScoringDataByTeam(subTeam, subOperation, teamScore, opponentScore);

            // Assert
            testObject.Received(1).GetGameTeamSeason(subTeam.Name);
            Assert.AreEqual((int)subOperation(1, teamScore), subTeamSeason.PointsFor);
            Assert.AreEqual((int)subOperation(1, opponentScore), subTeamSeason.PointsAgainst);
            subCalculator.Received(1).CalculatePythagoreanWinningPercentage(subTeamSeason);
            Assert.AreEqual(0, subTeamSeason.PythagoreanWins);
            Assert.AreEqual(0, subTeamSeason.PythagoreanLosses);
        }

        [TestMethod]
        public void EditScoringDataByTeamTest_PythPctNotNull_TeamPythWinsAndLossesComputed()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, null);

            var subTeam = new Team { Name = "Team1" };
            var subTeamSeason = new TeamSeason
            {
                TeamName = subTeam.Name,
                SeasonID = (int)SharedGlobals.SelectedSeason,
                Games = 2,
                PointsFor = 1,
                PointsAgainst = 1
            };
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(subTeamSeason);

            var pythPct = 0.600;
            subCalculator.CalculatePythagoreanWinningPercentage(Arg.Any<TeamSeason>()).Returns(pythPct);
            subCalculator.Multiply(Arg.Any<double>(), Arg.Any<double>()).Returns(1);

            var subOperation = new Operation(subCalculator.Add);
            var teamScore = 1;
            var opponentScore = 1;

            // Act
            testObject.EditScoringDataByTeam(subTeam, subOperation, teamScore, opponentScore);

            // Assert
            testObject.Received(1).GetGameTeamSeason(subTeam.Name);
            Assert.AreEqual((int)subOperation(1, teamScore), subTeamSeason.PointsFor);
            Assert.AreEqual((int)subOperation(1, opponentScore), subTeamSeason.PointsAgainst);
            subCalculator.Received(1).CalculatePythagoreanWinningPercentage(subTeamSeason);
            subCalculator.Received(1).Multiply((double)pythPct, subTeamSeason.Games);
            Assert.AreEqual(1, subTeamSeason.PythagoreanWins);
            subCalculator.Received(1).Multiply((double)(1 - pythPct), subTeamSeason.Games);
            Assert.AreEqual(1, subTeamSeason.PythagoreanLosses);
        }

        [TestMethod]
        public void EditTeamsTest_DirectionUp_OperationEqualsAdd()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subCalculator = Substitute.For<ICalculator>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, null);

            var currentGame = new Game();
            var operation = new Operation(subCalculator.Add);
            testObject.When(x => { x.ProcessGame(currentGame, operation); }).DoNotCallBase();

            // Act
            testObject.EditTeams(currentGame, Direction.Up);

            // Assert
            testObject.Received().ProcessGame(currentGame, operation);
        }

        [TestMethod]
        public void EditTeamsTest_DirectionDown_OperationEqualsSubtract()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subCalculator = Substitute.For<ICalculator>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, null);

            var currentGame = new Game();
            var operation = new Operation(subCalculator.Subtract);
            testObject.When(x => { x.ProcessGame(currentGame, operation); }).DoNotCallBase();

            // Act
            testObject.EditTeams(currentGame, Direction.Down);

            // Assert
            testObject.Received().ProcessGame(currentGame, operation);
        }

        [TestMethod]
        public void EditTeamsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, subGlobals);

            var currentGame = new Game();
            var operation = new Operation(subCalculator.Subtract);
            var ex = new Exception();
            testObject.When(x => { x.ProcessGame(currentGame, operation); }).Do(x => { throw ex; });

            // Act
            testObject.EditTeams(currentGame, Direction.Down);

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void EditWinLossDataTest_WinnerAndLoserBothNull_TiesUpdated()
        {
            // Arrange
            SharedGlobals.SelectedSeason = 2016;

            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, subGlobals);

            var currentGame = new Game
            {
                Guest = new Team { Name = "Team1" },
                Host = new Team { Name = "Team2" },
            };

            var guestSeason = GetGameTeamSeason(subDbContext, currentGame.Guest.Name);
            guestSeason.Games = 0;
            guestSeason.Wins = 0;
            guestSeason.Losses = 0;
            guestSeason.Ties = 0;

            var hostSeason = GetGameTeamSeason(subDbContext, currentGame.Host.Name);
            hostSeason.Games = 0;
            hostSeason.Wins = 0;
            hostSeason.Losses = 0;
            hostSeason.Ties = 0;

            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(x => guestSeason, x => hostSeason);

            subCalculator.CalculateWinningPercentage(guestSeason).Returns(0.500);
            subCalculator.CalculateWinningPercentage(hostSeason).Returns(0.500);
            subCalculator.Add(Arg.Any<double>(), Arg.Any<double>()).Returns(1);
            var operation = new Operation(subCalculator.Add);

            // Act
            testObject.EditWinLossData(currentGame, operation);

            // Assert
            testObject.Received(1).GetGameTeamSeason(currentGame.Guest.Name);
            testObject.Received(1).GetGameTeamSeason(currentGame.Host.Name);
            Assert.AreEqual(1, guestSeason.Games);
            Assert.AreEqual(0, guestSeason.Wins);
            Assert.AreEqual(0, guestSeason.Losses);
            Assert.AreEqual(1, guestSeason.Ties);
            Assert.AreEqual(1, hostSeason.Games);
            Assert.AreEqual(0, hostSeason.Wins);
            Assert.AreEqual(0, hostSeason.Losses);
            Assert.AreEqual(1, hostSeason.Ties);
            subCalculator.Received(1).CalculateWinningPercentage(guestSeason);
            Assert.AreEqual(0.500, guestSeason.WinningPercentage);
            subCalculator.Received(1).CalculateWinningPercentage(hostSeason);
            Assert.AreEqual(0.500, hostSeason.WinningPercentage);
        }

        [TestMethod]
        public void EditWinLossDataTest_WinnerAndLoserNotNull_WinnerWinsAndLoserLossesUpdated()
        {
            // Arrange
            SharedGlobals.SelectedSeason = 2016;

            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, subGlobals);

            var currentGame = new Game
            {
                Guest = new Team { Name = "Team1" },
                Host = new Team { Name = "Team2" },
                Winner = new Team { Name = "Team2" },
                Loser = new Team { Name = "Team1" }
            };

            var guestSeason = GetGameTeamSeason(subDbContext, currentGame.Guest.Name);
            guestSeason.Games = 0;
            guestSeason.Wins = 0;
            guestSeason.Losses = 0;
            guestSeason.Ties = 0;

            var hostSeason = GetGameTeamSeason(subDbContext, currentGame.Host.Name);
            hostSeason.Games = 0;
            hostSeason.Wins = 0;
            hostSeason.Losses = 0;
            hostSeason.Ties = 0;

            var winnerSeason = hostSeason;
            var loserSeason = guestSeason;

            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(x => hostSeason, x => guestSeason, x => winnerSeason, x => loserSeason);

            subCalculator.CalculateWinningPercentage(winnerSeason).Returns(1.000);
            subCalculator.CalculateWinningPercentage(loserSeason).Returns(0.000);
            subCalculator.Add(Arg.Any<double>(), Arg.Any<double>()).Returns(1);
            var operation = new Operation(subCalculator.Add);

            // Act
            testObject.EditWinLossData(currentGame, operation);

            // Assert
            testObject.Received(2).GetGameTeamSeason(currentGame.Guest.Name);
            testObject.Received(2).GetGameTeamSeason(currentGame.Host.Name);
            Assert.AreEqual(1, winnerSeason.Games);
            Assert.AreEqual(1, winnerSeason.Wins);
            Assert.AreEqual(0, winnerSeason.Losses);
            Assert.AreEqual(0, winnerSeason.Ties);
            Assert.AreEqual(1, loserSeason.Games);
            Assert.AreEqual(0, loserSeason.Wins);
            Assert.AreEqual(1, loserSeason.Losses);
            Assert.AreEqual(0, loserSeason.Ties);
            subCalculator.Received(1).CalculateWinningPercentage(winnerSeason);
            Assert.AreEqual(1.000, winnerSeason.WinningPercentage);
            subCalculator.Received(1).CalculateWinningPercentage(loserSeason);
            Assert.AreEqual(0.000, loserSeason.WinningPercentage);
        }

        [TestMethod]
        public void EditWinLossDataTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            SharedGlobals.SelectedSeason = 2016;

            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, subGlobals);

            var currentGame = new Game
            {
                Guest = new Team { Name = "Team1" },
                Host = new Team { Name = "Team2" },
                Winner = new Team { Name = "Team2" },
                Loser = new Team { Name = "Team1" }
            };

            var guestSeason = GetGameTeamSeason(subDbContext, currentGame.Guest.Name);
            guestSeason.Games = 0;
            guestSeason.Wins = 0;
            guestSeason.Losses = 0;
            guestSeason.Ties = 0;

            var hostSeason = GetGameTeamSeason(subDbContext, currentGame.Host.Name);
            hostSeason.Games = 0;
            hostSeason.Wins = 0;
            hostSeason.Losses = 0;
            hostSeason.Ties = 0;

            var winnerSeason = hostSeason;
            var loserSeason = guestSeason;

            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(x => hostSeason, x => guestSeason, x => winnerSeason, x => loserSeason);

            var ex = new Exception();
            subCalculator.CalculateWinningPercentage(Arg.Any<TeamSeason>()).Returns(x => { throw ex; });
            var operation = new Operation(subCalculator.Add);

            // Act
            testObject.EditWinLossData(currentGame, operation);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void GetGameTeamSeasonTest()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            var testObject = new GamesWindowViewModelHelper(subContext);

            var teamName = "Team1";
            SharedGlobals.SelectedSeason = 2016;

            // Act
            var result = testObject.GetGameTeamSeason(teamName);

            // Assert
            Assert.IsInstanceOfType(result, typeof(TeamSeason));
            Assert.AreEqual(teamName, result.Team.Name);
            Assert.AreEqual(SharedGlobals.SelectedSeason, result.SeasonID);
        }

        [TestMethod]
        public void LoadGamesTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            var testObject = new GamesWindowViewModelHelper(subContext);

            // Act
            testObject.LoadGames(subDbContext.Games);

            // Assert
            Assert.IsInstanceOfType(subContext.Games, typeof(ObservableCollection<Game>));
            Assert.AreEqual(subDbContext.Games.Count(), subContext.Games.Count);
        }

        [TestMethod]
        public void LoadGamesTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, globals: subGlobals);

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();

            var ex = new Exception();
            subContext.When(x => { x.Games = Arg.Any<ObservableCollection<Game>>(); }).Do(x => { throw ex; });

            // Act
            testObject.LoadGames(subDbContext.Games);

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void PopulateDataEntryControlsTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var selectedGame = new Game();
            selectedGame.Week = 1;
            selectedGame.GuestName = "Guest";
            selectedGame.GuestScore = 7;
            selectedGame.HostName = "Host";
            selectedGame.HostScore = 7;
            selectedGame.IsPlayoffGame = false;
            selectedGame.Notes = "Notes";
            subContext.SelectedGame = selectedGame;

            var testObject = new GamesWindowViewModelHelper(subContext);

            // Act
            testObject.PopulateDataEntryControls();

            // Assert
            Assert.AreEqual(selectedGame.Week, subContext.Week);
            Assert.AreEqual(selectedGame.GuestName, subContext.GuestName);
            Assert.AreEqual(selectedGame.GuestScore, subContext.GuestScore);
            Assert.AreEqual(selectedGame.HostName, subContext.HostName);
            Assert.AreEqual(selectedGame.HostScore, subContext.HostScore);
            Assert.AreEqual(selectedGame.IsPlayoffGame, subContext.IsPlayoffGame);
            Assert.AreEqual(selectedGame.Notes, subContext.Notes);

            Assert.AreEqual(Visibility.Hidden, subContext.AddGameControlVisibility);
            Assert.AreEqual(Visibility.Visible, subContext.EditGameControlVisibility);
            Assert.AreEqual(Visibility.Visible, subContext.RemoveGameControlVisibility);
        }

        [TestMethod]
        public void PopulateDataEntryControlsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var ex = new Exception();
            subContext.SelectedGame.Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, globals: subGlobals);

            // Act
            testObject.PopulateDataEntryControls();

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ProcessGameTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subCalculator = Substitute.For<ICalculator>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, null);

            var currentGame = new Game();
            currentGame.Guest = new Team();
            currentGame.GuestScore = 7;
            currentGame.Host = new Team();
            currentGame.HostScore = 7;

            var operation = new Operation(subCalculator.Add);

            testObject.When(x => { x.EditWinLossData(currentGame, operation); }).DoNotCallBase();
            testObject.When(x => { x.EditScoringDataByTeam(currentGame.Guest, operation, currentGame.GuestScore, currentGame.HostScore); }).DoNotCallBase();
            testObject.When(x => { x.EditScoringDataByTeam(currentGame.Host, operation, currentGame.HostScore, currentGame.GuestScore); }).DoNotCallBase();

            // Act
            testObject.ProcessGame(currentGame, operation);

            // Assert
            testObject.Received(1).EditWinLossData(currentGame, operation);
            testObject.Received(1).EditScoringDataByTeam(currentGame.Guest, operation, currentGame.GuestScore, currentGame.HostScore);
            testObject.Received(1).EditScoringDataByTeam(currentGame.Host, operation, currentGame.HostScore, currentGame.GuestScore);
        }

        [TestMethod]
        public void ProcessGameTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, subCalculator, subGlobals);

            var currentGame = new Game();
            currentGame.Guest = new Team();
            currentGame.GuestScore = 7;
            currentGame.Host = new Team();
            currentGame.HostScore = 7;

            var operation = new Operation(subCalculator.Add);
            var ex = new Exception();
            testObject.When(x => { x.EditWinLossData(currentGame, operation); }).Do(x => { throw ex; });

            // Act
            testObject.ProcessGame(currentGame, operation);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void SaveChangesTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = new GamesWindowViewModelHelper(subContext, subCalculator);

            // Act
            testObject.SaveChanges();

            // Assert
            subDbContext.Received(1).SaveChanges();
        }

        [TestMethod]
        public void SaveChangesTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            var ex = new Exception();
            subDbContext.When(x => { x.SaveChanges(); }).Do(x => { throw ex; });
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, subCalculator, subGlobals);

            // Act
            testObject.SaveChanges();

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void SortGamesByDefaultOrderTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.When(x => { x.LoadGames(Arg.Any<IQueryable<Game>>()); }).DoNotCallBase();

            // Act
            testObject.SortGamesByDefaultOrder();

            // Assert
            testObject.Received(1).LoadGames(Arg.Any<IQueryable<Game>>());
        }

        [TestMethod]
        public void SortGamesByDefaultOrderTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();
            subContext.DbContext.Returns(subDbContext);

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, subGlobals);

            var ex = new Exception();
            testObject.When(x => { x.LoadGames(Arg.Any<IQueryable<Game>>()); }).Do(x => { throw ex; });

            // Act
            testObject.SortGamesByDefaultOrder();

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void StoreNewGameValuesTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeams();
            subContext.DbContext.Returns(subDbContext);

            subContext.Week = 1;
            subContext.GuestName = "Team1";
            subContext.HostName = "Team2";
            var testObject = new GamesWindowViewModelHelper(subContext);

            var subGame = Substitute.For<IGame>();
            subGame.When(x => { x.DecideWinnerAndLoser(); }).Do(x => { });

            // Act
            var result = testObject.StoreNewGameValues(subGame);

            // Assert
            Assert.IsInstanceOfType(result, typeof(IGame));
            Assert.AreEqual(0, result.ID);
            Assert.AreEqual(subContext.Week, result.Week);
            Assert.IsInstanceOfType(result.Guest, typeof(Team));
            Assert.AreEqual(subContext.GuestScore, result.GuestScore);
            Assert.IsInstanceOfType(result.Host, typeof(Team));
            Assert.AreEqual(subContext.HostScore, result.HostScore);
            Assert.AreEqual(subContext.IsPlayoffGame, result.IsPlayoffGame);
            Assert.AreEqual(subContext.Notes, result.Notes);
            subGame.Received().DecideWinnerAndLoser();
        }

        [TestMethod]
        public void StoreNewGameValuesTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeams();
            subContext.DbContext.Returns(subDbContext);

            subContext.Week = 1;
            subContext.GuestName = "Team1";
            subContext.HostName = "Team2";

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, globals: subGlobals);

            var subGame = Substitute.For<IGame>();

            var ex = new Exception();
            subGame.When(x => { x.DecideWinnerAndLoser(); }).Do(x => { throw ex; });

            // Act
            var result = testObject.StoreNewGameValues(subGame);

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void StoreOldGameValuesTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeams();
            subContext.DbContext.Returns(subDbContext);

            var subGame = Substitute.For<IGame>();
            subGame.Week = 1;
            subGame.SeasonID = (int)SharedGlobals.SelectedSeason;
            subGame.Guest = new Team { Name = "Guest" };
            subGame.GuestScore = 7;
            subGame.Host = new Team { Name = "Host" };
            subGame.HostScore = 7;
            subGame.IsPlayoffGame = false;
            subGame.Notes = "Notes";
            subGame.When(x => { x.DecideWinnerAndLoser(); }).Do(x => { });
            subContext.SelectedGame = subGame;

            var testObject = new GamesWindowViewModelHelper(subContext);

            // Act
            var result = testObject.StoreOldGameValues(subGame);

            // Assert
            var selectedGame = subContext.SelectedGame;
            Assert.IsInstanceOfType(result, typeof(IGame));
            Assert.AreEqual(0, result.ID);
            Assert.AreEqual(selectedGame.Week, result.Week);
            Assert.IsInstanceOfType(result.Guest, typeof(Team));
            Assert.AreEqual(selectedGame.GuestScore, result.GuestScore);
            Assert.IsInstanceOfType(result.Host, typeof(Team));
            Assert.AreEqual(selectedGame.HostScore, result.HostScore);
            Assert.AreEqual(selectedGame.IsPlayoffGame, result.IsPlayoffGame);
            Assert.AreEqual(selectedGame.Notes, result.Notes);
            subGame.Received().DecideWinnerAndLoser();
        }

        [TestMethod]
        public void StoreOldGameValuesTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeams();
            subContext.DbContext.Returns(subDbContext);

            var subGame = Substitute.For<IGame>();
            subGame.Week = 1;
            subGame.Guest = new Team { Name = "Guest" };
            subGame.GuestScore = 7;
            subGame.Host = new Team { Name = "Host" };
            subGame.HostScore = 7;
            subGame.IsPlayoffGame = false;
            subGame.Notes = "Notes";
            var ex = new Exception();
            subGame.When(x => { x.DecideWinnerAndLoser(); }).Do(x => { throw ex; });
            subContext.SelectedGame = subGame;

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new GamesWindowViewModelHelper(subContext, globals: subGlobals);

            // Act
            var result = testObject.StoreOldGameValues(subGame);

            // Assert
            subGlobals.Received().ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsNull_BothTeamsNeeded()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = null;

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns((TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("GuestName");
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsEmpty_BothTeamsNeeded()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = " ";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns((TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("GuestName");
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsNull_BothTeamsNeeded()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = "Team";
            subContext.HostName = null;

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns((TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("HostName");
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsEmpty_BothTeamsNeeded()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = "Team";
            subContext.HostName = " ";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns((TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("HostName");
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNotInTeamSeasonsTable_TeamNotInDatabase()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = "Team";
            subContext.HostName = "Team";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns((TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("GuestName");
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNotInTeamSeasonsTable_TeamNotInDatabase()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = "Guest";
            subContext.HostName = "Team";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(
                x => new TeamSeason(), x => (TeamSeason)null);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("HostName");
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestAndHostAreSameTeam_DifferentTeamsNeeded()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();
            subContext.GuestName = "Guest";
            subContext.HostName = "Guest";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(new TeamSeason());

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received().MoveFocusTo("GuestName");
                Assert.AreEqual(WpfGlobals.Constants.DifferentTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected exception not caught");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestAndHostAreDifferentTeams_NoExceptionThrown()
        {
            // Arrange
            var subContext = Substitute.For<IGamesWindowViewModel>();

            subContext.GuestName = "Guest";
            subContext.HostName = "Host";

            var testObject = Substitute.ForPartsOf<GamesWindowViewModelHelper>(subContext, null, null);
            testObject.GetGameTeamSeason(Arg.Any<string>()).Returns(new TeamSeason());

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException)
            {
                // Assert
                Assert.Fail("Unexpected DataValidationException caught");
            }
        }
    }
}