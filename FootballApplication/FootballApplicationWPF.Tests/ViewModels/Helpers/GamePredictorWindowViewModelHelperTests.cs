﻿using System;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers.Tests
{
    [TestClass]
    public class GamePredictorWindowViewModelHelperTests
    {
        [TestMethod]
        public void GamePredictorWindowViewModelHelperTest_ContextNull_ThrowsArgumentNullException()
        {
            // Arrange

            try
            {
                // Act
                var testObject = new GamePredictorWindowViewModelHelper(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("context", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void GamePredictorWindowViewModelHelperTest_ContextNotNull_ConstructsObject()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            IGamePredictorWindowViewModelHelper testObject = null;
            try
            {
                // Act
                testObject = new GamePredictorWindowViewModelHelper(subContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(IGamePredictorWindowViewModelHelper));
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsNull_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            var textField = "GuestName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameIsWhitespace_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns(" ");

            var textField = "GuestName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsNull_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team not in database");

            var textField = "HostName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameIsWhitespace_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team not in database");
            subContext.HostName.Returns(" ");

            var textField = "HostName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.BothTeamsNeededErrorMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestNameNotInDatabase_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team not in database");
            subContext.HostName.Returns("Team not in database");

            var textField = "GuestName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            SharedGlobals.SelectedSeason = 2016;

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_GuestSeasonNotInDatabase_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team1");
            subContext.GuestSelectedSeason = 0;
            subContext.HostName.Returns("Team not in database");

            var textField = "GuestName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostNameNotInDatabase_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team1");
            subContext.GuestSelectedSeason = 2016;
            subContext.HostName.Returns("Team not in database");

            var textField = "HostName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_HostSeasonNotInDatabase_ThrowsDataValidationException()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team1");
            subContext.GuestSelectedSeason = 2016;
            subContext.HostName.Returns("Team2");
            subContext.HostSelectedSeason = 0;

            var textField = "HostName";
            subContext.When(x => { x.MoveFocusTo(textField); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            try
            {
                // Act
                testObject.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                // Assert
                subContext.Received(1).MoveFocusTo(textField);
                Assert.AreEqual(WpfGlobals.Constants.TeamNotInDatabaseMessage, ex.Message);
                return;
            }

            Assert.Fail("Expected DataValidationException not thrown");
        }

        [TestMethod]
        public void ValidateDataEntryTest_BothGuestAndHostInDatabase_ReturnsGuestAndHost()
        {
            // Arrange
            var subContext = Substitute.For<IGamePredictorWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();
            subContext.DbContext.Returns(subDbContext);

            subContext.GuestName.Returns("Team1");
            subContext.GuestSelectedSeason = 2016;
            subContext.HostName.Returns("Team2");
            subContext.HostSelectedSeason = 2016;

            subContext.When(x => { x.MoveFocusTo(Arg.Any<string>()); }).Do(x => { });

            var testObject = new GamePredictorWindowViewModelHelper(subContext);

            Matchup matchup = new Matchup();
            try
            {
                // Act
                matchup = testObject.ValidateDataEntry();
            }
            catch (DataValidationException)
            {
                // Assert
                Assert.Fail("Unexpected DataValidationException caught");
            }

            Assert.IsInstanceOfType(matchup, typeof(Matchup));
        }
    }
}