﻿using System;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers.Tests
{
    [TestClass]
    public class MainWindowViewModelHelperTests
    {
        [TestMethod]
        public void MainWindowViewModelHelperTest_ContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            IMainWindowViewModel context = null;

            try
            {
                // Act
                var testObject = new MainWindowViewModelHelper(context);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("context", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void MainWindowViewModelHelperTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            IMainWindowViewModelHelper testObject = null;
            try
            {
                // Act
                testObject = new MainWindowViewModelHelper(subContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IMainWindowViewModelHelper));
        }

        [TestMethod]
        public void UpdateRankingsTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubTeamSeasons();
            subDbContext.SaveChanges().Returns(2);
            subContext.DbContext.Returns(subDbContext);

            var testObject = Substitute.ForPartsOf<MainWindowViewModelHelper>(subContext, null, null);
            testObject.When(x => { x.UpdateRankingsByTeamSeason(Arg.Any<TeamSeason>()); }).DoNotCallBase();

            SharedGlobals.SelectedSeason = 2016;

            // Act
            testObject.UpdateRankings();

            // Assert
            testObject.Received(2).UpdateRankingsByTeamSeason(Arg.Any<TeamSeason>());
            subDbContext.Received(1).SaveChanges();
        }

        [TestMethod]
        public void UpdateRankingsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();

            var ex = new Exception();
            subDbContext.SaveChanges().Returns(x => { throw ex; });

            subContext.DbContext.Returns(subDbContext);

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = Substitute.ForPartsOf<MainWindowViewModelHelper>(subContext, null, subGlobals);
            testObject.When(x => { x.UpdateRankingsByTeamSeason(Arg.Any<TeamSeason>()); }).DoNotCallBase();

            // Act
            testObject.UpdateRankings();

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_TeamScheduleTotalsNull_DoesNothing()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: true);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: true);
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = new MainWindowViewModelHelper(subContext, subCalculator);

            var teamSeason = new TeamSeason { Team = new Team { Name = "Team" } };

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subCalculator.Received(0).CalculatePythagoreanWinningPercentage(teamSeason);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_TeamScheduleAveragesNull_DoesNothing()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: false);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: true);
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = new MainWindowViewModelHelper(subContext, subCalculator);

            var teamSeason = new TeamSeason { Team = new Team { Name = "Team" } };

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subCalculator.Received(0).CalculatePythagoreanWinningPercentage(teamSeason);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_TeamScheduleTotalsScheduleGamesNull_DoesNothing()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: false, scheduleGames: null);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: false);
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();
            var testObject = new MainWindowViewModelHelper(subContext, subCalculator);

            var teamSeason = new TeamSeason { Team = new Team { Name = "Team" } };

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subCalculator.Received(0).CalculatePythagoreanWinningPercentage(teamSeason);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_TeamScheduleTotalsScheduleGamesNotNull_ExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: false, scheduleGames: 225);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: false);
            subContext.DbContext.Returns(subDbContext);

            var teamSeason = new TeamSeason
            {
                Team = new Team { Name = "Team" },
                League = new League { Name = "NFL" },
                Games = 16,
                PointsFor = 320,
                PointsAgainst = 240,
                OffensiveAverage = 0,
                DefensiveAverage = 0,
                OffensiveFactor = 0,
                DefensiveFactor = 0,
                OffensiveIndex = 0,
                DefensiveIndex = 0
            };

            var subCalculator = Substitute.For<ICalculator>();
            subCalculator.Divide(teamSeason.PointsFor, teamSeason.Games).Returns(20);
            subCalculator.Divide(teamSeason.PointsAgainst, teamSeason.Games).Returns(15);
            subCalculator.Divide(20, 20).Returns(1.000);
            subCalculator.Divide(15, 15).Returns(1.000);
            subCalculator.CalculatePythagoreanWinningPercentage(teamSeason).Returns(0.5);

            var testObject = new MainWindowViewModelHelper(subContext, subCalculator);

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subCalculator.Received(1).Divide(teamSeason.PointsFor, teamSeason.Games);
            Assert.AreEqual(20, teamSeason.OffensiveAverage);

            subCalculator.Received(1).Divide(teamSeason.PointsAgainst, teamSeason.Games);
            Assert.AreEqual(15, teamSeason.DefensiveAverage);

            subCalculator.Received(1).Divide((double)teamSeason.OffensiveAverage, 20);
            Assert.AreEqual(1.000, teamSeason.OffensiveFactor);

            subCalculator.Received(1).Divide((double)teamSeason.DefensiveAverage, 15);
            Assert.AreEqual(1.000, teamSeason.DefensiveFactor);

            var league = subDbContext.LeagueSeasons.FirstOrDefault();
            Assert.AreEqual((teamSeason.OffensiveAverage + teamSeason.OffensiveFactor * league.AveragePoints) / 2, teamSeason.OffensiveIndex);
            Assert.AreEqual((teamSeason.DefensiveAverage + teamSeason.DefensiveFactor * league.AveragePoints) / 2, teamSeason.DefensiveIndex);

            subCalculator.Received(1).CalculatePythagoreanWinningPercentage(teamSeason);
            Assert.AreEqual(0.5, teamSeason.FinalPythagoreanWinningPercentage);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_InvalidOperationExceptionCaughtAndMessageEqualsNullableObjectMustHaveValue_DoesNothing()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagues();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: false, scheduleGames: 225);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: false);
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();

            var teamSeason = new TeamSeason
            {
                Team = new Team { Name = "Team" },
                League = new League { Name = "NFL" }
            };
            var ex = new InvalidOperationException("Nullable object must have a value.");
            subCalculator.Divide(teamSeason.PointsFor, teamSeason.Games).Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new MainWindowViewModelHelper(subContext, subCalculator, subGlobals);

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subGlobals.DidNotReceive().ShowExceptionMessage(ex, "InvalidOperationException - " + teamSeason.Team.Name);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_InvalidOperationExceptionCaughtAndMessageDoesNotEqualNullableObjectMustHaveValue_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubGetTeamSeasonScheduleTotals(isEmpty: false, scheduleGames: 225);
            subDbContext.SubGetTeamSeasonScheduleAverages(isEmpty: false);
            subContext.DbContext.Returns(subDbContext);

            var subCalculator = Substitute.For<ICalculator>();

            var teamSeason = new TeamSeason {
                Team = new Team { Name = "Team" }, League = new League { Name = "NFL" }
            };
            var ex = new InvalidOperationException();
            subCalculator.Divide(teamSeason.PointsFor, teamSeason.Games).Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new MainWindowViewModelHelper(subContext, subCalculator, subGlobals);

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex, "InvalidOperationException: " + teamSeason.Team.Name);
        }

        [TestMethod]
        public void UpdateRankingsByTeamTest_OtherExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindowViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagues();

            var teamSeason = new TeamSeason { Team = new Team { Name = "Team" } };
            var ex = new Exception();
            subDbContext.usp_GetTeamSeasonScheduleTotals(teamSeason.TeamName, SharedGlobals.SelectedSeason).Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new MainWindowViewModelHelper(subContext, globals: subGlobals);

            // Act
            testObject.UpdateRankingsByTeamSeason(teamSeason);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex.InnerException, "Exception: " + teamSeason.Team.Name);
        }
    }
}