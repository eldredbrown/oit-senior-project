﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass()]
    public class DelegateCommandTests
    {
        [TestMethod()]
        public void DelegateCommandTest_ExecuteArgIsNull_ThrowsException()
        {
            // Arrange
            DelegateCommand testObject = null;
            try
            {
                // Act
                testObject = new DelegateCommand(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("execute", ex.ParamName);
                Assert.IsNull(testObject);
            }
        }

        [TestMethod()]
        public void DelegateCommandTest_ExecuteArgIsNotNull_ObjectConstructed()
        {
            // Arrange
            var subAction = Substitute.For<System.Action<object>>();

            DelegateCommand testObject = null;
            try
            {
                // Act
                testObject = new DelegateCommand(subAction);
            }
            catch
            {
                Assert.Fail("DelegateCommand constructor threw an unexpected exception.");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(DelegateCommand));
        }

        [TestMethod()]
        public void CanExecuteTest()
        {
            // Arrange
            // Act
            // Assert
        }

        [TestMethod()]
        public void ExecuteTest()
        {
            // Arrange
            // Act
            // Assert
        }
    }
}