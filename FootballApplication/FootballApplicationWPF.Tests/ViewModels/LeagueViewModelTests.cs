﻿using System;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class LeagueViewModelTests
    {
        [TestMethod]
        public void LeagueViewModelTest_ParentControlArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            try
            {
                // Act
                var testObject = new LeagueViewModel(subDbContext, null, null);
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("parentControl", ex.ParamName);
                return;
            }

            // Assert
            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void LeagueViewModelTest_LeagueArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();

            try
            {
                // Act
                var testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, null);
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("league", ex.ParamName);
                return;
            }

            // Assert
            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void LeagueViewModelTest_NoArgsNullConferencesNotEmpty_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences(false);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var subLeague = new League();

            ITreeViewItemViewModel testObject = null;
            try
            {
                // Act
                testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, subLeague);
            }
            catch (ArgumentNullException)
            {
                Assert.Fail("Unxpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(ITreeViewItemViewModel));
        }

        [TestMethod]
        public void LoadChildrenTest_ConferencesNotEmpty_LoadsChildren()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences(false);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var subLeague = new League();
            var testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, subLeague);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            Assert.AreSame(children, testObject.Children);
        }

        [TestMethod]
        public void LoadChildrenTest_ConferencesEmptyDivisionsNotEmpty_LoadsChildren()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences(true);
            subDbContext.SubDivisions(false);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var subLeague = new League();
            var testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, subLeague);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            Assert.AreSame(children, testObject.Children);
        }

        [TestMethod]
        public void LoadChildrenTest_ConferencesAndDivisionsEmpty_ChildrenSetToNull()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences(true);
            subDbContext.SubDivisions(true);

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var subLeague = new League();
            var testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, subLeague);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            Assert.IsNull(testObject.Children);
        }

        [TestMethod]
        public void LoadChildrenTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubConferences(true);
            subDbContext.SubDivisions(true);

            var ex = new Exception();
            subDbContext.Conferences.Returns(x => { throw ex; });

            var subLeaguesTreeViewViewModel = Substitute.For<ILeaguesTreeViewViewModel>();
            var subLeague = new League();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new LeagueViewModel(subDbContext, subLeaguesTreeViewViewModel, subLeague, subGlobals);

            ObservableCollection<ITreeViewItemViewModel> children = new ObservableCollection<ITreeViewItemViewModel>();

            // Act
            testObject.LoadChildren(children);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}