﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class LeaguesTreeViewViewModelTests
    {
        [TestMethod]
        public void LeaguesTreeViewViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subParentControl = Substitute.For<IStandingsControlViewModel>();
            var subLeagues = Substitute.For<IEnumerable<League>>();

            try
            {
                // Act
                var testObject = new LeaguesTreeViewViewModel(null, null, null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void LeaguesTreeViewViewModelTest_ParentControlArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            try
            {
                // Act
                var testObject = new LeaguesTreeViewViewModel(subDbContext, null, null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("parentControl", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void LeaguesTreeViewViewModelTest_LeaguesArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subParentControl = Substitute.For<IStandingsControlViewModel>();

            try
            {
                // Act
                var testObject = new LeaguesTreeViewViewModel(subDbContext, subParentControl, null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("leagues", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void LeaguesTreeViewViewModelTest_NoArgsNullNoExceptionCaught_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subParentControl = Substitute.For<IStandingsControlViewModel>();
            var subLeagues = Substitute.For<IEnumerable<League>>();

            var subLeagueViewModels = new List<ITreeViewItemViewModel>();
            var subLeaguesCollection = new ReadOnlyCollection<ITreeViewItemViewModel>(subLeagueViewModels);

            ILeaguesTreeViewViewModel testObject = null;
            try
            {
                // Act
                testObject = new LeaguesTreeViewViewModel(subDbContext, subParentControl, subLeagues, leaguesCollection: subLeaguesCollection);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(ILeaguesTreeViewViewModel));
            Assert.AreSame(subLeaguesCollection, testObject.Leagues);
        }

        [TestMethod]
        public void LeaguesTreeViewViewModelTest_NoArgsNullExceptionCaught_ShowsExceptionMessage()
        {
            //// Arrange
            //var subParentControl = Substitute.For<IStandingsControlViewModel>();
            //var subLeagues = Substitute.For<IEnumerable<League>>();
            //var subDbContext = Substitute.For<FootballDbEntities>();
            //var subGlobals = Substitute.For<IGlobals>();
            //var testObject = new LeaguesTreeViewViewModel(subParentControl, subLeagues, subDbContext, subGlobals);

            //// Act
            //testObject = new LeaguesTreeViewViewModel(subParentControl, subLeagues, subDbContext);

            //// Assert
            //subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}