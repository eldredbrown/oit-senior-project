﻿using System;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace FootballApplicationWPFTests.ViewModels
{
    [TestClass]
    public class GamePredictorWindowViewModelTests
    {
        [TestMethod]
        public void GamePredictorWindowViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange

            try
            {
                // Act
                var testObject = new GamePredictorWindowViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void GamePredictorWindowViewModelTest_NoNullArgs_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            IGamePredictorWindowViewModel testObject = null;
            try
            {
                // Act
                testObject = new GamePredictorWindowViewModel(subDbContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(IGamePredictorWindowViewModel));
        }

        [TestMethod]
        public void CalculatePredictionTest_DataValidationExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();

            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamePredictorWindowViewModelHelper>();
            var ex = new DataValidationException("DVE");
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { throw ex; });

            var testObject = new GamePredictorWindowViewModel(subDbContext, subGlobals, subHelper);

            // Act
            testObject.CalculatePredictionCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void CalculatePredictionTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();

            var subGlobals = Substitute.For<IGlobals>();

            var subHelper = Substitute.For<IGamePredictorWindowViewModelHelper>();
            var ex = new Exception();
            subHelper.When(x => { x.ValidateDataEntry(); }).Do(x => { throw ex; });

            var testObject = new GamePredictorWindowViewModel(subDbContext, subGlobals, subHelper);

            // Act
            testObject.CalculatePredictionCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void CalculatePredictionTest_NoExceptionCaught_CalculatesPredictedScore()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();

            var subHelper = Substitute.For<IGamePredictorWindowViewModelHelper>();

            var guestSeason = (from teamSeason in subDbContext.TeamSeasons
                               where (teamSeason.Team.Name == "Team1") && (teamSeason.SeasonID == 2016)
                               select teamSeason)
                               .First();
            var hostSeason = (from teamSeason in subDbContext.TeamSeasons
                              where (teamSeason.Team.Name == "Team2") && (teamSeason.SeasonID == 2016)
                              select teamSeason)
                              .First();
            subHelper.ValidateDataEntry().Returns(new Matchup(guestSeason, hostSeason));

            var testObject = new GamePredictorWindowViewModel(subDbContext, helper: subHelper);
            testObject.GuestName = "Team1";
            testObject.GuestSelectedSeason = 2016;
            testObject.HostName = "Team2";
            testObject.HostSelectedSeason = 2016;

            // Act
            testObject.CalculatePredictionCommand.Execute(null);

            // Assert
            subHelper.Received(1).ValidateDataEntry();
            Assert.AreEqual((int)((guestSeason.OffensiveFactor * hostSeason.DefensiveAverage + hostSeason.DefensiveFactor * guestSeason.OffensiveAverage) / 2), testObject.GuestScore);
            Assert.AreEqual((int)((hostSeason.OffensiveFactor * guestSeason.DefensiveAverage + guestSeason.DefensiveFactor * hostSeason.OffensiveAverage) / 2), testObject.HostScore);
        }
    }
}
