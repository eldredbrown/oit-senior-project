﻿using System;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class DivisionViewModelTests
    {
        [TestMethod]
        public void DivisionViewModelTest_DivisionArgNull_ThrowsException()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            try
            {
                // Act
                var testObject = new DivisionViewModel(null, subDbContext, null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("division", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void DivisionViewModelTest_NoArgsNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            var subDivision = new Division();

            ITreeViewItemViewModel testObject = null;
            try
            {
                // Act
                testObject = new DivisionViewModel(null, subDbContext, subDivision);
            }
            catch
            {
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(ITreeViewItemViewModel));
        }

        [TestMethod]
        public void ShowStandingsTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subParent = Substitute.For<ITreeViewItemViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetDivisionSeasonStandings();

            var subDivision = new Division();
            var testObject = new DivisionViewModel(subParent, subDbContext, subDivision);

            var subAssociationStandingsResult = new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();

            // Act
            testObject.ShowStandings(subAssociationStandingsResult);

            // Assert
            Assert.AreSame(subAssociationStandingsResult, testObject.Parent.StandingsResult);
        }

        [TestMethod]
        public void ShowStandingsTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subParent = Substitute.For<ITreeViewItemViewModel>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetDivisionSeasonStandings();

            var ex = new Exception();
            subDbContext.usp_GetDivisionSeasonStandings(Arg.Any<string>(), Arg.Any<int>()).Returns(x => { throw ex; });

            var subDivision = new Division();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new DivisionViewModel(subParent, subDbContext, subDivision, subGlobals);

            var subAssociationStandingsResult = new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();

            // Act
            testObject.ShowStandings(subAssociationStandingsResult);

            // Assert
            Assert.IsNull(testObject.Parent.StandingsResult);
            subGlobals.Received(1).ShowExceptionMessage(ex.InnerException);
        }
    }
}