﻿using System;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class TeamsControlViewModelTests
    {
        [TestMethod]
        public void TeamsControlViewModelTest_DbContextArgIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new TeamsControlViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void TeamsControlViewModelTest_DbContextArgIsNotNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            ITeamsControlViewModel testObject = null;
            try
            {
                // Act
                testObject = new TeamsControlViewModel(subDbContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            Assert.IsInstanceOfType(testObject, typeof(ITeamsControlViewModel));
        }

        [TestMethod]
        public void ViewTeamsCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubLeagueSeasons();
            subDbContext.SubTeamSeasons();
            SharedGlobals.SelectedSeason = 2016;

            var testObject = new TeamsControlViewModel(subDbContext);

            // Act
            testObject.ViewTeamsCommand.Execute(null);

            // Assert
            Assert.AreEqual(2, testObject.Teams.Count);
            foreach (var team in testObject.Teams)
            {
                Assert.AreEqual(SharedGlobals.SelectedSeason, team.SeasonID);
            }
        }

        [TestMethod]
        public void ViewTeamsCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubTeamSeasons();

            var ex = new Exception();
            subDbContext.LeagueSeasons.Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new TeamsControlViewModel(subDbContext, globals: subGlobals);

            // Act
            testObject.ViewTeamsCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }

        [TestMethod]
        public void ViewTeamScheduleCommandTest_SelectedTeamIsNull_ClearsAllTeamScheduleTables()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTeamSeasonScheduleProfile();
            subDbContext.SubGetTeamSeasonScheduleTotals();
            subDbContext.SubGetTeamSeasonScheduleAverages();

            var testObject = new TeamsControlViewModel(subDbContext);

            // Act
            testObject.ViewTeamScheduleCommand.Execute(null);

            // Assert
            Assert.IsNull(testObject.TeamScheduleProfileResult);
            Assert.IsNull(testObject.TeamScheduleTotalsResult);
            Assert.IsNull(testObject.TeamScheduleAveragesResult);
        }

        [TestMethod]
        public void ViewTeamScheduleCommandTest_SelectedTeamIsNotNull_ExecutesSuccessfully()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTeamSeasonScheduleProfile();
            subDbContext.SubGetTeamSeasonScheduleTotals();
            subDbContext.SubGetTeamSeasonScheduleAverages();

            var testObject = new TeamsControlViewModel(subDbContext);
            var season = 2016;
            SharedGlobals.SelectedSeason = season;
            testObject.SelectedTeam = new TeamSeason { TeamName = "Team1", SeasonID = season };

            // Act
            testObject.ViewTeamScheduleCommand.Execute(null);

            // Assert
            var selectedTeam = testObject.SelectedTeam.TeamName;
            var selectedSeason = SharedGlobals.SelectedSeason;

            Assert.AreEqual(subDbContext.usp_GetTeamSeasonScheduleProfile(selectedTeam, selectedSeason), testObject.TeamScheduleProfileResult);
            Assert.AreEqual(subDbContext.usp_GetTeamSeasonScheduleTotals(selectedTeam, selectedSeason), testObject.TeamScheduleTotalsResult);
            Assert.AreEqual(subDbContext.usp_GetTeamSeasonScheduleAverages(selectedTeam, selectedSeason), testObject.TeamScheduleAveragesResult);
        }

        [TestMethod]
        public void ViewTeamScheduleCommandTest_SelectedTeamIsNotNullAndExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            var ex = new Exception();
            subDbContext.usp_GetTeamSeasonScheduleProfile(Arg.Any<string>(), Arg.Any<int>()).Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new TeamsControlViewModel(subDbContext, subGlobals);
            testObject.SelectedTeam = new TeamSeason { TeamName = "Team1", SeasonID = 2016 };

            // Act
            testObject.ViewTeamScheduleCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}