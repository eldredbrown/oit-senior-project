﻿using System;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Tests
{
    [TestClass]
    public class RankingsControlViewModelTests
    {
        [TestMethod]
        public void RankingsControlViewModelTest_DbContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new RankingsControlViewModel(null);
            }
            catch (ArgumentNullException ex)
            {
                Assert.AreEqual("dbContext", ex.ParamName);
                return;
            }

            // Assert
            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void RankingsControlViewModelTest_DbContextArgNotNull_ConstructsObject()
        {
            // Arrange
            var subDbContext = Substitute.For<FootballDbEntities>();

            IRankingsControlViewModel testObject = null;
            try
            {
                // Act
                testObject = new RankingsControlViewModel(subDbContext);
            }
            catch (ArgumentNullException)
            {
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IRankingsControlViewModel));
        }

        [TestMethod]
        public void ViewRankingsCommandTest_NoExceptionCaught_ExecutesSuccessfully()
        {
            // Arrange
            SharedGlobals.SelectedSeason = 2016;
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTotalRankings((int)SharedGlobals.SelectedSeason);
            subDbContext.SubGetOffensiveRankings((int)SharedGlobals.SelectedSeason);
            subDbContext.SubGetDefensiveRankings((int)SharedGlobals.SelectedSeason);

            var testObject = new RankingsControlViewModel(subDbContext);

            // Act
            testObject.ViewRankingsCommand.Execute(null);

            // Assert
            Assert.AreSame(subDbContext.usp_GetTotalRankings(SharedGlobals.SelectedSeason), testObject.TotalRankingsResult);
            Assert.AreSame(subDbContext.usp_GetOffensiveRankings(SharedGlobals.SelectedSeason), testObject.OffensiveRankingsResult);
            Assert.AreSame(subDbContext.usp_GetDefensiveRankings(SharedGlobals.SelectedSeason), testObject.DefensiveRankingsResult);
        }

        [TestMethod]
        public void ViewRankingsCommandTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            SharedGlobals.SelectedSeason = 2016;
            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGetTotalRankings((int)SharedGlobals.SelectedSeason);
            subDbContext.SubGetOffensiveRankings((int)SharedGlobals.SelectedSeason);
            subDbContext.SubGetDefensiveRankings((int)SharedGlobals.SelectedSeason);

            var ex = new Exception();
            subDbContext.usp_GetTotalRankings(Arg.Any<int>()).Returns(x => { throw ex; });

            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new RankingsControlViewModel(subDbContext, subGlobals);

            // Act
            testObject.ViewRankingsCommand.Execute(null);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}