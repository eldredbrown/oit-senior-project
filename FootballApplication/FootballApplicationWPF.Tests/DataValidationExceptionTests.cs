﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EldredBrown.FootballApplicationWPF.Tests
{
    [TestClass]
    public class DataValidationExceptionTests
    {
        [TestMethod]
        public void DataValidationExceptionTest()
        {
            // Arrange
            var message = "message";

            // Act
            var ex = new DataValidationException(message);

            // Assert
            Assert.IsInstanceOfType(ex, typeof(DataValidationException));
            Assert.AreEqual(message, ex.Message);
        }

        [TestMethod]
        public void DataValidationExceptionTest1()
        {
            // Arrange
            var message = "message";
            var innerException = new Exception();

            // Act
            var ex = new DataValidationException(message, innerException);

            // Assert
            Assert.IsInstanceOfType(ex, typeof(DataValidationException));
            Assert.AreEqual(message, ex.Message);
            Assert.AreEqual(innerException, ex.InnerException);
        }

        [TestMethod]
        public void DataValidationExceptionTest2()
        {
            //// Arrange
            //var info = new SerializationInfo(typeof(object), new FormatterConverter());
            //var context = new StreamingContext();

            //// Act
            //var ex = new DataValidationException(info, context);

            //// Assert
            //Assert.IsInstanceOfType(ex, typeof(DataValidationException));
        }
    }
}