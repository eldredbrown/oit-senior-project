﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.Tests
{
    [TestClass]
    public class GlobalsTests
    {
        [TestMethod]
        public void GlobalsTest()
        {
            // Arrange
            // Act
            var testObject = new WpfGlobals();

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IGlobals));
        }

        [TestMethod]
        public void ShowExceptionMessageTest()
        {
            // Arrange
            var testObject = Substitute.ForPartsOf<WpfGlobals>();

            var ex = new DataValidationException("DVE");
            testObject.When(x => { x.ShowExceptionMessage(ex as Exception); }).Do(x => { });

            // Act
            testObject.ShowExceptionMessage(ex);

            // Assert
            testObject.Received().ShowExceptionMessage(ex, "DataValidationException");
        }

        [TestMethod]
        public void ShowExceptionMessageTest1()
        {
            //// Arrange
            //var ex = new Exception("Ex");

            //var testObject = new Globals();

            //// Act
            //testObject.ShowExceptionMessage(ex);

            //// Assert
            //// Cannot mock the System.Windows.MessageBox class
        }
    }
}