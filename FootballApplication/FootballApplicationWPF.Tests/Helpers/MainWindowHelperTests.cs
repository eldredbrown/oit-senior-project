﻿using System;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.UserControls;
using EldredBrown.FootballApplicationWPF.Windows;
using FootballApplicationWPFTests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace EldredBrown.FootballApplicationWPF.Helpers.Tests
{
    [TestClass]
    public class MainWindowHelperTests
    {
        [TestMethod]
        public void MainWindowHelperTest_ContextArgNull_ThrowsArgumentNullException()
        {
            // Arrange
            try
            {
                // Act
                var testObject = new MainWindowHelper(null);
            }
            catch (ArgumentNullException ex)
            {
                // Assert
                Assert.AreEqual("context", ex.ParamName);
                return;
            }

            Assert.Fail("Expected ArgumentNullException not thrown");
        }

        [TestMethod]
        public void MainWindowHelperTest_ContextArgIsNotNull_ConstructsObject()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindow>();

            IMainWindowHelper testObject = null;
            try
            {
                // Act
                testObject = new MainWindowHelper(subContext);
            }
            catch (ArgumentNullException)
            {
                // Assert
                Assert.Fail("Unexpected ArgumentNullException caught");
            }

            // Assert
            Assert.IsInstanceOfType(testObject, typeof(IMainWindowHelper));
        }

        [TestMethod]
        public void ShowGamesTest_NoExceptionCaught_MethodExecutesSuccessfully()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindow>();
            var testObject = new MainWindowHelper(subContext);

            var subGamesWindow = Substitute.For<IGamesWindow>();

            var subDbContext = Substitute.For<FootballDbEntities>();
            subDbContext.SubGames();

            var subTeamsControl = Substitute.For<ITeamsControl>();

            // Act
            testObject.ShowGames(subGamesWindow, subDbContext, subTeamsControl);

            // Assert
            subGamesWindow.Received(1).ShowDialog();
            //subDbContext.Games.Received(1).Load();
            subTeamsControl.Received(1).Refresh();
        }

        [TestMethod]
        public void ShowGamesTest_ExceptionCaught_ShowsExceptionMessage()
        {
            // Arrange
            var subContext = Substitute.For<IMainWindow>();
            var subGlobals = Substitute.For<IGlobals>();
            var testObject = new MainWindowHelper(subContext, subGlobals);

            var subGamesWindow = Substitute.For<IGamesWindow>();

            var ex = new Exception();
            subGamesWindow.ShowDialog().Returns(x => { throw ex; });

            var subDbContext = Substitute.For<FootballDbEntities>();
            var subTeamsControl = Substitute.For<ITeamsControl>();

            // Act
            testObject.ShowGames(subGamesWindow, subDbContext, subTeamsControl);

            // Assert
            subGlobals.Received(1).ShowExceptionMessage(ex);
        }
    }
}