
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/02/2017 21:09:46
-- Generated from EDMX file: D:\Documents\BitBucket\CST 4X2 - SeniorProject\FootballApplication\FootballApplicationWPF\DataModel\FootballDataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FootballDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_TeamGameLost]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_TeamGameLost];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamGameWon]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_TeamGameWon];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamGameHostOf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_TeamGameHostOf];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamGameGuestOf]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_TeamGameGuestOf];
GO
IF OBJECT_ID(N'[dbo].[FK_SeasonGame]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Games] DROP CONSTRAINT [FK_SeasonGame];
GO
IF OBJECT_ID(N'[dbo].[FK_DivisionTeamSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamSeasons] DROP CONSTRAINT [FK_DivisionTeamSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_ConferenceTeamSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamSeasons] DROP CONSTRAINT [FK_ConferenceTeamSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueTeamSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamSeasons] DROP CONSTRAINT [FK_LeagueTeamSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamSeasonSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamSeasons] DROP CONSTRAINT [FK_TeamSeasonSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamTeamSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TeamSeasons] DROP CONSTRAINT [FK_TeamTeamSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueSeasonSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LeagueSeasons] DROP CONSTRAINT [FK_LeagueSeasonSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueLeagueSeason]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LeagueSeasons] DROP CONSTRAINT [FK_LeagueLeagueSeason];
GO
IF OBJECT_ID(N'[dbo].[FK_ConferenceDivision]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Divisions] DROP CONSTRAINT [FK_ConferenceDivision];
GO
IF OBJECT_ID(N'[dbo].[FK_DivisionSeasonLast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Divisions] DROP CONSTRAINT [FK_DivisionSeasonLast];
GO
IF OBJECT_ID(N'[dbo].[FK_DivisionSeasonFirst]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Divisions] DROP CONSTRAINT [FK_DivisionSeasonFirst];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueConference]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Conferences] DROP CONSTRAINT [FK_LeagueConference];
GO
IF OBJECT_ID(N'[dbo].[FK_ConferenceSeasonLast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Conferences] DROP CONSTRAINT [FK_ConferenceSeasonLast];
GO
IF OBJECT_ID(N'[dbo].[FK_ConferenceSeasonFirst]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Conferences] DROP CONSTRAINT [FK_ConferenceSeasonFirst];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueSeasonLast]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Leagues] DROP CONSTRAINT [FK_LeagueSeasonLast];
GO
IF OBJECT_ID(N'[dbo].[FK_LeagueSeasonFirst]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Leagues] DROP CONSTRAINT [FK_LeagueSeasonFirst];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[WeekCounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WeekCounts];
GO
IF OBJECT_ID(N'[dbo].[Games]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Games];
GO
IF OBJECT_ID(N'[dbo].[TeamSeasons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TeamSeasons];
GO
IF OBJECT_ID(N'[dbo].[LeagueSeasons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LeagueSeasons];
GO
IF OBJECT_ID(N'[dbo].[Teams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Teams];
GO
IF OBJECT_ID(N'[dbo].[Divisions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Divisions];
GO
IF OBJECT_ID(N'[dbo].[Conferences]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Conferences];
GO
IF OBJECT_ID(N'[dbo].[Leagues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Leagues];
GO
IF OBJECT_ID(N'[dbo].[Seasons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Seasons];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Seasons'
CREATE TABLE [dbo].[Seasons] (
    [ID] int  NOT NULL
);
GO

-- Creating table 'Leagues'
CREATE TABLE [dbo].[Leagues] (
    [Name] nvarchar(4)  NOT NULL,
    [LongName] nvarchar(25)  NOT NULL,
	[FirstSeasonID] int  NOT NULL,
	[LastSeasonID] int  NULL
);
GO

-- Creating table 'Conferences'
CREATE TABLE [dbo].[Conferences] (
    [Name] nchar(3)  NOT NULL,
    [LongName] nvarchar(30)  NOT NULL,
    [LeagueName] nvarchar(4)  NOT NULL,
	[FirstSeasonID] int  NOT NULL,
	[LastSeasonID] int  NULL
);
GO

-- Creating table 'Divisions'
CREATE TABLE [dbo].[Divisions] (
    [Name] nvarchar(25)  NOT NULL,
	[LeagueName] nvarchar(4)  NOT NULL,
    [ConferenceName] nchar(3)  NULL,
	[FirstSeasonID] int  NOT NULL,
	[LastSeasonID] int  NULL
);
GO

-- Creating table 'Teams'
CREATE TABLE [dbo].[Teams] (
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'LeagueSeasons'
CREATE TABLE [dbo].[LeagueSeasons] (
    [SeasonID] int  NOT NULL,
    [LeagueName] nvarchar(4)  NOT NULL,
    [TotalGames] int DEFAULT 0  NOT NULL,
    [TotalPoints] int DEFAULT 0  NOT NULL,
    [AveragePoints] float  NULL
);
GO

-- Creating table 'TeamSeasons'
CREATE TABLE [dbo].[TeamSeasons] (
    [SeasonID] int  NOT NULL,
    [TeamName] nvarchar(50)  NOT NULL,
    [LeagueName] nvarchar(4)  NOT NULL,
    [ConferenceName] nchar(3)  NULL,
    [DivisionName] nvarchar(25)  NULL,
    [Games] int DEFAULT 0  NOT NULL,
    [Wins] int DEFAULT 0  NOT NULL,
    [Losses] int DEFAULT 0  NOT NULL,
    [Ties] int DEFAULT 0  NOT NULL,
    [WinningPercentage] float  NULL,
    [PointsFor] int DEFAULT 0  NOT NULL,
    [PointsAgainst] int DEFAULT 0  NOT NULL,
    [PythagoreanWins] float DEFAULT 0  NOT NULL,
    [PythagoreanLosses] float DEFAULT 0  NOT NULL,
    [OffensiveAverage] float  NULL,
    [OffensiveFactor] float  NULL,
    [OffensiveIndex] float  NULL,
    [DefensiveAverage] float  NULL,
    [DefensiveFactor] float  NULL,
    [DefensiveIndex] float  NULL,
    [FinalPythagoreanWinningPercentage] float  NULL
);
GO

-- Creating table 'Games'
CREATE TABLE [dbo].[Games] (
    [ID] int IDENTITY(1,1)  NOT NULL,
    [SeasonID] int  NOT NULL,
    [Week] int  NOT NULL,
    [GuestName] nvarchar(50)  NOT NULL,
    [GuestScore] int  NOT NULL,
    [HostName] nvarchar(50)  NOT NULL,
    [HostScore] int  NOT NULL,
    [WinnerName] nvarchar(50)  NULL,
    [LoserName] nvarchar(50)  NULL,
    [IsPlayoffGame] bit  NOT NULL,
    [Notes] nvarchar(max)  NULL
);
GO

-- Creating table 'WeekCounts'
CREATE TABLE [dbo].[WeekCounts] (
	[ID] int IDENTITY(1,1)  NOT NULL,
    [Count] int DEFAULT 0  NOT NULL
);
GO
-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Seasons'
ALTER TABLE [dbo].[Seasons]
ADD CONSTRAINT [PK_Seasons]
    PRIMARY KEY CLUSTERED ([ID] DESC);
GO

-- Creating primary key on [LongName] in table 'Leagues'
ALTER TABLE [dbo].[Leagues]
ADD CONSTRAINT [PK_Leagues]
    PRIMARY KEY CLUSTERED ([Name] ASC);
GO

-- Creating primary key on [LongName] in table 'Conferences'
ALTER TABLE [dbo].[Conferences]
ADD CONSTRAINT [PK_Conferences]
    PRIMARY KEY CLUSTERED ([Name] ASC);
GO

-- Creating primary key on [Name] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [PK_Divisions]
    PRIMARY KEY CLUSTERED ([Name] ASC);
GO

-- Creating primary key on [Name] in table 'Teams'
ALTER TABLE [dbo].[Teams]
ADD CONSTRAINT [PK_Teams]
    PRIMARY KEY CLUSTERED ([Name] ASC);
GO

-- Creating primary key on [LeagueName], [SeasonID] in table 'LeagueSeasons'
ALTER TABLE [dbo].[LeagueSeasons]
ADD CONSTRAINT [PK_LeagueSeasons]
    PRIMARY KEY CLUSTERED ([SeasonID] DESC, [LeagueName] ASC);
GO

-- Creating primary key on [TeamName], [SeasonID] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [PK_TeamSeasons]
    PRIMARY KEY CLUSTERED ([SeasonID] DESC, [TeamName] ASC);
GO

-- Creating primary key on [ID] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [PK_Games]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [WeekCount] in table 'WeekCounts'
ALTER TABLE [dbo].[WeekCounts]
ADD CONSTRAINT [PK_WeekCounts]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [FirstSeasonID] in table 'Leagues'
ALTER TABLE [dbo].[Leagues]
ADD CONSTRAINT [FK_LeagueSeasonFirst]
    FOREIGN KEY ([FirstSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueSeasonFirst'
CREATE INDEX [IX_FK_LeagueSeasonFirst]
ON [dbo].[Leagues]
    ([FirstSeasonID]);
GO

-- Creating foreign key on [LastSeason] in table 'Leagues'
ALTER TABLE [dbo].[Leagues]
ADD CONSTRAINT [FK_LeagueSeasonLast]
    FOREIGN KEY ([LastSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueSeasonLast'
CREATE INDEX [IX_FK_LeagueSeasonLast]
ON [dbo].[Leagues]
    ([LastSeasonID]);
GO

-- Creating foreign key on [LeagueName] in table 'Conferences'
ALTER TABLE [dbo].[Conferences]
ADD CONSTRAINT [FK_LeagueConference]
    FOREIGN KEY ([LeagueName])
    REFERENCES [dbo].[Leagues]
        ([Name])
	ON UPDATE CASCADE
	ON DELETE CASCADE;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueConference'
CREATE INDEX [IX_FK_LeagueConference]
ON [dbo].[Conferences]
    ([LeagueName]);
GO

-- Creating foreign key on [FirstSeasonID] in table 'Conferences'
ALTER TABLE [dbo].[Conferences]
ADD CONSTRAINT [FK_ConferenceSeasonFirst]
    FOREIGN KEY ([FirstSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConferenceSeasonFirst'
CREATE INDEX [IX_FK_ConferenceSeasonFirst]
ON [dbo].[Conferences]
    ([FirstSeasonID]);
GO

-- Creating foreign key on [LastSeason] in table 'Conferences'
ALTER TABLE [dbo].[Conferences]
ADD CONSTRAINT [FK_ConferenceSeasonLast]
    FOREIGN KEY ([LastSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConferenceSeasonLast'
CREATE INDEX [IX_FK_ConferenceSeasonLast]
ON [dbo].[Conferences]
    ([LastSeasonID]);
GO

-- Creating foreign key on [LeagueName] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [FK_LeagueDivision]
    FOREIGN KEY ([LeagueName])
    REFERENCES [dbo].[Leagues]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueDivision'
CREATE INDEX [IX_FK_LeagueDivision]
ON [dbo].[Divisions]
    ([LeagueName]);
GO

-- Creating foreign key on [ConferenceName] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [FK_ConferenceDivision]
    FOREIGN KEY ([ConferenceName])
    REFERENCES [dbo].[Conferences]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConferenceDivision'
CREATE INDEX [IX_FK_ConferenceDivision]
ON [dbo].[Divisions]
    ([ConferenceName]);
GO

-- Creating foreign key on [FirstSeasonID] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [FK_DivisionSeasonFirst]
    FOREIGN KEY ([FirstSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DivisionSeasonFirst'
CREATE INDEX [IX_FK_DivisionSeasonFirst]
ON [dbo].[Divisions]
    ([FirstSeasonID]);
GO

-- Creating foreign key on [LastSeason] in table 'Divisions'
ALTER TABLE [dbo].[Divisions]
ADD CONSTRAINT [FK_DivisionSeasonLast]
    FOREIGN KEY ([LastSeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DivisionSeasonLast'
CREATE INDEX [IX_FK_DivisionSeasonLast]
ON [dbo].[Divisions]
    ([LastSeasonID]);
GO

-- Creating foreign key on [LeagueName] in table 'LeagueSeasons'
ALTER TABLE [dbo].[LeagueSeasons]
ADD CONSTRAINT [FK_LeagueLeagueSeason]
    FOREIGN KEY ([LeagueName])
    REFERENCES [dbo].[Leagues]
        ([Name])
	ON UPDATE CASCADE
	ON DELETE CASCADE;
GO

-- Creating foreign key on [SeasonID] in table 'LeagueSeasons'
ALTER TABLE [dbo].[LeagueSeasons]
ADD CONSTRAINT [FK_LeagueSeasonSeason]
    FOREIGN KEY ([SeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
	ON UPDATE CASCADE
	ON DELETE CASCADE;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueSeasonSeason'
CREATE INDEX [IX_FK_LeagueSeasonSeason]
ON [dbo].[LeagueSeasons]
    ([SeasonID]);
GO

-- Creating foreign key on [TeamName] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [FK_TeamTeamSeason]
    FOREIGN KEY ([TeamName])
    REFERENCES [dbo].[Teams]
        ([Name])
	ON UPDATE CASCADE
	ON DELETE CASCADE;
GO

-- Creating foreign key on [SeasonID] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [FK_SeasonTeamSeason]
    FOREIGN KEY ([SeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
	ON UPDATE CASCADE
	ON DELETE CASCADE;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamSeasonSeason'
CREATE INDEX [IX_FK_SeasonTeamSeason]
ON [dbo].[TeamSeasons]
    ([SeasonID]);
GO

-- Creating foreign key on [LeagueName] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [FK_LeagueTeamSeason]
    FOREIGN KEY ([LeagueName])
    REFERENCES [dbo].[Leagues]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LeagueTeamSeason'
CREATE INDEX [IX_FK_LeagueTeamSeason]
ON [dbo].[TeamSeasons]
    ([LeagueName]);
GO

-- Creating foreign key on [ConferenceName] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [FK_ConferenceTeamSeason]
    FOREIGN KEY ([ConferenceName])
    REFERENCES [dbo].[Conferences]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ConferenceTeamSeason'
CREATE INDEX [IX_FK_ConferenceTeamSeason]
ON [dbo].[TeamSeasons]
    ([ConferenceName]);
GO

-- Creating foreign key on [DivisionName] in table 'TeamSeasons'
ALTER TABLE [dbo].[TeamSeasons]
ADD CONSTRAINT [FK_DivisionTeamSeason]
    FOREIGN KEY ([DivisionName])
    REFERENCES [dbo].[Divisions]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DivisionTeamSeason'
CREATE INDEX [IX_FK_DivisionTeamSeason]
ON [dbo].[TeamSeasons]
    ([DivisionName]);
GO

-- Creating foreign key on [SeasonID] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_SeasonGame]
    FOREIGN KEY ([SeasonID])
    REFERENCES [dbo].[Seasons]
        ([ID])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SeasonGame'
CREATE INDEX [IX_FK_SeasonGame]
ON [dbo].[Games]
    ([SeasonID]);
GO

-- Creating foreign key on [GuestName] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_TeamGameGuestOf]
    FOREIGN KEY ([GuestName])
    REFERENCES [dbo].[Teams]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamGameGuestOf'
CREATE INDEX [IX_FK_TeamGameGuestOf]
ON [dbo].[Games]
    ([GuestName]);
GO

-- Creating foreign key on [HostName] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_TeamGameHostOf]
    FOREIGN KEY ([HostName])
    REFERENCES [dbo].[Teams]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamGameHostOf'
CREATE INDEX [IX_FK_TeamGameHostOf]
ON [dbo].[Games]
    ([HostName]);
GO

-- Creating foreign key on [WinnerName] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_TeamGameWon]
    FOREIGN KEY ([WinnerName])
    REFERENCES [dbo].[Teams]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamGameWon'
CREATE INDEX [IX_FK_TeamGameWon]
ON [dbo].[Games]
    ([WinnerName]);
GO

-- Creating foreign key on [LoserName] in table 'Games'
ALTER TABLE [dbo].[Games]
ADD CONSTRAINT [FK_TeamGameLost]
    FOREIGN KEY ([LoserName])
    REFERENCES [dbo].[Teams]
        ([Name])
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamGameLost'
CREATE INDEX [IX_FK_TeamGameLost]
ON [dbo].[Games]
    ([LoserName]);
GO

-- --------------------------------------------------
-- Populating all tables
-- --------------------------------------------------

-- Populating table 'Seasons'
INSERT INTO [dbo].Seasons VALUES (2016)
INSERT INTO [dbo].Seasons VALUES (2015)
INSERT INTO [dbo].Seasons VALUES (2014)
INSERT INTO [dbo].Seasons VALUES (2002)
INSERT INTO [dbo].Seasons VALUES (1970)
INSERT INTO [dbo].Seasons VALUES (1969)
INSERT INTO [dbo].Seasons VALUES (1960)
INSERT INTO [dbo].Seasons VALUES (1923)

-- Populating table 'Leagues'
INSERT INTO [dbo].Leagues VALUES ('NFL', 'National Football League', 1923, NULL)
INSERT INTO [dbo].Leagues VALUES ('AFL', 'American Football League', 1960, 1969)

-- Populating table 'Conferences'
INSERT INTO [dbo].Conferences VALUES ('AFC', 'American Football Conference', 'NFL', 1970, NULL)
INSERT INTO [dbo].Conferences VALUES ('NFC', 'National Football Conference', 'NFL', 1970, NULL)

-- Populating table 'Divisions'
INSERT INTO [dbo].Divisions VALUES ('AFC East', 'NFL', 'AFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('AFC North', 'NFL', 'AFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('AFC South', 'NFL', 'AFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('AFC West', 'NFL', 'AFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('NFC East', 'NFL', 'NFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('NFC North', 'NFL', 'NFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('NFC South', 'NFL', 'NFC', 2002, NULL)
INSERT INTO [dbo].Divisions VALUES ('NFC West', 'NFL', 'NFC', 2002, NULL)

-- Populating table 'Teams'
INSERT INTO [dbo].Teams VALUES ('Arizona Cardinals')
INSERT INTO [dbo].Teams VALUES ('Atlanta Falcons')
INSERT INTO [dbo].Teams VALUES ('Baltimore Ravens')
INSERT INTO [dbo].Teams VALUES ('Buffalo Bills')
INSERT INTO [dbo].Teams VALUES ('Carolina Panthers')
INSERT INTO [dbo].Teams VALUES ('Chicago Bears')
INSERT INTO [dbo].Teams VALUES ('Cincinnati Bengals')
INSERT INTO [dbo].Teams VALUES ('Cleveland Browns')
INSERT INTO [dbo].Teams VALUES ('Dallas Cowboys')
INSERT INTO [dbo].Teams VALUES ('Denver Broncos')
INSERT INTO [dbo].Teams VALUES ('Detroit Lions')
INSERT INTO [dbo].Teams VALUES ('Green Bay Packers')
INSERT INTO [dbo].Teams VALUES ('Houston Texans')
INSERT INTO [dbo].Teams VALUES ('Indianapolis Colts')
INSERT INTO [dbo].Teams VALUES ('Jacksonville Jaguars')
INSERT INTO [dbo].Teams VALUES ('Kansas City Chiefs')
INSERT INTO [dbo].Teams VALUES ('Los Angeles Rams')
INSERT INTO [dbo].Teams VALUES ('Miami Dolphins')
INSERT INTO [dbo].Teams VALUES ('Minnesota Vikings')
INSERT INTO [dbo].Teams VALUES ('New England Patriots')
INSERT INTO [dbo].Teams VALUES ('New Orleans Saints')
INSERT INTO [dbo].Teams VALUES ('New York Giants')
INSERT INTO [dbo].Teams VALUES ('New York Jets')
INSERT INTO [dbo].Teams VALUES ('Oakland Raiders')
INSERT INTO [dbo].Teams VALUES ('Philadelphia Eagles')
INSERT INTO [dbo].Teams VALUES ('Pittsburgh Steelers')
INSERT INTO [dbo].Teams VALUES ('San Diego Chargers')
INSERT INTO [dbo].Teams VALUES ('San Francisco 49ers')
INSERT INTO [dbo].Teams VALUES ('Seattle Seahawks')
INSERT INTO [dbo].Teams VALUES ('St. Louis Rams')
INSERT INTO [dbo].Teams VALUES ('Tampa Bay Buccaneers')
INSERT INTO [dbo].Teams VALUES ('Tennessee Titans')
INSERT INTO [dbo].Teams VALUES ('Washington Redskins')

-- Populating table 'LeagueSeasons'
INSERT INTO [dbo].LeagueSeasons (SeasonID, LeagueName) VALUES (2016, 'NFL')
INSERT INTO [dbo].LeagueSeasons (SeasonID, LeagueName) VALUES (2015, 'NFL')
INSERT INTO [dbo].LeagueSeasons (SeasonID, LeagueName) VALUES (2014, 'NFL')

-- Populating table 'TeamSeasons'
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Arizona Cardinals', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Atlanta Falcons', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Baltimore Ravens', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Buffalo Bills', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Carolina Panthers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Chicago Bears', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Cincinnati Bengals', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Cleveland Browns', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Dallas Cowboys', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Denver Broncos', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Detroit Lions', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Green Bay Packers', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Houston Texans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Indianapolis Colts', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Jacksonville Jaguars', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Kansas City Chiefs', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Los Angeles Rams', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Miami Dolphins', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Minnesota Vikings', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'New England Patriots', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'New Orleans Saints', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'New York Giants', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'New York Jets', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Oakland Raiders', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Philadelphia Eagles', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Pittsburgh Steelers', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'San Diego Chargers', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'San Francisco 49ers', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Seattle Seahawks', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Tampa Bay Buccaneers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Tennessee Titans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2016, 'Washington Redskins', 'NFL', 'NFC', 'NFC East')

INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Arizona Cardinals', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Atlanta Falcons', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Baltimore Ravens', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Buffalo Bills', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Carolina Panthers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Chicago Bears', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Cincinnati Bengals', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Cleveland Browns', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Dallas Cowboys', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Denver Broncos', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Detroit Lions', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Green Bay Packers', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Houston Texans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Indianapolis Colts', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Jacksonville Jaguars', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Kansas City Chiefs', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Miami Dolphins', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Minnesota Vikings', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'New England Patriots', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'New Orleans Saints', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'New York Giants', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'New York Jets', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Oakland Raiders', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Philadelphia Eagles', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Pittsburgh Steelers', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'San Diego Chargers', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'San Francisco 49ers', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Seattle Seahawks', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'St. Louis Rams', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Tampa Bay Buccaneers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Tennessee Titans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2015, 'Washington Redskins', 'NFL', 'NFC', 'NFC East')

INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Arizona Cardinals', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Atlanta Falcons', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Baltimore Ravens', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Buffalo Bills', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Carolina Panthers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Chicago Bears', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Cincinnati Bengals', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Cleveland Browns', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Dallas Cowboys', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Denver Broncos', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Detroit Lions', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Green Bay Packers', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Houston Texans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Indianapolis Colts', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Jacksonville Jaguars', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Kansas City Chiefs', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Miami Dolphins', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Minnesota Vikings', 'NFL', 'NFC', 'NFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'New England Patriots', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'New Orleans Saints', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'New York Giants', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'New York Jets', 'NFL', 'AFC', 'AFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Oakland Raiders', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Philadelphia Eagles', 'NFL', 'NFC', 'NFC East')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Pittsburgh Steelers', 'NFL', 'AFC', 'AFC North')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'San Diego Chargers', 'NFL', 'AFC', 'AFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'San Francisco 49ers', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Seattle Seahawks', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'St. Louis Rams', 'NFL', 'NFC', 'NFC West')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Tampa Bay Buccaneers', 'NFL', 'NFC', 'NFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Tennessee Titans', 'NFL', 'AFC', 'AFC South')
INSERT INTO [dbo].TeamSeasons (SeasonID, TeamName, LeagueName, ConferenceName, DivisionName)
	VALUES (2014, 'Washington Redskins', 'NFL', 'NFC', 'NFC East')

INSERT INTO [dbo].WeekCounts VALUES (1)

-- --------------------------------------------------
-- Verifying all tables
-- --------------------------------------------------

SELECT * FROM [dbo].Seasons ORDER BY [ID] DESC
SELECT * FROM [dbo].Leagues ORDER BY [Name] ASC
SELECT * FROM [dbo].Conferences ORDER BY [Name] ASC
SELECT * FROM [dbo].Divisions ORDER BY [Name] ASC
SELECT * FROM [dbo].Teams ORDER BY [Name] ASC
SELECT * FROM [dbo].LeagueSeasons ORDER BY [SeasonID] DESC, [LeagueName] ASC
SELECT * FROM [dbo].TeamSeasons ORDER BY [SeasonID] DESC, [TeamName] ASC
SELECT * FROM [dbo].Games ORDER BY [SeasonID] DESC
SELECT * FROM [dbo].WeekCounts

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------