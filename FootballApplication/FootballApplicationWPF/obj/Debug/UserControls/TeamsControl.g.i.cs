﻿#pragma checksum "..\..\..\UserControls\TeamsControl.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5852F24A218FF5330658103CA44F71A7E4AE8D33"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using EldredBrown.FootballApplicationWPF.UserControls;
using EldredBrown.FootballApplicationWPF.ViewModels;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace EldredBrown.FootballApplicationWPF.UserControls {
    
    
    /// <summary>
    /// TeamsControl
    /// </summary>
    public partial class TeamsControl : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TeamsGrid;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TeamsDataGrid;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamNameColumn;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn01;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn02;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn03;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamWinsColumn;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamLossesColumn;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamTiesColumn;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamWinningPercentageColumn;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn04;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamPointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamPointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn05;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamSpacerColumn06;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamPythagoreanWinsColumn;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamPythagoreanLossesColumn;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TeamScheduleProfileDataGrid;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentColumn;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileSpacerColumn01;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileGamePointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileGamePointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileSpacerColumn02;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileSpacerColumn03;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileSpacerColumn04;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentWinsColumn;
        
        #line default
        #line hidden
        
        
        #line 146 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentLossesColumn;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentTiesColumn;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentWinningPercentageColumn;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileSpacerColumn05;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentWeightedGamesColumn;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentWeightedPointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleProfileOpponentWeightedPointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TeamScheduleTotalsDataGrid;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsRowHeaderColumn;
        
        #line default
        #line hidden
        
        
        #line 196 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsGamesColumn;
        
        #line default
        #line hidden
        
        
        #line 200 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSpacerColumn01;
        
        #line default
        #line hidden
        
        
        #line 203 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsPointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 207 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsPointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 211 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSpacerColumn02;
        
        #line default
        #line hidden
        
        
        #line 214 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSpacerColumn03;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSpacerColumn04;
        
        #line default
        #line hidden
        
        
        #line 220 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsScheduleWinsColumn;
        
        #line default
        #line hidden
        
        
        #line 224 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsScheduleLossesColumn;
        
        #line default
        #line hidden
        
        
        #line 228 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsScheduleTiesColumn;
        
        #line default
        #line hidden
        
        
        #line 232 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsScheduleWinningPercentageColumn;
        
        #line default
        #line hidden
        
        
        #line 236 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSpacerColumn05;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsScheduleGamesColumn;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSchedulePointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 247 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleTotalsSchedulePointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 253 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TeamScheduleAveragesDataGrid;
        
        #line default
        #line hidden
        
        
        #line 269 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesRowHeaderColumn;
        
        #line default
        #line hidden
        
        
        #line 274 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSpacerColumn01;
        
        #line default
        #line hidden
        
        
        #line 277 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesPointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 281 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesPointsAgainstColumn;
        
        #line default
        #line hidden
        
        
        #line 285 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSpacerColumn02;
        
        #line default
        #line hidden
        
        
        #line 288 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSpacerColumn03;
        
        #line default
        #line hidden
        
        
        #line 291 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSpacerColumn04;
        
        #line default
        #line hidden
        
        
        #line 294 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSpacerColumn05;
        
        #line default
        #line hidden
        
        
        #line 296 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSchedulePointsForColumn;
        
        #line default
        #line hidden
        
        
        #line 300 "..\..\..\UserControls\TeamsControl.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGridTextColumn TeamScheduleAveragesSchedulePointsAgainstColumn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/EldredBrown.FootballApplicationWPF;component/usercontrols/teamscontrol.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\UserControls\TeamsControl.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TeamsGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.TeamsDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 3:
            this.TeamNameColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 4:
            this.TeamSpacerColumn01 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 5:
            this.TeamSpacerColumn02 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 6:
            this.TeamSpacerColumn03 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 7:
            this.TeamWinsColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 8:
            this.TeamLossesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 9:
            this.TeamTiesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 10:
            this.TeamWinningPercentageColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 11:
            this.TeamSpacerColumn04 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 12:
            this.TeamPointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 13:
            this.TeamPointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 14:
            this.TeamSpacerColumn05 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 15:
            this.TeamSpacerColumn06 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 16:
            this.TeamPythagoreanWinsColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 17:
            this.TeamPythagoreanLossesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 18:
            this.TeamScheduleProfileDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 19:
            this.TeamScheduleProfileOpponentColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 20:
            this.TeamScheduleProfileSpacerColumn01 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 21:
            this.TeamScheduleProfileGamePointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 22:
            this.TeamScheduleProfileGamePointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 23:
            this.TeamScheduleProfileSpacerColumn02 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 24:
            this.TeamScheduleProfileSpacerColumn03 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 25:
            this.TeamScheduleProfileSpacerColumn04 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 26:
            this.TeamScheduleProfileOpponentWinsColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 27:
            this.TeamScheduleProfileOpponentLossesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 28:
            this.TeamScheduleProfileOpponentTiesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 29:
            this.TeamScheduleProfileOpponentWinningPercentageColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 30:
            this.TeamScheduleProfileSpacerColumn05 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 31:
            this.TeamScheduleProfileOpponentWeightedGamesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 32:
            this.TeamScheduleProfileOpponentWeightedPointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 33:
            this.TeamScheduleProfileOpponentWeightedPointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 34:
            this.TeamScheduleTotalsDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 35:
            this.TeamScheduleTotalsRowHeaderColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 36:
            this.TeamScheduleTotalsGamesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 37:
            this.TeamScheduleTotalsSpacerColumn01 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 38:
            this.TeamScheduleTotalsPointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 39:
            this.TeamScheduleTotalsPointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 40:
            this.TeamScheduleTotalsSpacerColumn02 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 41:
            this.TeamScheduleTotalsSpacerColumn03 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 42:
            this.TeamScheduleTotalsSpacerColumn04 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 43:
            this.TeamScheduleTotalsScheduleWinsColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 44:
            this.TeamScheduleTotalsScheduleLossesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 45:
            this.TeamScheduleTotalsScheduleTiesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 46:
            this.TeamScheduleTotalsScheduleWinningPercentageColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 47:
            this.TeamScheduleTotalsSpacerColumn05 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 48:
            this.TeamScheduleTotalsScheduleGamesColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 49:
            this.TeamScheduleTotalsSchedulePointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 50:
            this.TeamScheduleTotalsSchedulePointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 51:
            this.TeamScheduleAveragesDataGrid = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 52:
            this.TeamScheduleAveragesRowHeaderColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 53:
            this.TeamScheduleAveragesSpacerColumn01 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 54:
            this.TeamScheduleAveragesPointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 55:
            this.TeamScheduleAveragesPointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 56:
            this.TeamScheduleAveragesSpacerColumn02 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 57:
            this.TeamScheduleAveragesSpacerColumn03 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 58:
            this.TeamScheduleAveragesSpacerColumn04 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 59:
            this.TeamScheduleAveragesSpacerColumn05 = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 60:
            this.TeamScheduleAveragesSchedulePointsForColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            case 61:
            this.TeamScheduleAveragesSchedulePointsAgainstColumn = ((System.Windows.Controls.DataGridTextColumn)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

