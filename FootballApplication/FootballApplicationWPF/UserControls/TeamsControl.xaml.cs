﻿using System.Windows.Controls;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.UserControls
{
    public interface ITeamsControl
    {
        void Refresh();
    }

    /// <summary>
    /// Interaction logic for TeamsControl.xaml
    /// </summary>
    public partial class TeamsControl : UserControl, ITeamsControl
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the TeamsControl class
        /// </summary>
        public TeamsControl()
		{
            InitializeComponent();

            DataContext = new TeamsControlViewModel(DataAccess.DbContext);
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Refreshes the view of this TeamsControl object
        /// </summary>
        public void Refresh()
        {
            (DataContext as ITeamsControlViewModel).ViewTeamsCommand.Execute(null);
        }

        #endregion Methods
    }
}
