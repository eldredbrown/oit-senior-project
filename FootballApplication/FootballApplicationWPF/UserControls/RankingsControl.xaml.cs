﻿using System.Windows.Controls;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.UserControls
{
    public interface IRankingsControl
    {
        void Refresh();
    }

    /// <summary>
    /// Interaction logic for RankingsControl.xaml
    /// </summary>
    public partial class RankingsControl : UserControl, IRankingsControl
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the RankingsControl class
        /// </summary>
        public RankingsControl()
		{
			InitializeComponent();

            DataContext = new RankingsControlViewModel(DataAccess.DbContext);
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Refreshes the view of this RankingsControl object
        /// </summary>
        public void Refresh()
        {
            (DataContext as IRankingsControlViewModel).ViewRankingsCommand.Execute(null);
        }

        #endregion Methods
    }
}
