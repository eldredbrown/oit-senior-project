﻿using System.Windows.Controls;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.UserControls
{
    public interface IStandingsControl
    {
        void Refresh();
    }

    /// <summary>
    /// Interaction logic for StandingsControl.xaml
    /// </summary>
    public partial class StandingsControl : UserControl, IStandingsControl
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the StandingsControl class
        /// </summary>
        public StandingsControl()
		{
			InitializeComponent();

            DataContext = new StandingsControlViewModel(DataAccess.DbContext);
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Refreshes the view of this StandingsControl object
        /// </summary>
        public void Refresh()
        {
            (DataContext as IStandingsControlViewModel).ViewStandingsCommand.Execute(null);
        }

        #endregion Methods
    }
}
