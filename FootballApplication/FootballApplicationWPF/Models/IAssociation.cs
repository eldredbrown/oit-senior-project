﻿using System;
using EldredBrown.FootballApplicationShared;

namespace EldredBrown.FootballApplicationWPF.Models
{
    public interface IAssociation
    {
        string Name { get; set; }
        int FirstSeasonID { get; set; }
        Nullable<int> LastSeasonID { get; set; }
        Season FirstSeason { get; set; }
        Season LastSeason { get; set; }

        bool AssociationExists();
    }

    public partial class League : IAssociation
    {
        public virtual bool AssociationExists()
        {
            return (FirstSeasonID <= SharedGlobals.SelectedSeason) &&
                ((LastSeason == null) || (LastSeasonID >= SharedGlobals.SelectedSeason));
        }
    }

    public partial class Conference : IAssociation
    {
        public virtual bool AssociationExists()
        {
            return (FirstSeasonID <= SharedGlobals.SelectedSeason) &&
                ((LastSeason == null) || (LastSeasonID >= SharedGlobals.SelectedSeason));
        }
    }

    public partial class Division : IAssociation
    {
        public virtual bool AssociationExists()
        {
            return (FirstSeasonID <= SharedGlobals.SelectedSeason) &&
                ((LastSeason == null) || (LastSeasonID >= SharedGlobals.SelectedSeason));
        }
    }
}
