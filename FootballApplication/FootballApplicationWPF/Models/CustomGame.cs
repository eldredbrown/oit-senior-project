﻿using System;

namespace EldredBrown.FootballApplicationWPF.Models
{
    public interface IGame
    {
        int ID { get; set; }
        int SeasonID { get; set; }
        int Week { get; set; }
        string GuestName { get; set; }
        double GuestScore { get; set; }
        string HostName { get; set; }
        double HostScore { get; set; }
        string WinnerName { get; set; }
        Nullable<double> WinnerScore { get; set; }
        string LoserName { get; set; }
        Nullable<double> LoserScore { get; set; }
        bool IsPlayoffGame { get; set; }
        string Notes { get; set; }

        Season Season { get; set; }
        Team Guest { get; set; }
        Team Host { get; set; }
        Team Winner { get; set; }
        Team Loser { get; set; }

        void DecideWinnerAndLoser();
    }

    /// <summary>
    /// Extension of the DataModel's auto-generated Game class.
    /// </summary>
    public partial class Game : IGame
    {
        #region Fields

        private readonly IGlobals _globals;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the Game class.
        /// </summary>
        public Game()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Game class.
        /// </summary>
        public Game(IGlobals globals = null)
        {
            _globals = (globals != null) ? globals : new WpfGlobals();
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Decides this game's winner and loser.
        /// </summary>
        public virtual void DecideWinnerAndLoser()
        {
            try
            {
                // Declare the winner to be the team that scored more points in the game.
                if (GuestScore > HostScore)
                {
                    Winner = Guest;
                    WinnerScore = GuestScore;
                    Loser = Host;
                    LoserScore = HostScore;
                }
                else if (HostScore > GuestScore)
                {
                    Winner = Host;
                    WinnerScore = HostScore;
                    Loser = Guest;
                    LoserScore = GuestScore;
                }
                else
                {
                    Winner = null;
                    WinnerScore = 0;
                    Loser = null;
                    LoserScore = 0;
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Methods
    }
}
