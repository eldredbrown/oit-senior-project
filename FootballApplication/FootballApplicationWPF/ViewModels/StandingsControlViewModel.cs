﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IStandingsControlViewModel
    {
        ReadOnlyCollection<ITreeViewItemViewModel> Leagues { get; }
        ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult { get; set; }

        DelegateCommand ViewStandingsCommand { get; }
    }

    /// <summary>
    /// ViewModel logic for the Standings control.
    /// </summary>
    public class StandingsControlViewModel : ViewModelBase, IStandingsControlViewModel
    {
        #region Fields

        private ILeaguesTreeViewViewModel _leaguesTreeViewViewModel;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the StandingsControlViewModel class.
        /// </summary>
        public StandingsControlViewModel(FootballDbEntities dbContext, IGlobals globals = null)
            : base(dbContext, globals)
		{
            _leaguesTreeViewViewModel = null;
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the leagues collection for the TreeView control contained inside this control.
        /// </summary>
        private ReadOnlyCollection<ITreeViewItemViewModel> _Leagues;
        public ReadOnlyCollection<ITreeViewItemViewModel> Leagues
        {
            get
            {
                return _Leagues;
            }
            private set
            {
                if (value != _Leagues)
                {
                    _Leagues = value;
                    OnPropertyChanged("Leagues");
                }
            }
        }

        /// <summary>
        /// Gets or sets this control's standings collection.
        /// </summary>
        private ObservableCollection<usp_GetSeasonAssociationStandings_Result> _StandingsResult;
        public ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult
        {
            get
            {
                return _StandingsResult;
            }
            set
            {
                if (value != _StandingsResult)
                {
                    _StandingsResult = value;
                    OnPropertyChanged("StandingsResult");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Loads the control.
        /// </summary>
        private DelegateCommand _viewStandingsCommand;
        public DelegateCommand ViewStandingsCommand
        {
            get
            {
                if (_viewStandingsCommand == null)
                {
                    _viewStandingsCommand = new DelegateCommand(param => ViewStandings(param as ILeaguesTreeViewViewModel));
                }
                return _viewStandingsCommand;
            }
        }
        private void ViewStandings(ILeaguesTreeViewViewModel leaguesTreeViewViewModel)
        {
            try
            {
                StandingsResult = null;

                var leagues = (from league in DbContext.Leagues
                               orderby league.Name ascending
                               select league)
                               .AsEnumerable()
                               .Where(x => x.AssociationExists());

                _leaguesTreeViewViewModel = (leaguesTreeViewViewModel != null) ?
                    leaguesTreeViewViewModel
                    :
                    new LeaguesTreeViewViewModel(DbContext, this, leagues);
                Leagues = _leaguesTreeViewViewModel.Leagues;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands
    }
}
