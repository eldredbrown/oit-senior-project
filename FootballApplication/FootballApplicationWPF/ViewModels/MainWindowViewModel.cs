﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;
using EldredBrown.FootballApplicationWPF.Windows;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IMainWindowViewModel : IViewModelBase
    {
        List<int> Seasons { get; set; }
        int SelectedSeason { get; set; }

        DelegateCommand ViewSeasonsCommand { get; }
        DelegateCommand WeeklyUpdateCommand { get; }
    }

    /// <summary>
    /// ViewModel logic for the MainWindow class.
    /// </summary>
    public class MainWindowViewModel : ViewModelBase, IMainWindowViewModel
    {
        #region Fields

        private readonly IMainWindowViewModelHelper _helper;
        private readonly ICalculator _calculator;
        private readonly IGamePredictorWindow _gamePredictor;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the MainWindowViewModel class.
        /// </summary>
        public MainWindowViewModel(FootballDbEntities dbContext, IGlobals globals = null, IMainWindowViewModelHelper helper = null, ICalculator calculator = null, IGamePredictorWindow gamePredictor = null)
            :base(dbContext, globals)
		{
            _helper = (helper != null) ? helper : new MainWindowViewModelHelper(this);
            _calculator = (calculator != null) ? calculator : new Calculator();
            _gamePredictor = (gamePredictor != null) ? gamePredictor : new GamePredictorWindow();
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets or sets this window's seasons collection.
        /// </summary>
        private List<int> _Seasons;
        public List<int> Seasons
        {
            get
            {
                return _Seasons;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Seasons");
                }
                else if (value != _Seasons)
                {
                    _Seasons = value;
                    OnPropertyChanged("Seasons");
                    RequestUpdate = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's selected season.
        /// </summary>
        public int SelectedSeason
        {
            get
            {
                return (int)SharedGlobals.SelectedSeason;
            }
            set
            {
                if (value != SharedGlobals.SelectedSeason)
                {
                    SharedGlobals.SelectedSeason = value;
                    OnPropertyChanged("SelectedSeason");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Loads the DataModel's Seasons table.
        /// </summary>
        private DelegateCommand _ViewSeasonsCommand;
        public DelegateCommand ViewSeasonsCommand
        {
            get
            {
                if (_ViewSeasonsCommand == null)
                {
                    _ViewSeasonsCommand = new DelegateCommand(param => ViewSeasons());
                }
                return _ViewSeasonsCommand;
            }
        }
        private void ViewSeasons()
        {
            try
            {
                // Load the DataModel's Teams table.
                DbContext.Seasons.Load();
                var seasons = (from season in DbContext.Seasons select season).AsQueryable<Season>();
                Seasons = new List<int>();
                foreach (var season in seasons)
                {
                    Seasons.Add(season.ID);
                }
                SelectedSeason = 2016;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Runs a weekly update.
        /// </summary>
        private DelegateCommand _WeeklyUpdateCommand;
        public DelegateCommand WeeklyUpdateCommand
        {
            get
            {
                if (_WeeklyUpdateCommand == null)
                {
                    _WeeklyUpdateCommand = new DelegateCommand(param => RunWeeklyUpdate(),
                         param => { return DbContext.Games.Count() > 0; });
                }
                return _WeeklyUpdateCommand;
            }
        }
        private void RunWeeklyUpdate()
        {
            try
            {
                // I experimented with farming this long running operation out to a separate thread to improve UI responsiveness,
                // but I eventually concluded that I actually DON'T want the UI to be responsive while this update operation
                // is running. An attempt to view data that's in the process of changing may cause the application to crash, and
                // the data won't mean anything, anyway.
                var dlgResult = MessageBox.Show(
                    "This operation may make the UI unresponsive for a minute or two. Are you sure you want to continue?",
                    WpfGlobals.Constants.AppName, MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (dlgResult == MessageBoxResult.No)
                {
                    return;
                }

                foreach (var leagueSeason in DbContext.LeagueSeasons)
                {
                    var leagueTotalsRow = DbContext.usp_GetLeagueSeasonTotals(SharedGlobals.SelectedSeason).First();

                    leagueSeason.TotalGames = (int)leagueTotalsRow.TotalGames;
                    leagueSeason.TotalPoints = (int)leagueTotalsRow.TotalPoints;
                    leagueSeason.AveragePoints =
                        _calculator.Divide((double)leagueTotalsRow.TotalPoints, (double)leagueTotalsRow.TotalGames);

                }

                var srcWeekCount = (from game in DbContext.Games
                                    where game.SeasonID == SharedGlobals.SelectedSeason
                                    select game.Week).Max();
                var destWeekCount = (from w in DbContext.WeekCounts select w).FirstOrDefault();
                destWeekCount.Count = srcWeekCount;

                DbContext.SaveChanges();

                if (srcWeekCount >= 3)
                {
                    _helper.UpdateRankings();
                }

                MessageBox.Show("Weekly update completed.", WpfGlobals.Constants.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Opens the GamePredictorWindow.
        /// </summary>
        private DelegateCommand _PredictGameScoreCommand;
        public DelegateCommand PredictGameScoreCommand
        {
            get
            {
                if (_PredictGameScoreCommand == null)
                {
                    _PredictGameScoreCommand = new DelegateCommand(param => PredictGameScore());
                }
                return _PredictGameScoreCommand;
            }
        }
        private void PredictGameScore()
        {
            try
            {
                _gamePredictor.Show();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands
    }
}
