﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    /// <summary>
    /// ViewModel logic for the state TreeView control.
    /// </summary>
    public class LeagueViewModel : TreeViewItemViewModel
    {
        #region Fields

        private readonly ILeaguesTreeViewViewModel _parentControl;
        private readonly League _league;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new custom instance of the LeagueViewModel class.
        /// </summary>
        /// <param name = "parentControl"></param>
        /// <param name = "classes"></param>
        public LeagueViewModel(FootballDbEntities dbContext, ILeaguesTreeViewViewModel parentControl, League league, IGlobals globals = null)
            : base(dbContext, null, true, globals)
        {
            // Validate arguments.
            if (parentControl == null)
            {
                throw new ArgumentNullException("parentControl");
            }
            if (league == null)
            {
                throw new ArgumentNullException("league");
            }

            _parentControl = parentControl;
            _league = league;
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the name of the League instance wrapped inside this ViewModel.
        /// </summary>
        public override string AssociationName
        {
            get
            {
                return _league.Name;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is expanded.
        /// </summary>
        public override bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                if (value != _IsExpanded)
                {
                    _IsExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (_IsExpanded && Parent != null)
                {
                    Parent.IsExpanded = true;
                }

                // Lazy load the child items, if necessary.
                if (HasDummyChild())
                {
                    Children.Remove(DummyChild);
                    LoadChildren();
                }
            }
        }

        /// <summary>
        /// Gets or sets this control's standings collection.
        /// </summary>
        public override ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult
        {
            get
            {
                return _parentControl.StandingsResult;
            }
            set
            {
                _parentControl.StandingsResult = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Loads this object's children into the TreeView.
        /// </summary>
        protected internal override void LoadChildren(ObservableCollection<ITreeViewItemViewModel> children = null)
        {
            try
            {
                // Load collection of Conference objects for the League object wrapped inside this ViewModel.
                var conferences = (from conference in DbContext.Conferences
                                   orderby conference.Name ascending
                                   select conference)
                                   .AsEnumerable()
                                   .Where(x => x.AssociationExists() && x.LeagueName == this.AssociationName);

                if (conferences.Count() > 0)
                {
                    Children = (children != null) ?
                        children
                        :
                        new ObservableCollection<ITreeViewItemViewModel>((
                            from conference in conferences
                            select new ConferenceViewModel(this, DbContext, conference))
                            .ToList<TreeViewItemViewModel>());
                }
                else
                {
                    // Load collection of Division objects for the League object wrapped inside this ViewModel.
                    var divisions = (from division in DbContext.Divisions
                                     orderby division.Name ascending
                                     select division)
                                     .AsEnumerable()
                                     .Where(x => x.AssociationExists() && x.LeagueName == this.AssociationName);

                    if (divisions.Count() > 0)
                    {
                        Children = (children != null) ?
                            children
                            :
                            new ObservableCollection<ITreeViewItemViewModel>((
                                from division in divisions
                                select new DivisionViewModel(this, DbContext, division))
                                .ToList<TreeViewItemViewModel>());
                    }
                    else
                    {
                        Children = null;
                    }
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Methods
    }
}
