﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.FocusVMLib;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;
using EldredBrown.FootballApplicationWPF.Windows;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IGamesWindowViewModel : IViewModelBase
    {
        Visibility AddGameControlVisibility { get; set; }
        Visibility EditGameControlVisibility { get; set; }
        IGameFinderWindow GameFinder { get; set; }
        ObservableCollection<Game> Games { get; set; }
        string GuestName { get; set; }
        double GuestScore { get; set; }
        string HostName { get; set; }
        double HostScore { get; set; }
        bool IsFindGameFilterApplied { get; set; }
        bool IsGamesReadOnly { get; set; }
        bool IsPlayoffGame { get; set; }
        bool IsPlayoffGameEnabled { get; set; }
        bool IsShowAllGamesEnabled { get; set; }
        string Notes { get; set; }
        Visibility RemoveGameControlVisibility { get; set; }
        IGame SelectedGame { get; set; }
        int Week { get; set; }

        DelegateCommand AddGameCommand { get; }
        DelegateCommand EditGameCommand { get; }
        DelegateCommand FindGameCommand { get; }
        DelegateCommand RemoveGameCommand { get; }
        DelegateCommand ShowAllGamesCommand { get; }
        DelegateCommand ViewGamesCommand { get; }

        void MoveFocusTo(string focusedProperty);

        event EventHandler<MoveFocusEventArgs> MoveFocus;
    }

    /// <summary>
    /// ViewModel logic for the Games window.
    /// </summary>
    public class GamesWindowViewModel : ViewModelBase, IFocusMover, IGamesWindowViewModel
	{
        #region Fields

        private readonly IGamesWindowViewModelHelper _helper;

        #endregion Fields

        #region Constructors & Finalizers
        /// <summary>
        /// Initializes a new instance of the GamesWindowViewModel class
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="globals"></param>
        /// <param name="helper"></param>
        /// <param name="gameFinder"></param>
        public GamesWindowViewModel(FootballDbEntities dbContext, IGlobals globals = null, IGamesWindowViewModelHelper helper = null, IGameFinderWindow gameFinder = null)
            :base(dbContext, globals)
		{
            _helper = (helper != null) ? helper : new GamesWindowViewModelHelper(this);
            GameFinder = (gameFinder != null) ? gameFinder : new GameFinderWindow();
		}

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the visibility of the AddGame control
        /// </summary>
        private Visibility _AddGameControlVisibility;
        public Visibility AddGameControlVisibility
        {
            get
            {
                return _AddGameControlVisibility;
            }
            set
            {
                if (value != _AddGameControlVisibility)
                {
                    _AddGameControlVisibility = value;
                    OnPropertyChanged("AddGameControlVisibility");
                }
            }
        }

        /// <summary>
        /// Gets the visibility of the EditGame control.
        /// </summary>
        private Visibility _EditGameControlVisibility;
        public Visibility EditGameControlVisibility
        {
            get
            {
                return _EditGameControlVisibility;
            }
            set
            {
                if (value != _EditGameControlVisibility)
                {
                    _EditGameControlVisibility = value;
                    OnPropertyChanged("EditGameControlVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets this object's GameFinder window
        /// </summary>
        public IGameFinderWindow GameFinder { get; set; }

        /// <summary>
        /// Gets this window's games collection. 
        /// </summary>
        private ObservableCollection<Game> _Games;
        public ObservableCollection<Game> Games
        {
            get
            {
                return _Games;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Games");
                }
                else if (value != _Games)
                {
                    _Games = value;
                    OnPropertyChanged("Games");
                    RequestUpdate = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's guest value.
        /// </summary>
        private string _GuestName;
        public string GuestName
        {
            get
            {
                return _GuestName;
            }
            set
            {
                if (value != _GuestName)
                {
                    _GuestName = value;
                    OnPropertyChanged("GuestName");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's guest score value.
        /// </summary>
        private double _GuestScore;
        public double GuestScore
        {
            get
            {
                return _GuestScore;
            }
            set
            {
                if (value != _GuestScore)
                {
                    _GuestScore = value;
                    OnPropertyChanged("GuestScore");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's host value.
        /// </summary>
        private string _HostName;
        public string HostName
        {
            get
            {
                return _HostName;
            }
            set
            {
                if (value != _HostName)
                {
                    _HostName = value;
                    OnPropertyChanged("HostName");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's host score value.
        /// </summary>
        private double _HostScore;
        public double HostScore
        {
            get
            {
                return _HostScore;
            }
            set
            {
                if (value != _HostScore)
                {
                    _HostScore = value;
                    OnPropertyChanged("HostScore");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the FindGameFilter has been applied for this object
        /// </summary>
        public bool IsFindGameFilterApplied { get; set; }

        /// <summary>
        /// Gets or sets the value that indicates whether the Games collection is read-only.
        /// </summary>
        private bool _IsGamesReadOnly;
        public bool IsGamesReadOnly
        {
            get
            {
                return _IsGamesReadOnly;
            }
            set
            {
                if (value != _IsGamesReadOnly)
                {
                    _IsGamesReadOnly = value;
                    OnPropertyChanged("IsGamesReadOnly");
                }
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates whether this game is a playoff game.
        /// </summary>
        private bool _IsPlayoffGame;
        public bool IsPlayoffGame
        {
            get
            {
                return _IsPlayoffGame;
            }
            set
            {
                if (value != _IsPlayoffGame)
                {
                    _IsPlayoffGame = value;
                    OnPropertyChanged("IsPlayoffGame");
                }
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates whether this game can be a playoff game.
        /// </summary>
        private bool _IsPlayoffGameEnabled;
        public bool IsPlayoffGameEnabled
        {
            get
            {
                return _IsPlayoffGameEnabled;
            }
            set
            {
                if (value != _IsPlayoffGameEnabled)
                {
                    _IsPlayoffGameEnabled = value;
                    OnPropertyChanged("IsPlayoffGameEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the value that indicates whether the ShowAllGames function is enabled.
        /// </summary>
        private bool _IsShowAllGamesEnabled;
        public bool IsShowAllGamesEnabled
        {
            get
            {
                return _IsShowAllGamesEnabled;
            }
            set
            {
                if (value != _IsShowAllGamesEnabled)
                {
                    _IsShowAllGamesEnabled = value;
                    OnPropertyChanged("IsShowAllGamesEnabled");
                }
            }
        }

        /// <summary>
        /// Gets or sets the notes entered for this game, if any.
        /// </summary>
        private string _Notes;
        public string Notes
        {
            get
            {
                return _Notes;
            }
            set
            {
                if (value != _Notes)
                {
                    _Notes = value;
                    OnPropertyChanged("Notes");
                }
            }
        }

        /// <summary>
        /// Gets the visibility of the RemoveGame control.
        /// </summary>
        private Visibility _RemoveGameControlVisibility;
        public Visibility RemoveGameControlVisibility
        {
            get
            {
                return _RemoveGameControlVisibility;
            }
            set
            {
                if (value != _RemoveGameControlVisibility)
                {
                    _RemoveGameControlVisibility = value;
                    OnPropertyChanged("RemoveGameControlVisibility");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's selected game
        /// </summary>
        private IGame _SelectedGame;
        public IGame SelectedGame
        {
            get
            {
                return _SelectedGame;
            }
            set
            {
                if ((value != _SelectedGame) || (_SelectedGame == null))
                {
                    _SelectedGame = value;
                    OnPropertyChanged("SelectedGame");

                    if (_SelectedGame == null)
                    {
                        _helper.ClearDataEntryControls();
                    }
                    else
                    {
                        _helper.PopulateDataEntryControls();
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets this window's week value.
        /// </summary>
        private int _Week;
        public int Week
        {
            get
            {
                return _Week;
            }
            set
            {
                if (value != _Week)
                {
                    _Week = value;
                    OnPropertyChanged("Week");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Adds a new game to the database.
        /// </summary>
        private DelegateCommand _AddGameCommand;
        public DelegateCommand AddGameCommand
        {
            get
            {
                if (_AddGameCommand == null)
                {
                    _AddGameCommand = new DelegateCommand(param => AddGame());
                }
                return _AddGameCommand;
            }
        }
        private void AddGame()
        {
            try
            {
                _helper.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                _globals.ShowExceptionMessage(ex);
                return;
            }

            try
            {
                var newGame = _helper.StoreNewGameValues();

                // Add this game to the Game's table.
                DbContext.Games.Add(newGame as Game);

                _helper.EditTeams(newGame, Direction.Up);
                _helper.SaveChanges();

                // Sort Games collection so that the most recently added games appear at the top.
                // I find this good for bookmarking my progress.
                _helper.SortGamesByDefaultOrder();
                SelectedGame = null;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Edits an existing game in the database.
        /// </summary>
        private DelegateCommand _EditGameCommand;
        public DelegateCommand EditGameCommand
        {
            get
            {
                if (_EditGameCommand == null)
                {
                    _EditGameCommand = new DelegateCommand(param => EditGame());
                }
                return _EditGameCommand;
            }
        }
        private void EditGame()
        {
            try
            {
                _helper.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                _globals.ShowExceptionMessage(ex);
                return;
            }

            try
            {
                var oldGame = _helper.StoreOldGameValues();
                _helper.EditTeams(oldGame, Direction.Down);

                var newGame = _helper.StoreNewGameValues();

                // Edit selected row of Games table.
                // Replace property values of Game instance selected in DataGrid with values from newGame.
                SelectedGame.Week = newGame.Week;
                SelectedGame.Guest = newGame.Guest;
                SelectedGame.GuestScore = newGame.GuestScore;
                SelectedGame.Host = newGame.Host;
                SelectedGame.HostScore = newGame.HostScore;
                SelectedGame.IsPlayoffGame = newGame.IsPlayoffGame;
                SelectedGame.Notes = newGame.Notes;

                _helper.EditTeams(newGame, Direction.Up);

                // Save changes and refresh view object bound to the Games table.
                _helper.SaveChanges();

                if (IsFindGameFilterApplied)
                {
                    _helper.ApplyFindGameFilter();
                }
                else
                {
                    _helper.SortGamesByDefaultOrder();
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Searches for a specified game in the database.
        /// </summary>
        private DelegateCommand _FindGameCommand;
        public DelegateCommand FindGameCommand
        {
            get
            {
                if (_FindGameCommand == null)
                {
                    _FindGameCommand = new DelegateCommand(param => FindGame());
                }
                return _FindGameCommand;
            }
        }
        private void FindGame()
        {
            try
            {
                var dlgResult = GameFinder.ShowDialog();

                if (dlgResult == true)
                {
                    _helper.ApplyFindGameFilter();
                    IsGamesReadOnly = true;
                    IsShowAllGamesEnabled = true;

                    if (Games.Count == 0)
                    {
                        SelectedGame = null;
                    }

                    AddGameControlVisibility = Visibility.Hidden;
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Removes an existing game from the database.
        /// </summary>
        private DelegateCommand _RemoveGameCommand;
        public DelegateCommand RemoveGameCommand
        {
            get
            {
                if (_RemoveGameCommand == null)
                {
                    _RemoveGameCommand = new DelegateCommand(param => RemoveGame());
                }
                return _RemoveGameCommand;
            }
        }
        private void RemoveGame()
        {
            try
            {
                var oldGame = _helper.StoreOldGameValues();

                // Delete matching row in the dataset.Games table
                DbContext.Games.Remove(SelectedGame as Game);

                _helper.EditTeams(oldGame, Direction.Down);
                _helper.SaveChanges();
                SelectedGame = null;

                _helper.SortGamesByDefaultOrder();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Shows all the games currently in the database.
        /// </summary>
        private DelegateCommand _ShowAllGamesCommand;
        public DelegateCommand ShowAllGamesCommand
        {
            get
            {
                if (_ShowAllGamesCommand == null)
                {
                    _ShowAllGamesCommand = new DelegateCommand(param => ShowAllGames());
                }
                return _ShowAllGamesCommand;
            }
        }
        private void ShowAllGames()
        {
            try
            {
                ViewGames();
                IsFindGameFilterApplied = false;
                IsGamesReadOnly = false;
                IsShowAllGamesEnabled = false;
                SelectedGame = null;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Views the Games database table.
        /// </summary>
        private DelegateCommand _ViewGamesCommand;
        public DelegateCommand ViewGamesCommand
        {
            get
            {
                if (_ViewGamesCommand == null)
                {
                    _ViewGamesCommand = new DelegateCommand(param => ViewGames());
                }
                return _ViewGamesCommand;
            }
        }
        private void ViewGames()
        {
            try
            {
                // Load the DataModel's Games table.
                DbContext.Games.Load();
                _helper.SortGamesByDefaultOrder();
                SelectedGame = null;
                OnMoveFocus("GuestName");

                Week = (from count in DbContext.WeekCounts select count)
                    .FirstOrDefault()
                    .Count;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands

        #region Methods

        /// <summary>
        /// Moves the focus to the specified property
        /// </summary>
        /// <param name="focusedProperty"></param>
        public void MoveFocusTo(string focusedProperty)
        {
            OnMoveFocus(focusedProperty);
        }

        #endregion Methods
    }
}
