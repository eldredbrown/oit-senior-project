﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.FocusVMLib;
using EldredBrown.FootballApplicationWPF.ViewModels.Helpers;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IGamePredictorWindowViewModel : IViewModelBase
    {
        string GuestName { get; set; }
        int? GuestScore { get; set; }
        List<int> GuestSeasons { get; set; }
        int GuestSelectedSeason { get; set; }
        string HostName { get; set; }
        int? HostScore { get; set; }
        List<int> HostSeasons { get; set; }
        int HostSelectedSeason { get; set; }

        DelegateCommand CalculatePredictionCommand { get; }
        DelegateCommand ViewSeasonsCommand { get; }

        void MoveFocusTo(string focusedProperty);
    }

    public class GamePredictorWindowViewModel : ViewModelBase, IFocusMover, IGamePredictorWindowViewModel
    {
        #region Fields

        private readonly IGamePredictorWindowViewModelHelper _helper;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GamePredictorWindowViewModel class
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="globals"></param>
        /// <param name="helper"></param>
        public GamePredictorWindowViewModel(FootballDbEntities dbContext, IGlobals globals = null, IGamePredictorWindowViewModelHelper helper = null)
            : base(dbContext, globals)
        {
            _helper = (helper != null) ? helper : new GamePredictorWindowViewModelHelper(this);
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets or sets this window's guest value.
        /// </summary>
        private string _GuestName;
        public string GuestName
        {
            get
            {
                return _GuestName;
            }
            set
            {
                if (value != _GuestName)
                {
                    _GuestName = value;
                    OnPropertyChanged("GuestName");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's predicted guest score value
        /// </summary>
        private int? _GuestScore;
        public int? GuestScore
        {
            get
            {
                return _GuestScore;
            }
            set
            {
                if (value != _GuestScore)
                {
                    _GuestScore = value;
                    OnPropertyChanged("GuestScore");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's seasons collection.
        /// </summary>
        private List<int> _GuestSeasons;
        public List<int> GuestSeasons
        {
            get
            {
                return _GuestSeasons;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("GuestSeasons");
                }
                else if (value != _GuestSeasons)
                {
                    _GuestSeasons = value;
                    OnPropertyChanged("GuestSeasons");
                    RequestUpdate = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's selected season.
        /// </summary>
        private int _GuestSelectedSeason;
        public int GuestSelectedSeason
        {
            get
            {
                return _GuestSelectedSeason;
            }
            set
            {
                if (value != _GuestSelectedSeason)
                {
                    _GuestSelectedSeason = value;
                    OnPropertyChanged("GuestSelectedSeason");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's host value.
        /// </summary>
        private string _HostName;
        public string HostName
        {
            get
            {
                return _HostName;
            }
            set
            {
                if (value != _HostName)
                {
                    _HostName = value;
                    OnPropertyChanged("HostName");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's predicted guest score value
        /// </summary>
        private int? _HostScore;
        public int? HostScore
        {
            get
            {
                return _HostScore;
            }
            set
            {
                if (value != _HostScore)
                {
                    _HostScore = value;
                    OnPropertyChanged("HostScore");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's seasons collection.
        /// </summary>
        private List<int> _HostSeasons;
        public List<int> HostSeasons
        {
            get
            {
                return _HostSeasons;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("HostSeasons");
                }
                else if (value != _HostSeasons)
                {
                    _HostSeasons = value;
                    OnPropertyChanged("HostSeasons");
                    RequestUpdate = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's selected season.
        /// </summary>
        private int _HostSelectedSeason;
        public int HostSelectedSeason
        {
            get
            {
                return _HostSelectedSeason;
            }
            set
            {
                if (value != _HostSelectedSeason)
                {
                    _HostSelectedSeason = value;
                    OnPropertyChanged("HostSelectedSeason");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Loads the DataModel's Seasons table.
        /// </summary>
        private DelegateCommand _ViewSeasonsCommand;
        public DelegateCommand ViewSeasonsCommand
        {
            get
            {
                if (_ViewSeasonsCommand == null)
                {
                    _ViewSeasonsCommand = new DelegateCommand(param => ViewSeasons());
                }
                return _ViewSeasonsCommand;
            }
        }
        private void ViewSeasons()
        {
            try
            {
                // Load the DataModel's Teams table.
                DbContext.Seasons.Load();
                var seasons = (from season in DbContext.Seasons select season).AsQueryable<Season>();

                GuestSeasons = new List<int>();
                HostSeasons = new List<int>();
                foreach (var season in seasons)
                {
                    GuestSeasons.Add(season.ID);
                    HostSeasons.Add(season.ID);
                }
                GuestSelectedSeason = 2016;
                HostSelectedSeason = 2016;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Calculates the predicted score of a future or hypothetical game.
        /// </summary>
        private DelegateCommand _CalculatePredictionCommand;
        public DelegateCommand CalculatePredictionCommand
        {
            get
            {
                if (_CalculatePredictionCommand == null)
                {
                    _CalculatePredictionCommand = new DelegateCommand(param => CalculatePrediction());
                }
                return _CalculatePredictionCommand;
            }
        }
        private void CalculatePrediction()
        {
            try
            {
                var matchup = _helper.ValidateDataEntry();
                var guestSeason = (from teamSeason in DbContext.TeamSeasons
                                   where (teamSeason.Team.Name == GuestName) && (teamSeason.SeasonID == GuestSelectedSeason)
                                   select teamSeason)
                                   .FirstOrDefault();
                var hostSeason = (from teamSeason in DbContext.TeamSeasons
                                  where (teamSeason.Team.Name == HostName) && (teamSeason.SeasonID == HostSelectedSeason)
                                  select teamSeason)
                                  .FirstOrDefault();

                GuestScore = (int)((guestSeason.OffensiveFactor * hostSeason.DefensiveAverage + hostSeason.DefensiveFactor * guestSeason.OffensiveAverage) / 2d);
                HostScore = (int)((hostSeason.OffensiveFactor * guestSeason.DefensiveAverage + guestSeason.DefensiveFactor * hostSeason.OffensiveAverage) / 2d);
            }
            catch (DataValidationException ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands

        #region Methods

        /// <summary>
        /// Moves the focus to the specified property
        /// </summary>
        /// <param name="focusedProperty"></param>
        public void MoveFocusTo(string focusedProperty)
        {
            OnMoveFocus(focusedProperty);
        }

        #endregion Methods
    }
}
