﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface ILeaguesTreeViewViewModel
    {
        ReadOnlyCollection<ITreeViewItemViewModel> Leagues { get; }
        ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult { get; set; }
    }

    public class LeaguesTreeViewViewModel : ViewModelBase, ILeaguesTreeViewViewModel
    {
        #region Fields

        private readonly IStandingsControlViewModel _parentControl;

        #endregion Fields

        #region Constructors & Finalizers
        /// <summary>
        /// Initializes a new instance of the LeaguesTreeViewViewModel class
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="parentControl"></param>
        /// <param name="leagues"></param>
        /// <param name="globals"></param>
        /// <param name="leaguesCollection"></param>
        public LeaguesTreeViewViewModel(FootballDbEntities dbContext, IStandingsControlViewModel parentControl, IEnumerable<League> leagues, IGlobals globals = null, ReadOnlyCollection<ITreeViewItemViewModel> leaguesCollection = null)
            :base(dbContext, globals)
        {
            // Validate required arguments.
            if (parentControl == null)
            {
                throw new ArgumentNullException("parentControl");
            }
            if (leagues == null)
            {
                throw new ArgumentNullException("leagues");
            }

            _parentControl = parentControl;

            try
            {
                this.Leagues = (leaguesCollection != null) ?
                    leaguesCollection
                    :
                    new ReadOnlyCollection<ITreeViewItemViewModel>((
                    from league in leagues
                    select new LeagueViewModel(dbContext, this, league))
                    .ToList<ITreeViewItemViewModel>());
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets this control's leagues collection.
        /// </summary>
        private ReadOnlyCollection<ITreeViewItemViewModel> _Leagues;
        public ReadOnlyCollection<ITreeViewItemViewModel> Leagues
        {
            get
            {
                return _Leagues;
            }
            internal set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Leagues");
                }
                else if (value != _Leagues)
                {
                    _Leagues = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets this control's standings collection.
        /// </summary>
        public ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult
        {
            get
            {
                return _parentControl.StandingsResult;
            }
            set
            {
                _parentControl.StandingsResult = value;
            }
        }

        #endregion Properties
    }
}
