﻿using System;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IGameFinderWindowViewModel
    {
        string GuestName { get; set; }
        string HostName { get; set; }

        DelegateCommand WindowLoadedCommand { get; }
    }

    /// <summary>
    /// ViewModel logic for the game finder window.
    /// </summary>
    public class GameFinderWindowViewModel : ViewModelBase, IGameFinderWindowViewModel
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the GameFinderWindowViewModel class.
        /// </summary>
        public GameFinderWindowViewModel(FootballDbEntities dbContext, IGlobals globals = null)
            : base(dbContext, globals)
		{
		}

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets or sets this window's guest value.
        /// </summary>
        private string _GuestName;
        public string GuestName
        {
            get
            {
                return _GuestName;
            }
            set
            {
                if (value != _GuestName)
                {
                    _GuestName = value;
                    OnPropertyChanged("GuestName");
                }
            }
        }

        /// <summary>
        /// Gets or sets this window's host value.
        /// </summary>
        private string _HostName;
        public string HostName
        {
            get
            {
                return _HostName;
            }
            set
            {
                if (value != _HostName)
                {
                    _HostName = value;
                    OnPropertyChanged("HostName");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Views the Games database table.
        /// </summary>
        private DelegateCommand _WindowLoadedCommand;
        public DelegateCommand WindowLoadedCommand
        {
            get
            {
                if (_WindowLoadedCommand == null)
                {
                    _WindowLoadedCommand = new DelegateCommand(param => WindowLoaded());
                }
                return _WindowLoadedCommand;
            }
        }
        private void WindowLoaded()
        {
            MoveFocusTo("GuestName");
        }

        #endregion Commands

        #region Methods

        /// <summary>
        /// Moves the focus to the specified property
        /// </summary>
        /// <param name="focusedProperty"></param>
        public void MoveFocusTo(string focusedProperty)
        {
            OnMoveFocus(focusedProperty);
        }

        /// <summary>
        /// Validates data entered into the data entry controls.
        /// </summary>
        public void ValidateDataEntry()
        {
            if (String.IsNullOrWhiteSpace(GuestName) || String.IsNullOrWhiteSpace(HostName))
            {
                throw new DataValidationException(WpfGlobals.Constants.BothTeamsNeededErrorMessage);
            }
            else if (GuestName == HostName)
            {
                throw new DataValidationException(WpfGlobals.Constants.DifferentTeamsNeededErrorMessage);
            }
        }

        #endregion Methods
    }
}
