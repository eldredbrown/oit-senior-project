﻿using System;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IRankingsControlViewModel
    {
        ObjectResult<usp_GetDefensiveRankings_Result> DefensiveRankingsResult { get; set; }
        ObjectResult<usp_GetOffensiveRankings_Result> OffensiveRankingsResult { get; set; }
        ObjectResult<usp_GetTotalRankings_Result> TotalRankingsResult { get; set; }

        DelegateCommand ViewRankingsCommand { get; }
    }

    /// <summary>
    /// ViewModel logic for the Rankings control.
    /// </summary>
    public class RankingsControlViewModel : ViewModelBase, IRankingsControlViewModel
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a default instance of the RankingsControlViewModel class.
        /// </summary>
        public RankingsControlViewModel(FootballDbEntities dbContext, IGlobals globals = null)
            : base(dbContext, globals)
        {
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets or sets this control's defensive rankings collection.
        /// </summary>
        private ObjectResult<usp_GetDefensiveRankings_Result> _DefensiveRankingsResult;
        public ObjectResult<usp_GetDefensiveRankings_Result> DefensiveRankingsResult
        {
            get
            {
                return _DefensiveRankingsResult;
            }
            set
            {
                Debug.Assert(value != null);

                if (value == null)
                {
                    throw new ArgumentNullException("DefensiveRankingsResult");
                }
                else if (value != _DefensiveRankingsResult)
                {
                    _DefensiveRankingsResult = value;
                    OnPropertyChanged("DefensiveRankingsResult");
                }
            }
        }

        /// <summary>
        /// Gets or sets this control's offensive rankings collection.
        /// </summary>
        private ObjectResult<usp_GetOffensiveRankings_Result> _OffensiveRankingsResult;
        public ObjectResult<usp_GetOffensiveRankings_Result> OffensiveRankingsResult
        {
            get
            {
                return _OffensiveRankingsResult;
            }
            set
            {
                Debug.Assert(value != null);

                if (value == null)
                {
                    throw new ArgumentNullException("OffensiveRankingsResult");
                }
                else if (value != _OffensiveRankingsResult)
                {
                    _OffensiveRankingsResult = value;
                    OnPropertyChanged("OffensiveRankingsResult");
                }
            }
        }

        /// <summary>
        /// Gets or sets this control's total rankings collection.
        /// </summary>
        private ObjectResult<usp_GetTotalRankings_Result> _TotalRankingsResult;
        public ObjectResult<usp_GetTotalRankings_Result> TotalRankingsResult
        {
            get
            {
                return _TotalRankingsResult;
            }
            set
            {
                Debug.Assert(value != null);

                if (value == null)
                {
                    throw new ArgumentNullException("TotalRankingsResult");
                }
                else if (value != _TotalRankingsResult)
                {
                    _TotalRankingsResult = value;
                    OnPropertyChanged("TotalRankingsResult");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Loads the DataModel's Teams table.
        /// </summary>
        private DelegateCommand _viewRankingsCommand;
        public DelegateCommand ViewRankingsCommand
        {
            get
            {
                if (_viewRankingsCommand == null)
                {
                    _viewRankingsCommand = new DelegateCommand(param => ViewRankings());
                }
                return _viewRankingsCommand;
            }
        }
        private void ViewRankings()
        {
            try
            {
                // Load the rankings grids.
                var selectedSeason = SharedGlobals.SelectedSeason;
                TotalRankingsResult = DbContext.usp_GetTotalRankings(selectedSeason);
                OffensiveRankingsResult = DbContext.usp_GetOffensiveRankings(selectedSeason);
                DefensiveRankingsResult = DbContext.usp_GetDefensiveRankings(selectedSeason);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands
    }
}
