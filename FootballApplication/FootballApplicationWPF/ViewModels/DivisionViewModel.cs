﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    /// <summary>
    /// ViewModel logic for the Division tree view item.
    /// </summary>
    public class DivisionViewModel : TreeViewItemViewModel
	{
        #region Fields

        private readonly Division _division;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a custom instance of the DivisionViewModel class.
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "division"></param>
        public DivisionViewModel(ITreeViewItemViewModel parent, FootballDbEntities dbContext, Division division, IGlobals globals = null)
			: base(dbContext, parent, false, globals)
		{
            // Validate required arguments.
			if (division == null)
			{
				throw new ArgumentNullException("division");
			}

			// Assign argument values to member fields.
			_division = division;
		}

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the name of the Division instance wrapped inside this ViewModel.
        /// </summary>
        public override string AssociationName
        {
            get
            {
                return _division.Name;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
        /// </summary>
        public override bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }

                ShowStandings();
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Shows the standings for the selected division.
        /// </summary>
        protected internal override void ShowStandings(ObservableCollection<usp_GetSeasonAssociationStandings_Result> associationStandingsResult = null)
        {
            try
            {
                // Load standings data for the Division object wrapped inside this ViewModel.
                var conferenceName = Parent.AssociationName;
                var dsr = (from result
                           in DbContext.usp_GetDivisionSeasonStandings(_division.Name, SharedGlobals.SelectedSeason)
                           select result)
                           .ToArray();

                var divisionStandingsResult = (associationStandingsResult != null) ?
                    associationStandingsResult
                    :
                    new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();
                foreach (var item in dsr)
                {
                    divisionStandingsResult.Add(item);
                }

                Parent.StandingsResult = divisionStandingsResult;
            }
            catch (Exception ex)
            {
                Parent.StandingsResult = null;
                _globals.ShowExceptionMessage(ex.InnerException);
            }
        }

        #endregion Methods
    }
}
