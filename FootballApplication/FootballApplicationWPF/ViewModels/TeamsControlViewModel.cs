﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface ITeamsControlViewModel
    {
        TeamSeason SelectedTeam { get; set; }
        ObservableCollection<TeamSeason> Teams { get; set; }
        ObjectResult<usp_GetTeamSeasonScheduleAverages_Result> TeamScheduleAveragesResult { get; }
        ObjectResult<usp_GetTeamSeasonScheduleProfile_Result> TeamScheduleProfileResult { get; }
        ObjectResult<usp_GetTeamSeasonScheduleTotals_Result> TeamScheduleTotalsResult { get; }

        DelegateCommand ViewTeamScheduleCommand { get; }
        DelegateCommand ViewTeamsCommand { get; }
    }

    /// <summary>
    /// ViewModel logic for the Teams control.
    /// </summary>
    public class TeamsControlViewModel : ViewModelBase, ITeamsControlViewModel
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the TeamsControlViewModel class.
        /// </summary>
        public TeamsControlViewModel(FootballDbEntities dbContext, IGlobals globals = null)
            : base(dbContext, globals)
		{
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets or sets this control's selected team.
        /// </summary>
        private TeamSeason _SelectedTeam;
        public TeamSeason SelectedTeam
        {
            get
            {
                return _SelectedTeam;
            }
            set
            {
                if (value != _SelectedTeam)
                {
                    _SelectedTeam = value;
                    OnPropertyChanged("SelectedTeam");
                }
            }
        }

        /// <summary>
        /// Gets this control's teams collection.
        /// </summary>
        private ObservableCollection<TeamSeason> _Teams;
        public ObservableCollection<TeamSeason> Teams
        {
            get
            {
                return _Teams;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Teams");
                }
                else if (value != _Teams)
                {
                    _Teams = value;
                    OnPropertyChanged("Teams");
                    RequestUpdate = true;
                }
            }
        }

        /// <summary>
        /// Gets this control's collection of team schedule averages.
        /// </summary>
        private ObjectResult<usp_GetTeamSeasonScheduleAverages_Result> _TeamScheduleAveragesResult;
        public ObjectResult<usp_GetTeamSeasonScheduleAverages_Result> TeamScheduleAveragesResult
        {
            get
            {
                return _TeamScheduleAveragesResult;
            }
            private set
            {
                if (value != _TeamScheduleAveragesResult)
                {
                    _TeamScheduleAveragesResult = value;
                    OnPropertyChanged("TeamScheduleAveragesResult");
                }
            }
        }

        /// <summary>
        /// Gets this control's team schedule profile collection.
        /// </summary>
        private ObjectResult<usp_GetTeamSeasonScheduleProfile_Result> _TeamScheduleProfileResult;
        public ObjectResult<usp_GetTeamSeasonScheduleProfile_Result> TeamScheduleProfileResult
        {
            get
            {
                return _TeamScheduleProfileResult;
            }
            private set
            {
                if (value != _TeamScheduleProfileResult)
                {
                    _TeamScheduleProfileResult = value;
                    OnPropertyChanged("TeamScheduleProfileResult");
                }
            }
        }

        /// <summary>
        /// Gets this controls's collection of team schedule totals.
        /// </summary>
        private ObjectResult<usp_GetTeamSeasonScheduleTotals_Result> _TeamScheduleTotalsResult;
        public ObjectResult<usp_GetTeamSeasonScheduleTotals_Result> TeamScheduleTotalsResult
        {
            get
            {
                return _TeamScheduleTotalsResult;
            }
            private set
            {
                if (value != _TeamScheduleTotalsResult)
                {
                    _TeamScheduleTotalsResult = value;
                    OnPropertyChanged("TeamScheduleTotalsResult");
                }
            }
        }

        #endregion Properties

        #region Commands

        /// <summary>
        /// Views the team schedule profile, totals, and averages for the selected team.
        /// </summary>
        private DelegateCommand _viewTeamScheduleCommand;
        public DelegateCommand ViewTeamScheduleCommand
        {
            get
            {
                if (_viewTeamScheduleCommand == null)
                {
                    _viewTeamScheduleCommand = new DelegateCommand(param => ViewTeamSchedule());
                }
                return _viewTeamScheduleCommand;
            }
        }
        private void ViewTeamSchedule()
        {
            try
            {
                if (SelectedTeam == null)
                {
                    TeamScheduleProfileResult = null;
                    TeamScheduleTotalsResult = null;
                    TeamScheduleAveragesResult = null;
                }
                else
                {
                    // Load data from TeamScheduleProfile, TeamScheduleTotals, and TeamScheduleAverages for selected team.
                    var teamName = SelectedTeam.TeamName;

                    TeamScheduleProfileResult = DbContext.usp_GetTeamSeasonScheduleProfile(teamName, SharedGlobals.SelectedSeason);
                    TeamScheduleTotalsResult = DbContext.usp_GetTeamSeasonScheduleTotals(teamName, SharedGlobals.SelectedSeason);
                    TeamScheduleAveragesResult = DbContext.usp_GetTeamSeasonScheduleAverages(teamName, SharedGlobals.SelectedSeason);
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Loads the DataModel's Teams table.
        /// </summary>
        private DelegateCommand _viewTeamsCommand;
        public DelegateCommand ViewTeamsCommand
        {
            get
            {
                if (_viewTeamsCommand == null)
                {
                    _viewTeamsCommand = new DelegateCommand(param => ViewTeams());
                }
                return _viewTeamsCommand;
            }
        }
        private void ViewTeams()
        {
            try
            {
                // Load the DataModel's Teams table.
                DbContext.LeagueSeasons.Load();

                var TeamSeasons = (from TeamSeason in DbContext.TeamSeasons
                                   where TeamSeason.SeasonID == SharedGlobals.SelectedSeason
                                   select TeamSeason)
                                   .AsQueryable();
                Teams = new ObservableCollection<TeamSeason>();
                foreach (var TeamSeason in TeamSeasons)
                {
                    Teams.Add(TeamSeason);
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Commands
    }
}
