﻿using System;
using System.Collections.ObjectModel;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface ITreeViewItemViewModel
    {
        string AssociationName { get; }
        ObservableCollection<ITreeViewItemViewModel> Children { get; }
        bool IsExpanded { get; set; }
        bool IsSelected { get; set; }
        ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult { get; set; }
    }

    /// <summary>
    /// Base class for all TreeViewItem ViewModels.
    /// </summary>
    public class TreeViewItemViewModel : ViewModelBase, ITreeViewItemViewModel
	{
        #region Fields

        protected static readonly TreeViewItemViewModel DummyChild = new TreeViewItemViewModel();

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a custom instance of the TreeViewItemViewModel class.
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "lazyLoadChildren"></param>
        protected TreeViewItemViewModel(FootballDbEntities dbContext, ITreeViewItemViewModel parent, bool lazyLoadChildren, IGlobals globals = null)
            : base(dbContext, globals)
		{
            Parent = parent;

            try
            {
				Children = new ObservableCollection<ITreeViewItemViewModel>();
				if (lazyLoadChildren)
				{
					Children.Add(DummyChild);
				}
			}
			catch (Exception ex)
			{
                _globals.ShowExceptionMessage(ex);
			}
		}

		// This is used to create the DummyChild instance.
		private TreeViewItemViewModel()
			: this(dbContext: DataAccess.DbContext, parent: null, lazyLoadChildren: true)
		{
		}

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the name of the association (league, conference, or division) represented by this object
        /// </summary>
        public virtual string AssociationName { get; }

        /// <summary>
        /// Gets the children for this TreeViewItem
        /// </summary>
        protected ObservableCollection<ITreeViewItemViewModel> _Children;
        public ObservableCollection<ITreeViewItemViewModel> Children
        {
            get
            {
                return _Children;
            }
            protected set
            {
                if (value != _Children)
                {
                    _Children = value;
                    OnPropertyChanged("Children");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is expanded.
        /// </summary>
        protected bool _IsExpanded;
        public virtual bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                if (value != _IsExpanded)
                {
                    _IsExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
        /// </summary>
        protected bool _IsSelected;
        public virtual bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// Gets or sets the standings table for display
        /// </summary>
        public virtual ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult { get; set; }

        /// <summary>
        /// Gets the parent for this TreeViewItem
        /// </summary>
        internal ITreeViewItemViewModel Parent { get; }

        #endregion Properties

        #region Protected Methods

        /// <summary>
        /// Gets a value indicating whether this control's children have been populated yet.
        /// </summary>
        protected bool HasDummyChild()
        {
            return ((Children.Count == 1) && (Children[0] == DummyChild));
        }

        protected internal virtual void LoadChildren(ObservableCollection<ITreeViewItemViewModel> children = null)
        {
            // To be implemented in subclass
        }

        protected internal virtual void ShowStandings(ObservableCollection<usp_GetSeasonAssociationStandings_Result> associationStandingsResult = null)
        {
            // To be implemented in subclass
        }

        #endregion Protected Methods
    }
}
