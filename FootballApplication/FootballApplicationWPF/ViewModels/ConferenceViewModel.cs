﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    /// <summary>
    /// ViewModel logic for the Conference tree view item.
    /// </summary>
    public class ConferenceViewModel : TreeViewItemViewModel
	{
        #region Fields

        private readonly Conference _conference;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a custom instance of the ConferenceViewModel class.
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "conference"></param>
        public ConferenceViewModel(ITreeViewItemViewModel parent, FootballDbEntities dbContext, Conference conference, IGlobals globals = null)
			: base(dbContext, parent, true, globals)
		{
			// Validate required arguments.
			if (conference == null)
			{
				throw new ArgumentNullException("conference");
			}

			// Assign argument values to member fields.
			_conference = conference;
        }

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets the name of the Conference instance wrapped inside this ViewModel.
        /// </summary>
        public override string AssociationName
        {
            get
            {
                return _conference.Name;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is expanded.
        /// </summary>
        public override bool IsExpanded
        {
            get
            {
                return _IsExpanded;
            }
            set
            {
                if (value != _IsExpanded)
                {
                    _IsExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (_IsExpanded && Parent != null)
                {
                    Parent.IsExpanded = true;
                }

                // Lazy load the child items, if necessary.
                if (HasDummyChild())
                {
                    Children.Remove(DummyChild);
                    LoadChildren();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the TreeViewItem associated with this object is selected.
        /// </summary>
        public override bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged("IsSelected");
                }

                ShowStandings();
            }
        }

        /// <summary>
        /// Gets or sets this control's standings collection.
        /// </summary>
        public override ObservableCollection<usp_GetSeasonAssociationStandings_Result> StandingsResult
        {
            get
            {
                return Parent.StandingsResult;
            }
            set
            {
                Parent.StandingsResult = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Loads this object's children into the TreeView.
        /// </summary>
        protected internal override void LoadChildren(ObservableCollection<ITreeViewItemViewModel> children = null)
        {
            try
            {
                // Load collection of Division objects for the League object wrapped inside this ViewModel.
                var divisions = (from division in DbContext.Divisions
                                 orderby division.Name ascending
                                 select division)
                                 .AsEnumerable()
                                 .Where(x => x.AssociationExists() && x.ConferenceName == this.AssociationName);

                if (divisions.Count() > 0)
                {
                    Children = (children != null) ?
                        children
                        :
                        new ObservableCollection<ITreeViewItemViewModel>((
                            from division in divisions
                            select new DivisionViewModel(this, DbContext, division))
                            .ToList<TreeViewItemViewModel>());
                }
                else
                {
                    Children = null;
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Shows the standings for the selected conference.
        /// </summary>
        protected internal override void ShowStandings(ObservableCollection<usp_GetSeasonAssociationStandings_Result> associationStandingsResult = null)
        {
            try
            {
                // Load standings data for the Conference object wrapped inside this ViewModel.
                var csr = (from result
                           in DbContext.usp_GetConferenceSeasonStandings(_conference.Name, SharedGlobals.SelectedSeason)
                           select result)
                           .ToArray();

                var conferenceStandingsResult = (associationStandingsResult != null) ?
                    associationStandingsResult
                    :
                    new ObservableCollection<usp_GetSeasonAssociationStandings_Result>();
                foreach (var item in csr)
                {
                    conferenceStandingsResult.Add(item);
                }

                Parent.StandingsResult = conferenceStandingsResult;
            }
            catch (Exception ex)
            {
                Parent.StandingsResult = null;
                _globals.ShowExceptionMessage(ex.InnerException);
            }
        }

        #endregion Methods
    }
}
