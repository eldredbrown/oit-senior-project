﻿using System;
using System.ComponentModel;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.ViewModels.FocusVMLib;

namespace EldredBrown.FootballApplicationWPF.ViewModels
{
    public interface IViewModelBase
    {
        FootballDbEntities DbContext { get; }
        bool RequestUpdate { get; set; }

        DelegateCommand UpdateCommand { get; }
    }

	/// <summary>
	/// Base class for all the ViewModel classes used in this project.
	/// </summary>
	public abstract class ViewModelBase : IViewModelBase, IFocusMover, INotifyPropertyChanged
    {
        #region Fields

        protected readonly IGlobals _globals;

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the ViewModelBase class.
        /// </summary>
        protected ViewModelBase(FootballDbEntities dbContext, IGlobals globals = null)
		{
            if (dbContext == null)
            {
                throw new ArgumentNullException("dbContext");
            }

            DbContext = dbContext;
            _globals = (globals != null) ? globals : new WpfGlobals();
		}

        #endregion Constructors & Finalizers

        #region Properties

        /// <summary>
        /// Gets this view model's data entity context
        /// </summary>
        public FootballDbEntities DbContext { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this view model object needs to be updated
        /// </summary>
        private bool _RequestUpdate;
		public bool RequestUpdate
		{
			get
            {
                return _RequestUpdate;
            }
			set
			{
				_RequestUpdate = value;
				OnPropertyChanged("RequestUpdate");
				_RequestUpdate = false;
			}
		}

        #endregion Properties

        #region Commands

        public DelegateCommand UpdateCommand
		{
			get
			{
				return new DelegateCommand(param => { RequestUpdate = true; });
			}
		}

        #endregion Commands

        #region Event Raisers

        /// <summary>
        /// Raises MoveFocus event when focus is moved from one control to another.
        /// </summary>
        /// <param name="focusedProperty"></param>
        protected virtual void OnMoveFocus(string focusedProperty)
        {
            // Validate focusedProperty argument.
            if (String.IsNullOrEmpty(focusedProperty))
            {
                throw new ArgumentException("focusedProperty");
            }

            // Raise MoveFocus event.
            var handler = MoveFocus;
            if (handler != null)
            {
                handler(this, new MoveFocusEventArgs(focusedProperty));
            }
        }

        /// <summary>
        /// Raise the PropertyChanged event.
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged(string name)
		{
			// Validate name argument.
			if (String.IsNullOrEmpty(name))
			{
				throw new ArgumentException("name");
			}

			// Raise event to all subscribed event handlers.
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}

        #endregion Event Raisers

        #region IFocusMover Members

        public event EventHandler<MoveFocusEventArgs> MoveFocus;

        #endregion IFocusMover Members

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Event triggered when a property is changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion INotifyPropertyChanged Members
    }
}
