﻿using System;
using System.Linq;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers
{
    public interface IGamePredictorWindowViewModelHelper
    {
        Matchup ValidateDataEntry();
    }

    public class GamePredictorWindowViewModelHelper : IGamePredictorWindowViewModelHelper
    {
        #region Fields

        private readonly IGamePredictorWindowViewModel _context;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GamePredictorWindowViewModelHelper class
        /// </summary>
        /// <param name="context"></param>
        public GamePredictorWindowViewModelHelper(IGamePredictorWindowViewModel context)
        {
            // Validate arguments.
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            _context = context;
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Validates the data entered via the context window
        /// </summary>
        /// <returns></returns>
        public Matchup ValidateDataEntry()
        {
            TeamSeason guestSeason = (from teamSeason in _context.DbContext.TeamSeasons
                                      where (teamSeason.Team.Name == _context.GuestName) &&
                                      (teamSeason.SeasonID == _context.GuestSelectedSeason)
                                      select teamSeason)
                                      .FirstOrDefault();

            TeamSeason hostSeason = (from teamSeason in _context.DbContext.TeamSeasons
                                     where (teamSeason.Team.Name == _context.HostName) &&
                                     (teamSeason.SeasonID == _context.HostSelectedSeason)
                                     select teamSeason)
                                     .FirstOrDefault();

            if (String.IsNullOrWhiteSpace(_context.GuestName))
            {
                // GuestName not in database (name probably misspelled)
                _context.MoveFocusTo("GuestName");
                throw new DataValidationException(WpfGlobals.Constants.BothTeamsNeededErrorMessage);
            }
            else if (String.IsNullOrWhiteSpace(_context.HostName))
            {
                // GuestName not in database (name probably misspelled)
                _context.MoveFocusTo("HostName");
                throw new DataValidationException(WpfGlobals.Constants.BothTeamsNeededErrorMessage);
            }
            else if (guestSeason == null)
            {
                // GuestName not in database (name probably misspelled)
                _context.MoveFocusTo("GuestName");
                throw new DataValidationException(WpfGlobals.Constants.TeamNotInDatabaseMessage);
            }
            else if (hostSeason == null)
            {
                // GuestName not in database (name probably misspelled)
                _context.MoveFocusTo("HostName");
                throw new DataValidationException(WpfGlobals.Constants.TeamNotInDatabaseMessage);
            }

            return new Matchup(guestSeason, hostSeason);
        }

        #endregion Methods
    }
}
