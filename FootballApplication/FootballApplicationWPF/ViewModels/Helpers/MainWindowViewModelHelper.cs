﻿using System;
using System.Linq;
using System.Threading.Tasks;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers
{
    public interface IMainWindowViewModelHelper
    {
        void UpdateRankings();
        void UpdateRankingsByTeamSeason(TeamSeason TeamSeason);
    }

    public class MainWindowViewModelHelper : IMainWindowViewModelHelper
    {
        #region Fields

        private readonly IMainWindowViewModel _context;
        private readonly ICalculator _calculator;
        private readonly IGlobals _globals;

        private object _dbLock = new object();

        #endregion Fields

        #region Constructors & Finalizers
        /// <summary>
        /// Initializes a new instance of the MainWindowViewModelHelper class
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calculator"></param>
        /// <param name="globals"></param>
        public MainWindowViewModelHelper(IMainWindowViewModel context, ICalculator calculator = null, IGlobals globals = null)
        {
            // Validate required arguments.
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            _context = context;
            _calculator = (calculator != null) ? calculator : new Calculator();
            _globals = (globals != null) ? globals: new WpfGlobals();
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Updates the rankings for the selected season
        /// </summary>
        public void UpdateRankings()
        {
            try
            {
                var seasonTeams = (from st in _context.DbContext.TeamSeasons
                                   where st.SeasonID == SharedGlobals.SelectedSeason
                                   select st)
                                   .ToList();

                // This looks like the place where I want to make maximum use of parallel threading.
                Parallel.ForEach(seasonTeams, seasonTeam => UpdateRankingsByTeamSeason(seasonTeam));

                //foreach (var seasonTeam in seasonTeams)
                //{
                //    UpdateRankingsByTeamSeason(seasonTeam);
                //}

                _context.DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Updates the rankings for the specified team and the selected season
        /// </summary>
        /// <param name="seasonTeam"></param>
        public virtual void UpdateRankingsByTeamSeason(TeamSeason seasonTeam)
        {
            try
            {
                usp_GetTeamSeasonScheduleTotals_Result seasonTeamScheduleTotals = null;
                usp_GetTeamSeasonScheduleAverages_Result seasonTeamScheduleAverages = null;

                lock (_dbLock)
                {
                    var selectedSeason = SharedGlobals.SelectedSeason;
                    seasonTeamScheduleTotals = _context.DbContext.usp_GetTeamSeasonScheduleTotals(seasonTeam.TeamName, selectedSeason).FirstOrDefault();
                    seasonTeamScheduleAverages = _context.DbContext.usp_GetTeamSeasonScheduleAverages(seasonTeam.TeamName, selectedSeason).FirstOrDefault();
                }

                if ((seasonTeamScheduleTotals != null) && (seasonTeamScheduleAverages != null) && (seasonTeamScheduleTotals.ScheduleGames != null))
                {
                    LeagueSeason leagueSeason;
                    lock (_dbLock)
                    {
                        leagueSeason = (from ls in _context.DbContext.LeagueSeasons
                                        where ls.LeagueName == seasonTeam.League.Name
                                        select ls)
                                        .FirstOrDefault();
                    }

                    seasonTeam.OffensiveAverage = _calculator.Divide(seasonTeam.PointsFor, seasonTeam.Games);
                    seasonTeam.DefensiveAverage = _calculator.Divide(seasonTeam.PointsAgainst, seasonTeam.Games);

                    seasonTeam.OffensiveFactor =
                        _calculator.Divide((double)seasonTeam.OffensiveAverage, (double)seasonTeamScheduleAverages.PointsAgainst);
                    seasonTeam.DefensiveFactor =
                        _calculator.Divide((double)seasonTeam.DefensiveAverage, (double)seasonTeamScheduleAverages.PointsFor);

                    seasonTeam.OffensiveIndex = (seasonTeam.OffensiveAverage + seasonTeam.OffensiveFactor * leagueSeason.AveragePoints) / 2;
                    seasonTeam.DefensiveIndex = (seasonTeam.DefensiveAverage + seasonTeam.DefensiveFactor * leagueSeason.AveragePoints) / 2;

                    seasonTeam.FinalPythagoreanWinningPercentage =
                        _calculator.CalculatePythagoreanWinningPercentage(seasonTeam);
                }
            }
            catch (InvalidOperationException ex)
            {
                if (ex.Message == "Nullable object must have a value.")
                {
                    // Ignore exception.
                }
                else
                {
                    _globals.ShowExceptionMessage(ex, "InvalidOperationException: " + seasonTeam.Team.Name);
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex.InnerException, "Exception: " + seasonTeam.Team.Name);
            }
        }

        #endregion Methods
    }
}
