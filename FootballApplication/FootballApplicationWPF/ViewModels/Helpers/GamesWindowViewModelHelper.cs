﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using EldredBrown.FootballApplicationShared;
using EldredBrown.FootballApplicationWPF.Models;

namespace EldredBrown.FootballApplicationWPF.ViewModels.Helpers
{
    public interface IGamesWindowViewModelHelper
    {
        void ApplyFindGameFilter();
        void ClearDataEntryControls();
        void EditScoringDataByTeam(Team team, Operation operation, double teamScore, double opponentScore);
        void EditTeams(IGame currentGame, Direction direction);
        void EditWinLossData(IGame currentGame, Operation operation);
        TeamSeason GetGameTeamSeason(string teamName);
        void LoadGames(IQueryable<Game> games);
        void PopulateDataEntryControls();
        void ProcessGame(IGame currentGame, Operation operation);
        void SaveChanges();
        void SortGamesByDefaultOrder();
        IGame StoreNewGameValues(IGame newGame = null);
        IGame StoreOldGameValues(IGame newGame = null);
        void ValidateDataEntry();
    }

    public class GamesWindowViewModelHelper : IGamesWindowViewModelHelper
    {
        #region Fields

        private readonly IGamesWindowViewModel _context;
        private readonly ICalculator _calculator;
        private readonly IGlobals _globals;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GamesWindowViewModelHelper class
        /// </summary>
        /// <param name="context"></param>
        /// <param name="calculator"></param>
        /// <param name="globals"></param>
        public GamesWindowViewModelHelper(IGamesWindowViewModel context, ICalculator calculator = null, IGlobals globals = null)
        {
            // Validate required arguments.
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            // Assign argument values to member fields.
            _context = context;
            _calculator = (calculator != null) ? calculator : new Calculator();
            _globals = (globals != null) ? globals: new WpfGlobals();
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Applies the filter set in the GameFinderWindowViewModel
        /// </summary>
        public void ApplyFindGameFilter()
        {
            try
            {
                var dataContext = _context.GameFinder.DataContext as IGameFinderWindowViewModel;
                var guest = dataContext.GuestName;
                var host = dataContext.HostName;
                var games = (from game in _context.DbContext.Games
                             where ((game.GuestName == guest) && (game.HostName == host) && (game.SeasonID == SharedGlobals.SelectedSeason))
                             select game);

                LoadGames(games);

                _context.IsFindGameFilterApplied = true;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Resets data in all data entry fields to their default values.
        /// </summary>
        public void ClearDataEntryControls()
        {
            try
            {
                _context.GuestName = String.Empty;
                _context.GuestScore = 0;
                _context.HostName = String.Empty;
                _context.HostScore = 0;
                _context.IsPlayoffGame = false;
                _context.Notes = String.Empty;

                _context.AddGameControlVisibility = Visibility.Visible;
                _context.EditGameControlVisibility = Visibility.Hidden;
                _context.RemoveGameControlVisibility = Visibility.Hidden;

                // Set focus to GuestName field.
                _context.MoveFocusTo("GuestName");
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /////// <summary>
        /////// Edits each teamSeason's data (games, points for, points against) from playoff games.
        /////// </summary>
        /////// <param name = "winner"></param>
        /////// <param name = "loser"></param>
        /////// <param name = "operation"></param>
        /////// <param name = "currentGame"></param>
        /////// <param name = "winnerScore"></param>
        /////// <param name = "loserScore"></param>
        ////public static void EditDataFromPlayoffGames(Team winner, Team loser, Operation operation, Game currentGame)
        ////{
        ////	try
        ////	{
        ////		EditWinLossDataFromPlayoffGames(winner, loser, operation, currentGame);
        ////	}
        ////	catch ( Exception ex )
        ////	{
        ////		_globals.ShowExceptionMessage(ex);
        ////	}
        ////}

        /// <summary>
        /// Edits all scoring data for the specified Team
        /// </summary>
        /// <param name = "team"></param>
        /// <param name = "operation"></param>
        /// <param name = "teamScore"></param>
        /// <param name = "opponentScore"></param>
        public virtual void EditScoringDataByTeam(Team team, Operation operation, double teamScore, double opponentScore)
        {
            var teamSeason = GetGameTeamSeason(team.Name);

            teamSeason.PointsFor = (int)operation(teamSeason.PointsFor, teamScore);
            teamSeason.PointsAgainst = (int)operation(teamSeason.PointsAgainst, opponentScore);

            var pythPct = _calculator.CalculatePythagoreanWinningPercentage(teamSeason);
            if (pythPct == null)
            {
                teamSeason.PythagoreanWins = 0;
                teamSeason.PythagoreanLosses = 0;
            }
            else
            {
                teamSeason.PythagoreanWins = _calculator.Multiply((double)pythPct, teamSeason.Games);
                teamSeason.PythagoreanLosses = _calculator.Multiply((double)(1 - pythPct), teamSeason.Games);
            }
        }

        /// <summary>
        /// Edits the DataModel's Teams table with data from the specified game.
        /// </summary>
        /// <param name = "currentGame"></param>
        /// <param name = "direction"></param>
        public void EditTeams(IGame currentGame, Direction direction)
        {
            try
            {
                Operation operation;

                // Decide whether the teams need to be edited up or down.
                // Up for new game, down then up for edited game, down for deleted game.
                switch (direction)
                {
                    case Direction.Up:
                        operation = new Operation(_calculator.Add);
                        break;

                    case Direction.Down:
                        operation = new Operation(_calculator.Subtract);
                        break;

                    default:
                        throw new ArgumentException("direction");
                }

                ProcessGame(currentGame, operation);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Edits each teamSeason's data (wins, losses, and ties) from all games.
        /// </summary>
        /// <param name = "currentGame"></param>
        /// <param name = "operation"></param>
        public virtual void EditWinLossData(IGame currentGame, Operation operation)
        {
            try
            {
                var guestSeason = GetGameTeamSeason(currentGame.Guest.Name);
                var hostSeason = GetGameTeamSeason(currentGame.Host.Name);

                TeamSeason winnerSeason = null;
                TeamSeason loserSeason = null;
                if ((currentGame.Winner != null) && (currentGame.Loser != null))
                {
                    winnerSeason = GetGameTeamSeason(currentGame.Winner.Name);
                    loserSeason = GetGameTeamSeason(currentGame.Loser.Name);
                }

                guestSeason.Games = (int)operation(guestSeason.Games, 1);
                hostSeason.Games = (int)operation(hostSeason.Games, 1);

                if ((winnerSeason == null) && (loserSeason == null))
                {
                    guestSeason.Ties = (int)operation(guestSeason.Ties, 1);
                    hostSeason.Ties = (int)operation(hostSeason.Ties, 1);
                }
                else
                {
                    winnerSeason.Wins = (int)operation(winnerSeason.Wins, 1);
                    loserSeason.Losses = (int)operation(loserSeason.Losses, 1);
                }

                guestSeason.WinningPercentage = _calculator.CalculateWinningPercentage(guestSeason);
                hostSeason.WinningPercentage = _calculator.CalculateWinningPercentage(hostSeason);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Gets the TeamSeason object for the specified team and the selected season
        /// </summary>
        /// <param name="teamName"></param>
        /// <returns></returns>
        public virtual TeamSeason GetGameTeamSeason(string teamName)
        {
            TeamSeason retVal = (from teamSeason in _context.DbContext.TeamSeasons
                                 where (teamSeason.Team.Name == teamName) && (teamSeason.SeasonID == SharedGlobals.SelectedSeason)
                                 select teamSeason)
                                 .FirstOrDefault();
            return retVal;
        }

        /// <summary>
        /// Edits each teamSeason's data (games, points for, points against, adjusted points for, adjusted points against) from all games.
        /// </summary>
        /// <param name = "currentGame"></param>
        /// <param name = "operation"></param>
        /// <summary>
        /// Loads _context GamesWindowViewModel object's Games collection.
        /// </summary>
        /// <param name = "games"></param>
        public virtual void LoadGames(IQueryable<Game> games)
        {
            try
            {
                _context.Games = new ObservableCollection<Game>();
                foreach (var game in games)
                {
                    _context.Games.Add(game);
                }
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Populates data entry controls with data from selected game.
        /// </summary>
        public void PopulateDataEntryControls()
        {
            try
            {
                var selectedGame = _context.SelectedGame;
                _context.Week = selectedGame.Week;
                _context.GuestName = selectedGame.GuestName;
                _context.GuestScore = selectedGame.GuestScore;
                _context.HostName = selectedGame.HostName;
                _context.HostScore = selectedGame.HostScore;
                _context.IsPlayoffGame = selectedGame.IsPlayoffGame;
                _context.Notes = selectedGame.Notes;

                _context.AddGameControlVisibility = Visibility.Hidden;
                _context.EditGameControlVisibility = Visibility.Visible;
                _context.RemoveGameControlVisibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        public virtual void ProcessGame(IGame currentGame, Operation operation)
        {
            try
            {
                EditWinLossData(currentGame, operation);

                EditScoringDataByTeam(currentGame.Guest, operation, currentGame.GuestScore, currentGame.HostScore);
                EditScoringDataByTeam(currentGame.Host, operation, currentGame.HostScore, currentGame.GuestScore);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Saves all changes made to the database.
        /// </summary>
        public void SaveChanges()
        {
            try
            {
                // Save all changes to the database.
                _context.DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Sorts the Games collection by Week ascending, then by HostName ascending.
        /// </summary>
        public void SortGamesByDefaultOrder()
        {
            try
            {
                var games = (from game in _context.DbContext.Games
                             where game.SeasonID == SharedGlobals.SelectedSeason
                             orderby game.Week descending
                             orderby game.ID descending
                             select game).
                             AsQueryable();

                LoadGames(games);
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        /// <summary>
        /// Stores values from the data entry controls into a Game instance.
        /// </summary>
        /// <returns></returns>
        public IGame StoreNewGameValues(IGame newGame = null)
        {
            try
            {
                var guest = (from team in _context.DbContext.Teams
                             where team.Name == _context.GuestName
                             select team)
                             .FirstOrDefault();
                var host = (from team in _context.DbContext.Teams
                            where (team.Name == _context.HostName)
                            select team)
                            .FirstOrDefault();

                if (newGame == null)
                {
                    newGame = new Game();
                }

                newGame.ID = 0;
                newGame.SeasonID = (int)SharedGlobals.SelectedSeason;
                newGame.Week = _context.Week;
                newGame.Guest = guest;
                newGame.GuestScore = _context.GuestScore;
                newGame.Host = host;
                newGame.HostScore = _context.HostScore;
                newGame.IsPlayoffGame = _context.IsPlayoffGame;
                newGame.Notes = _context.Notes;

                newGame.DecideWinnerAndLoser();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }

            return newGame;
        }

        /// <summary>
        /// Stores values from the selected game into a Game instance.
        /// </summary>
        public IGame StoreOldGameValues(IGame oldGame = null)
        {
            try
            {
                var guest = (from team in _context.DbContext.Teams
                             where team.Name == _context.GuestName
                             select team)
                             .FirstOrDefault();
                var host = (from team in _context.DbContext.Teams
                            where (team.Name == _context.HostName)
                            select team)
                            .FirstOrDefault();

                if (oldGame == null)
                {
                    oldGame = new Game();
                }

                var selectedGame = _context.SelectedGame;

                oldGame.ID = 0;
                oldGame.SeasonID = (int)SharedGlobals.SelectedSeason;
                oldGame.Week = selectedGame.Week;
                oldGame.Guest = selectedGame.Guest;
                oldGame.GuestScore = selectedGame.GuestScore;
                oldGame.Host = selectedGame.Host;
                oldGame.HostScore = selectedGame.HostScore;
                oldGame.IsPlayoffGame = selectedGame.IsPlayoffGame;
                oldGame.Notes = selectedGame.Notes;

                oldGame.DecideWinnerAndLoser();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }

            return oldGame;
        }

        /// <summary>
        /// Validates all data entered into the data entry fields.
        /// </summary>
        public void ValidateDataEntry()
        {
            var guestSeason = GetGameTeamSeason(_context.GuestName);
            var hostSeason = GetGameTeamSeason(_context.HostName);

            if (String.IsNullOrWhiteSpace(_context.GuestName))
            {
                // GuestName field is left empty.
                _context.MoveFocusTo("GuestName");
                throw new DataValidationException(WpfGlobals.Constants.BothTeamsNeededErrorMessage);
            }
            else if (String.IsNullOrWhiteSpace(_context.HostName))
            {
                // HostName field is left empty.
                _context.MoveFocusTo("HostName");
                throw new DataValidationException(WpfGlobals.Constants.BothTeamsNeededErrorMessage);
            }
            else if (guestSeason == null)
            {
                // GuestName not in database for the selected season (name probably misspelled)
                _context.MoveFocusTo("GuestName");
                throw new DataValidationException(WpfGlobals.Constants.TeamNotInDatabaseMessage);
            }
            else if (hostSeason == null)
            {
                // HostName not in database for the selected season (name probably misspelled)
                _context.MoveFocusTo("HostName");
                throw new DataValidationException(WpfGlobals.Constants.TeamNotInDatabaseMessage);
            }
            else if (_context.GuestName == _context.HostName)
            {
                // Guest and host are the same team.
                _context.MoveFocusTo("GuestName");
                throw new DataValidationException(WpfGlobals.Constants.DifferentTeamsNeededErrorMessage);
            }
        }

        #endregion Methods
    }
}
