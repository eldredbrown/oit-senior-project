﻿using System;
using System.Data.Entity;
using EldredBrown.FootballApplicationWPF.Models;
using EldredBrown.FootballApplicationWPF.UserControls;
using EldredBrown.FootballApplicationWPF.Windows;

namespace EldredBrown.FootballApplicationWPF.Helpers
{
    public interface IMainWindowHelper
    {
        void ShowGames(IGamesWindow gamesWindow, FootballDbEntities dbContext, ITeamsControl teamsControl);
    }

    public class MainWindowHelper : IMainWindowHelper
    {
        #region Fields

        private readonly IMainWindow _context;
        private readonly IGlobals _globals;

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Constructs a new instance of the MainWindowHelper class
        /// </summary>
        /// <param name="context"></param>
        /// <param name="globals"></param>
        public MainWindowHelper(IMainWindow context, IGlobals globals = null)
        {
            // Validate required arguments.
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            _context = context;
            _globals = (globals != null) ? globals : new WpfGlobals();
        }

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Shows the Games window
        /// </summary>
        /// <param name="gamesWindow"></param>
        /// <param name="dbContext"></param>
        /// <param name="teamsControl"></param>
        public void ShowGames(IGamesWindow gamesWindow, FootballDbEntities dbContext, ITeamsControl teamsControl)
        {
            try
            {
                // Show the Games window.
                gamesWindow.ShowDialog();

                // Load the EntityDataModel representation of the Games table.
                dbContext.Games.Load();

                teamsControl.Refresh();
            }
            catch (Exception ex)
            {
                _globals.ShowExceptionMessage(ex);
            }
        }

        #endregion Methods
    }
}
