﻿using System;
using System.Windows;

namespace EldredBrown.FootballApplicationWPF
{
    public interface IGlobals
    {
        void ShowExceptionMessage(DataValidationException ex);
        void ShowExceptionMessage(Exception ex, string caption = "Exception");
    }

    public class WpfGlobals : IGlobals
	{
        internal struct Constants
        {
            internal const string AppName = "Football Application";

            internal const string AdjustedScoresErrorMessage = "Unless the game is a forfeit, the winning team must be awarded a higher adjusted score than the losing team.";
            internal const string BothTeamsNeededErrorMessage = "Please enter names for both teams.";
            internal const string DifferentTeamsNeededErrorMessage = "Please enter a different name for each team.";
            internal const string TeamNotInDatabaseMessage = "Please make sure that both teams are in the NFL and that both team names are spelled correctly.";
            internal const string TieScoresNotPermittedErrorMessage = "Tie scores are not permitted for playoff games.";
        }

        #region Constructors

        /// <summary>
        /// Initializes a new default instance of the Globals class
        /// </summary>
        public WpfGlobals()
        {
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Shows a DataValidationException message.
        /// </summary>
        /// <param name = "message"></param>
        public virtual void ShowExceptionMessage(DataValidationException ex)
        {
            ShowExceptionMessage(ex, "DataValidationException");
        }

        /// <summary>
        /// Shows the message for any exception of a type that doesn't have its own specific ShowMessage method.
        /// </summary>
        /// <param name = "message"></param>
        /// <param name = "caption"></param>
        public virtual void ShowExceptionMessage(Exception ex, string caption = "Exception")
        {
            MessageBox.Show(ex.Message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #endregion Methods
    }
}
