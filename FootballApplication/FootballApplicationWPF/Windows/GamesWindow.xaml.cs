﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.Windows
{
    public interface IGamesWindow
    {
        bool? ShowDialog();
    }

    /// <summary>
    /// Interaction logic for Games.xaml
    /// </summary>
    public partial class GamesWindow : Window, IGamesWindow
	{
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GamesWindow class
        /// </summary>
        public GamesWindow()
		{
			InitializeComponent();

			DataContext = new GamesWindowViewModel(DataAccess.DbContext);
		}

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Opens a window and returns only when the newly opened window is closed
        /// </summary>
        /// <returns></returns>
        public new bool? ShowDialog()
        {
            return base.ShowDialog();
        }

        #endregion Methods

        #region Event Handlers

        private void GamesDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (GamesDataGrid.SelectedItem == CollectionView.NewItemPlaceholder)
            {
                // Prepare to add a new game.
                (DataContext as GamesWindowViewModel).SelectedGame = null;
            }
        }

        #endregion Event Handlers
    }
}
