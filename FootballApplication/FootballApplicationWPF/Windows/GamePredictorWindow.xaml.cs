﻿using System.Windows;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.Windows
{
    public interface IGamePredictorWindow
    {
        void Show();
    }

    /// <summary>
    /// Interaction logic for GamePredictorWindow.xaml
    /// </summary>
    public partial class GamePredictorWindow : Window, IGamePredictorWindow
    {
        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GamePredictorWindow class
        /// </summary>
        public GamePredictorWindow()
        {
            InitializeComponent();

            DataContext = new GamePredictorWindowViewModel(DataAccess.DbContext);
        }

        #endregion Constructors & Finalizers
    }
}
