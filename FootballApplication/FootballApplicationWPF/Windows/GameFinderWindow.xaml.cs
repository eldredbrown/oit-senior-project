﻿using System.Windows;
using EldredBrown.FootballApplicationWPF.ViewModels;

namespace EldredBrown.FootballApplicationWPF.Windows
{
    public interface IGameFinderWindow
    {
        object DataContext { get; set; }

        bool? ShowDialog();
    }

    /// <summary>
    /// Interaction logic for GameFinder.xaml
    /// </summary>
    public partial class GameFinderWindow : Window, IGameFinderWindow
	{
        #region Fields

        private readonly IGlobals _globals = new WpfGlobals();

        #endregion Fields

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new instance of the GameFinderWindow class
        /// </summary>
        public GameFinderWindow()
		{
			InitializeComponent();

			DataContext = new GameFinderWindowViewModel(DataAccess.DbContext);
		}

        #endregion Constructors & Finalizers

        #region Methods

        /// <summary>
        /// Opens a window and returns only when the newly opened window is closed
        /// </summary>
        /// <returns></returns>
        public new bool? ShowDialog()
        {
            return base.ShowDialog();
        }

        #endregion

        #region Event Handlers

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            var vm = (DataContext as GameFinderWindowViewModel);
            try
            {
                // The GuestName and HostName properties in the underlying ViewModel are not updated automatically when a press
                // of the Enter key clicks the OK button automatically, so we need to update these directly as follows: 
                vm.GuestName = GuestTextBox.Text;
                vm.HostName = HostTextBox.Text;
                vm.ValidateDataEntry();
            }
            catch (DataValidationException ex)
            {
                _globals.ShowExceptionMessage(ex);
                GuestTextBox.Focus();
                return;
            }

            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        #endregion Event Handlers
    }
}
