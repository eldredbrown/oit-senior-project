﻿using System;
using System.Runtime.Serialization;

namespace EldredBrown.FootballApplicationWPF
{
	public class DataValidationException : Exception
	{
        /// <summary>
        /// Initializes a new instance of the DataValidationException class
        /// </summary>
        /// <param name="message"></param>
		public DataValidationException(string message)
			: base(message)
		{
		}

        /// <summary>
        /// Initializes a new instance of the DataValidationException class
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
		public DataValidationException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

        /// <summary>
        /// Initializes a new instance of the DataValidationException class
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
		public DataValidationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
