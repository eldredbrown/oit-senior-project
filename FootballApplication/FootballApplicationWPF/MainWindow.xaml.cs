﻿using System.Windows;
using EldredBrown.FootballApplicationWPF.Helpers;
using EldredBrown.FootballApplicationWPF.ViewModels;
using EldredBrown.FootballApplicationWPF.Windows;

namespace EldredBrown.FootballApplicationWPF
{
    public interface IMainWindow
    {
    }

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IMainWindow
	{
        #region Fields

        private readonly IMainWindowHelper _helper;

        #endregion

        #region Constructors & Finalizers

        /// <summary>
        /// Initializes a new default instance of the MainWindow class
        /// </summary>
        public MainWindow()
		{
            InitializeComponent();

            _helper = new MainWindowHelper(this);

            DataContext = new MainWindowViewModel(DataAccess.DbContext);
        }

        #endregion Constructors & Finalizers

        #region Event Handlers

        private void SeasonsComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            this.TeamsControl.Refresh();
            this.StandingsControl.Refresh();
            this.RankingsControl.Refresh();
        }

        private void ShowGamesButton_Click(object sender, RoutedEventArgs e)
        {
            _helper.ShowGames(new GamesWindow(), DataAccess.DbContext, TeamsControl);
        }

        #endregion Event Handlers
    }
}
